<?php

return [

	'sizes' => [34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56],

	'surcharge' => [
		'fixed'    => 1,
		'percents' => 0.03,
	],

	'free_delivery' => 250,

	'max_available_products' => 10
];
