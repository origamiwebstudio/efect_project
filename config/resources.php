<?php

return [
  'home' => [
    'styles' => [
      'css/home.css'
    ],
    'scripts' => [
      'js/home.js'
    ]
  ],
  'products' => [
    'all' => [
      'styles' => [
        'css/products.css'
      ],
      'scripts' => [
        'js/products.js'
      ]
    ],
    'single' => [
      'styles' => [
        'css/product.css'
      ],
      'scripts' => [
        'js/product.js'
      ]
    ]
  ],
  'auth' => [
    'styles' => [
      'css/auth.css'
    ],
    'scripts' => [
      'js/auth.js'
    ]
  ],
  'profile' => [
    'styles' => [
      'css/profile.css'
    ],
    'scripts' => [
      'js/profile.js'
    ]
  ],
  'otherpages' => [
    'styles' => [
      'css/otherpages.css'
    ],
    'scripts' => [
      'js/otherpages.js'
    ]
  ]
];