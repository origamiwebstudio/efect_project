<?php

Auth::routes();

Route::get('/home', 'HomeController@showHomePage')->name('home');
Route::get('/', 'HomeController@showHomePage')->name('home');

Route::post('/search', 'SearchController@getJsonResults')->name('search-results-json');

#Products
Route::get('/products/{filter?}', 'ProductsController@showProductsPage')->where('filter', '[a-zA-Z]+')->name('products');
Route::get('/products/{id}', 'ProductsController@showSingleProductPage')->where('id', '[0-9]+')->name('product');
Route::post('/products', 'ProductsController@getJsonProducts')->name('json-products');

#Basket
Route::get('/profile/basket', 'CartController@showCartPage')->name('profile-basket');
Route::post('/basket/sidebar', 'CartController@getCartShortInfo')->name('json-basket-sidebar');
Route::post('/basket/add', 'CartController@addProduct')->name('add-product-to-basket');
Route::post('/basket/remove', 'CartController@removeProduct')->name('remove-product-from-basket');
Route::post('/basket/count', 'CartController@getProductsCount')->name('count-products-in-basket');
Route::post('/basket/quantity/change', 'CartController@changeProductQuantity')->name('change-product-quantity-in-basket');

#Favorites
Route::post('/favorites', 'FavoritesController@getJsonFavorites')->name('json-favorites');
Route::post('/favorites/add', 'FavoritesController@addProduct')->name('add-product-to-favorites');
Route::post('/favorites/remove', 'FavoritesController@removeProduct')->name('remove-product-to-favorites');

#Orders
Route::get('/order', 'OrdersController@getOrderPage')->name('order');
Route::get('/order/accept', 'OrdersController@getOrderAcceptPage')->name('order-accept');
Route::get('/order/payment/cancel/{uid}', 'OrdersController@cancelPayment')->where('uid', '[a-zA-Z0-9]+')->name('order-cancel-payment');
Route::post('/order', 'OrdersController@makeOrder')->name('make-order');
Route::post('/order/update', 'OrdersController@updateOrder')->name('update-order');
Route::post('/order/accept', 'OrdersController@makePayment')->name('make-payment');

#Payments
Route::get('/order/paypal/{uid?}', 'OrdersController@paymentPayPalExpress')->where('uid', '[a-fA-F0-9]{7}')->name('make-order-paypal');

#Profile
Route::get('/profile', 'ProfileController@showProfilePage')->name('profile');
Route::get('/profile/favorites', 'ProfileController@showFavoritesPage')->name('profile-favorites');
Route::get('/profile/orders', 'ProfileController@showOrdersPage')->name('profile-orders');
Route::get('/profile/viewed', 'ProfileController@showLastViewedPage')->name('profile-last-viewed');
Route::post('/profile/edit', 'ProfileController@changeProfileData')->name('profile-edit');

#Message
Route::get('/message/{code}', 'MessagesController@getMessagePage')->where('code', '[0-9]+')->name('message');

#Cookies
Route::post('/cookies/accept', 'CookiesController@acceptCookies')->name('accept-cookies');

#Static pages
Route::get('/contacts', 'ContactsController@getContactsPage')->name('contacts');
Route::post('/contacts/send', 'EmailRequestsController@sendContactForm')->name('contacts-send');

Route::get('/about', 'AboutController@getAboutPage')->name('about');
Route::get('/information/privacy-policy', 'InformationController@getPolicyPage')->name('privacy-policy');
Route::get('/information/regimen', 'InformationController@getRegimenPage')->name('regimen');
Route::get('/information/delivery-and-payment', 'InformationController@getDeliveryAndPaymentPage')->name('delivery-payment');
Route::get('/information/complaints-and-returns', 'InformationController@getComplaintsAndReturnsPage')->name('complaints-returns');