<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartProductPivotTable extends Migration
{
	public function up()
	{
		Schema::create('cart_product', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('cart_id');
			$table->integer('product_id');
			$table->string('size', 2);
			$table->string('color', 7);
			$table->integer('quantity');
			$table->decimal('actual_price', 6, 2);
			$table->decimal('summary', 8, 2);
		});
	}

	public function down()
	{
		Schema::dropIfExists('cart_product');
	}
}
