<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSummaryFieldToOrderProductTable extends Migration
{
	public function up()
	{
		Schema::table('order_product', function (Blueprint $table) {
			//
			$table->decimal('summary', 8, 2)->after('actual_price');
		});
	}

	public function down()
	{
		Schema::table('order_product', function (Blueprint $table) {
			//
			$table->dropColumn('summary');
		});
	}
}
