<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductImagesTable extends Migration
{
	public function up()
	{
		Schema::create('product_images', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('order_')->default(0);
			$table->string('link');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('product_images');
	}
}
