<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsTable extends Migration
{
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->decimal('price', 6, 2);
			$table->string('offer_type', 48)->nullable();
			$table->decimal('offer_value', 6, 2)->nullable();
			$table->boolean('novelty')->default(true);
			$table->integer('sales')->default(0);
			$table->integer('views')->default(0);
			$table->integer('favorite')->default(0);
			$table->integer('s34')->default(0);
			$table->integer('s36')->default(0);
			$table->integer('s38')->default(0);
			$table->integer('s40')->default(0);
			$table->integer('s42')->default(0);
			$table->integer('s44')->default(0);
			$table->integer('s46')->default(0);
			$table->integer('s48')->default(0);
			$table->integer('s50')->default(0);
			$table->integer('s52')->default(0);
			$table->integer('s54')->default(0);
			$table->integer('s56')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('products');
	}
}
