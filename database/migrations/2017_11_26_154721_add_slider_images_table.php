<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSliderImagesTable extends Migration
{
	public function up()
	{
		Schema::create('slider_images', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('order_')->default(0);
			$table->string('link');
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::dropIfExists('slider_images');
	}
}
