<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryTypesTable extends Migration
{
	public function up()
	{
		Schema::create('delivery_types', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('order_')->default(0);
			$table->string('name');
			$table->decimal('price', 6, 2);
		});
	}

	public function down()
	{
		Schema::dropIfExists('delivery_types');
	}
}
