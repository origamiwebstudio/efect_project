<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductColorPivotTable extends Migration
{
	public function up()
	{
		Schema::create('product_color', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('color_id');
		});
	}

	public function down()
	{
		Schema::dropIfExists('product_color');
	}
}
