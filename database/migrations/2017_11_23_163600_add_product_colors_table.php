<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductColorsTable extends Migration
{
	public function up()
	{
		Schema::create('product_colors', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('hex');
		});
	}

	public function down()
	{
		Schema::dropIfExists('product_colors');
	}
}
