<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisibleFieldToProductsTable extends Migration
{
	public function up()
	{
		Schema::table('products', function (Blueprint $table) {
			//
			$table->boolean('visible')->default(true)->after('novelty');
		});
	}

	public function down()
	{
		Schema::table('products', function (Blueprint $table) {
			//
			$table->removeColumn('visible');
		});
	}
}
