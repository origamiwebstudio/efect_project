<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvantagesTable extends Migration
{
	public function up()
	{
		Schema::create('advantages', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('order_');
			$table->string('name');
			$table->string('text');
		});
	}

	public function down()
	{
		Schema::dropIfExists('advantages');
	}
}
