<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
	public function up()
	{
		Schema::create('orders', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->string('order_number', 12);
			$table->string('order_status', 128);
			$table->string('name');
			$table->string('surname');
			$table->string('email');
			$table->string('phone');
			$table->string('country');
			$table->string('locality');
			$table->string('postal_code');
			$table->string('street');
			$table->string('house_number');
			$table->string('delivery_type');
			$table->decimal('delivery_price', 6, 2);
			$table->string('payment_type');
			$table->string('payment_status');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('orders');
	}
}
