<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
	public function up()
	{
		Schema::create('carts', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->boolean('enabled')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('carts');
	}
}
