<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCartIdFieldToOrdersTable extends Migration
{
	public function up()
	{
		Schema::table('orders', function (Blueprint $table) {
			//
			$table->integer('cart_id')->nullable()->after('user_id');
		});
	}

	public function down()
	{
		Schema::table('orders', function (Blueprint $table) {
			//
			$table->removeColumn('cart_id');
		});
	}
}
