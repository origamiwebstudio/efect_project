<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFieldsToUsersTable extends Migration
{
	public function up()
	{
		Schema::table('users', function (Blueprint $table) {
			//
			$table->string('country')->nullable()->after('phone');
			$table->string('locality')->nullable()->after('phone');
			$table->string('postal_code')->nullable()->after('phone');
			$table->string('street')->nullable()->after('phone');
			$table->string('house_number')->nullable()->after('phone');
		});
	}

	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			//
			$table->dropColumn('country');
			$table->dropColumn('locality');
			$table->dropColumn('postal_code');
			$table->dropColumn('street');
			$table->dropColumn('house_number');
		});
	}
}
