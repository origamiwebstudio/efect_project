const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir((mix) => {
    mix.sass('common.scss')
      .sass('home.scss')
      .sass('auth.scss')
      .sass('products.scss')
      .sass('product.scss')
      .sass('profile.scss')
      .sass('otherpages.scss')
      .webpack('common.js')
      .webpack('home.js')
      .webpack('auth.js')
      .webpack('products.js')
      .webpack('profile.js')
      .webpack('product.js')
      .webpack('otherpages.js')
});


// .version([
//   'css/common.css',
//   'css/home.css',
//   'css/auth.css',
//   'css/products.css',
//   'css/product.css',
//   'css/profile.css',
//   'css/otherpages.css',
//   'js/common.js',
//   'js/home.js',
//   'js/auth.js',
//   'js/products.js',
//   'js/product.js',
//   'js/otherpages.js'
// ])