<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advantage extends Model
{
	protected $table = 'advantages';
	public $timestamps = false;

	public static function getAdvantages() {
		return Advantage::orderBy('order_')->limit(4)->get();
	}
}
