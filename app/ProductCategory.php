<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	protected $table = 'product_categories';
	public $timestamps = false;

	public static function getAvailableCategories() {
		return ProductCategory::get();
	}
}
