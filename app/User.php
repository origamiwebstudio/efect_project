<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
	use Notifiable, EntrustUserTrait, CanResetPassword;

	protected $table = 'users';
	protected $fillable = ['name', 'email', 'password'];
	protected $hidden = ['password', 'remember_token'];

	public function favorites()
	{
		return $this->belongsToMany(Product::class, 'favorites', 'user_id', 'product_id')->withTimestamps();
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}

	public function getActiveCartAttribute()
	{
		$cart = Cart::where('user_id', '=', $this->attributes['id'])->where('enabled', true)->first();

		if (!$cart) {
			$cart = new Cart();

			$cart['user_id'] = $this->attributes['id'];
			$cart['enabled'] = true;

			$cart->save();
		}

		return $cart;
	}

	public function sendPasswordResetNotification($token)
	{
		Mail::send('emails.reset-link', ['reset_link' => url('/password/reset/' . $token)], function ($message) {
			$message->from('contact@efect-moda.com', 'efect-moda.com');
			$message->to($this->attributes['email'])->subject('Resetowanie hasła');
		});
	}
}
