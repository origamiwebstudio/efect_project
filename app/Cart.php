<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	protected $table = 'carts';

	public function products()
	{
		return $this->belongsToMany(Product::class, 'cart_product', 'cart_id', 'product_id')
			->withPivot(['id', 'size', 'color', 'quantity', 'actual_price', 'summary']);
	}

	public static function getProductsCount()
	{
		if ($user = auth()->user()) {
			if ($cart = $user->activeCart) {
				return $cart->products->count();
			}
		} elseif ($guest_cart = session('guest_cart')) {
			return count($guest_cart);
		}

		return 0;
	}

	public static function getProductsSummaryPrice()
	{
		if ($user = auth()->user()) {
			if ($cart = $user->activeCart) {
				$summary = 0;

				foreach ($cart->products as $product) {
					$summary += $product->pivot->summary;
				}

				return $summary;
			}
		}

		return 0;
	}

	public static function isAllProductsAvailable()
	{
		if ($user = auth()->user()) {
			if ($cart = $user->activeCart) {
				$switcher = true;
				$tmp = 0;

				foreach ($cart->products as $product) {
					foreach ($cart->products()->where('products.id', '=', $product->id)->wherePivot('size', '=', $product->pivot->size)->get() as $item) {
						$tmp += $item->pivot->quantity;
					}

					if ($tmp > $product['s' . $product->pivot->size]) {
						$switcher = false;
						break;
					}
				}

				return $switcher;
			}
		}

		return false;
	}

	public function getProductsSummaryPriceAttribute()
	{
		$summary = 0;

		if (isset($this->products)) {
			foreach ($this->products as $product) {
				$summary += $product->pivot->summary;
			}
		}

		return $summary;
	}

	public function getProductsCountAttribute()
	{
		$count = 0;

		if (isset($this->products)) {
			$count = $this->products->count();
		}

		return $count;
	}
}
