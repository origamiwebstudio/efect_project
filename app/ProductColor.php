<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
	protected $table = 'product_colors';
	public $timestamps = false;

	public static function isColor($hex) {
		return preg_match('/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/', $hex);
	}
}
