<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\ProductCategory;

AdminSection::registerModel(ProductCategory::class, function (ModelConfiguration $model) {

	$model->enableAccessCheck();
	$model->setTitle('Kategorie produktów');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->setColumns([
			AdminColumn::text('id', '#')
				->setWidth(100),

			AdminColumn::text('name', 'Nazwa'),
		]);

		$display->setOrder([[0, 'asc']]);

		$display->paginate(15);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		$form = AdminForm::panel();
		$form->addBody(

			AdminFormElement::text('name', 'Nazwa kategorii')
				->required()
		);

		return $form;
	});

});