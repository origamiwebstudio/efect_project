<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Permission;

AdminSection::registerModel(Permission::class, function (ModelConfiguration $model) {
	$model->enableAccessCheck();
	$model->setTitle('Uprawnienia');

	$model->onDisplay(function () {
		$display = AdminDisplay::table()->setColumns([
			AdminColumn::text('display_name')->setLabel('Wyświetlana nazwa'),
			AdminColumn::text('name')->setLabel('Nazwa'),
			AdminColumn::text('description')->setLabel('Opis'),
		]);
		$display->paginate(50);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		return $form = AdminForm::panel()->addBody(
			AdminFormElement::text('name', 'Nazwa')->required(),
			AdminFormElement::text('display_name', 'Wyświetlana nazwa')->required(),
			AdminFormElement::textarea('description', 'Opis')
		);
	});

});