<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\SliderImage;

AdminSection::registerModel(SliderImage::class, function (ModelConfiguration $model) {

	$model->deleting(function(ModelConfiguration $model, SliderImage $sliderImage) {
		$sliderImage->beforeDeleting();
	});

	$model->enableAccessCheck();

	$model->setTitle('Slider');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->setColumns([
			AdminColumn::text('id', '#')
				->setWidth(80),
			AdminColumn::image('link', 'Zdjęcie')
				->setHtmlAttribute('class', 'hidden-sm hidden-xs')
				->setImageWidth('200px')
				->setWidth(220)
				->setOrderable(false),

			AdminColumn::text('order_', 'Kolejność')
				->setHtmlAttribute('class', 'text-center')
				->setWidth(100),

			AdminColumn::text('name', 'Nazwa'),
		]);

		$display->setOrder([[0, 'asc']]);

		$display->paginate(15);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		$form = AdminForm::panel();
		$form->addBody(

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::text('order_', 'Kolejność')
						->required()
				], 2)
				->addColumn([
					AdminFormElement::text('name', 'Nazwa zdjęcia')
						->required()
				], 10)
				->addColumn([
					AdminFormElement::image('link', 'Zdjęcie')
						->required()
				], 12)
		);

		return $form;
	});

});