<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Advantage;

AdminSection::registerModel(Advantage::class, function (ModelConfiguration $model) {

	$model->enableAccessCheck();
	$model->setTitle('Przewagi');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->setColumns([
			AdminColumn::text('order_', 'Kolejność')
				->setHtmlAttribute('class', 'text-center')
				->setWidth(100),

			AdminColumn::text('name', 'Nazwa'),

			AdminColumn::text('text', 'Tekst'),
		]);

		$display->setOrder([[0, 'asc']]);

		$display->paginate(15);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		$form = AdminForm::panel();
		$form->addBody(

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::text('order_', 'Kolejność')
						->required()
				], 2)
				->addColumn([
					AdminFormElement::text('name', 'Nazwa')
						->required()
				], 10)
				->addColumn([
					AdminFormElement::text('text', 'Opis')
						->required()
				], 12)
		);

		return $form;
	});

});