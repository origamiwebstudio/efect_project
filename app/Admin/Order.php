<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Order;

AdminSection::registerModel(Order::class, function (ModelConfiguration $model) {

//	$model->deleting(function(ModelConfiguration $model, SliderImage $sliderImage) {
//		$sliderImage->beforeDeleting();
//	});

	$model->disableDeleting();
	$model->enableAccessCheck();

	$model->setTitle('Zamówienia');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->with('cart');
		$display->setApply(function ($query) {
			$query->where('order_status', '<>', 'processing');
		});

//		dd($display);

		$display->setColumns([
//			AdminColumn::text('id', '#')
//				->setWidth(50),
			AdminColumn::custom('Status')
				->setCallback(function ($instance) {
					$result = '-';

					switch ($instance->order_status) {
						case 'done':
							$result = '<span class="label label-success">Gotowe</span>';
							break;
						case 'realization':
							$result = '<span class="label label-info">Realizacja</span>';
							break;
						case 'processing':
							$result = '<span class="label label-warning">Przetwarzanie</span>';
							break;
						case 'canceled':
							$result = '<span class="label label-danger">Anulowane</span>';
							break;
					}

					return $result;
				}),
			AdminColumn::text('initials', 'Klient')
				->setOrderable(function($query, $direction) {
					$query->orderBy('name', $direction);
					$query->orderBy('surname', $direction);
				}),
			AdminColumn::text('email', 'Email'),
			AdminColumn::text('phone', 'Telefon'),
			AdminColumn::text('address', 'Adres')
				->setOrderable(function($query, $direction) {
					$query->orderBy('country', $direction);
					$query->orderBy('locality', $direction);
					$query->orderBy('street', $direction);
				}),
			AdminColumn::text('products_count', 'Ilość')
				->setOrderable(false),
			AdminColumn::text('products_summary', 'Kwota')
				->setOrderable(false),
			AdminColumn::datetime('created_at', 'Data')
				->setFormat('d.m.y H:i'),

		]);

		$display->setOrder([[7, 'desc']]);

		$display->paginate(15);

		return $display;
	});

	$model->onEdit(function ($id) {
		$form = AdminForm::panel();
		$order = Order::find($id);

		$form->addBody(

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::view('admin.order-info'),
				], 12)
				->addColumn([
					$order->order_status == 'canceled'
						? AdminFormElement::html('<span style="color: red">Zamówienie anulowane przez użytkownika</span>')
						: AdminFormElement::select('order_status', 'Status zamówienia:')
						->setOptions([
							'realization' => 'Realizacja',
							'done'        => 'Gotowe',
						])->required()
				], 12)
				->addColumn([
					AdminFormElement::view('admin.order-products'),
				], 12)
		);

		return $form;
	});

});