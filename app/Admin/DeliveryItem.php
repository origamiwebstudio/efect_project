<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\DeliveryItem;

AdminSection::registerModel(DeliveryItem::class, function (ModelConfiguration $model) {
	$model->enableAccessCheck();
	$model->setTitle('Metoda dostawy');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->setColumns([
			AdminColumn::text('id', '#')
				->setWidth(100),
			AdminColumn::text('price', 'Cena')
				->setWidth(100),
			AdminColumn::text('name', 'Nazwa'),
		]);

		$display->setOrder([[0, 'asc']]);

		$display->paginate(15);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		$form = AdminForm::panel();
		$form->addBody(

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::text('price', 'Cena')
						->required()
				], 2)
				->addColumn([
					AdminFormElement::text('name', 'Nazwa')
						->required()
				], 10)
		);

		return $form;
	});

});