<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\ProductColor;

AdminSection::registerModel(ProductColor::class, function (ModelConfiguration $model) {

	$model->enableAccessCheck();
	$model->setTitle('Kolory produktów');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->setColumns([
			AdminColumn::text('id', '#')
				->setWidth(100),

			AdminColumn::custom('Kolor')
				->setWidth(40)
				->setCallback(function ($instance) {
					return '<div style="width:24px;height:24px;border-radius:50%;display:inline-block;margin: 0 1px;background: ' . $instance->hex . ';"></div>';
				}),

			AdminColumn::text('name', 'Nazwa'),
		]);

		$display->setOrder([[0, 'asc']]);

		$display->paginate(30);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		$hex_color = '<div class="alert bg-warning text-warning alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span></button><small>Wybierz <a style="color: #F97979; font-weight: 800" target="_blank" href="http://www.google.com/search?q=hex+color">tutaj</a> dowolny color, skopiuj kod <strong>podobny do #093377</strong> i wklej w pole <strong>Kolor hex</strong></small></div>';

		$form = AdminForm::panel();
		$form->addBody(
			AdminFormElement::html($hex_color),

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::text('hex', 'Kolor hex (n.p. #FF9343)')
						->required(),
				], 4)
				->addColumn([
					AdminFormElement::text('name', 'Nazwa koloru')
						->required(),
				], 8)
		);

		return $form;
	});

});