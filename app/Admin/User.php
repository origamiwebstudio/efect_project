<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\User;

AdminSection::registerModel(User::class, function (ModelConfiguration $model) {

	$model->enableAccessCheck();
	$model->disableDeleting();

	$model->setTitle('Użytkownicy');

	$model->onDisplay(function () {
		$display = AdminDisplay::table()
			->setColumns([
				AdminColumn::text('name')
					->setLabel('Imię'),
				AdminColumn::text('surname')
					->setLabel('Nazwisko'),
				AdminColumn::text('phone')
					->setLabel('Telefon'),
				AdminColumn::text('email')
					->setLabel('Email'),
				AdminColumn::lists('roles.display_name', 'Role')
			]);

		$display->paginate(30);

		return $display;
	});

	$model->onEdit(function () {
		$form = AdminForm::panel()->addBody(

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::text('name', 'Imię')->setReadOnly(true),
				], 3)
				->addColumn([
					AdminFormElement::text('surname', 'Nazwisko')->setReadOnly(true),
				], 3)
				->addColumn([
					AdminFormElement::text('email', 'Email')->setReadOnly(true),
				], 3)
				->addColumn([
					AdminFormElement::text('phone', 'Telefon')->setReadOnly(true),
				], 3)
				->addColumn([
					#Developer
					auth()->user()->hasRole('developer')

						#Can edit roles and set developer role
						? AdminFormElement::multiselect('roles', 'Role')
						->setModelForOptions('App\Role')
						->setDisplay('display_name')

						#Owner
						: (auth()->user()->hasRole('owner')

							#Can edit roles but can't set developer role
							? AdminFormElement::multiselect('roles', 'Role')
							->setModelForOptions('App\Role')
							->setLoadOptionsQueryPreparer(function ($item, $query) {
								return $query->where('id', '!=', 1);
							})
							->setDisplay('display_name')

							#Manager only can see roles
							: AdminFormElement::multiselect('roles', 'Role')
								->setModelForOptions('App\Role')
								->setDisplay('display_name')
								->setReadOnly(true))
				], 6)
		);

		return $form;
	});

});