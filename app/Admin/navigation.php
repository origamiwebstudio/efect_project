<?php
return [
	[
		'title'    => 'Produkty',
		'priority' => 1,
		'icon'     => 'fa fa-female',
		'model'    => \App\Product::class,
	],
	[
		'priority' => 2,
		'title'    => 'Zamowienia',
		'icon'     => 'fa fa-book',
		'model'    => \App\Order::class,
		'badge'    => new \SleepingOwl\Admin\Navigation\Badge(function () {
			$count = \App\Order::where('order_status', '=', 'realization')->count();

			return $count;
		}),
	],
	[
		'title'    => 'Ustawienia',
		'priority' => 3,
		'icon'     => 'fa fa-cog',
		'pages'    => [
			[
				'title' => 'Slider',
				'icon'  => 'fa fa-picture-o',
				'model' => \App\SliderImage::class,
			],
			[
				'title' => 'Przewagi',
				'icon'  => 'fa fa-trophy',
				'model' => \App\Advantage::class,
			],
			[
				'title' => 'Kategorie produktów',
				'icon'  => 'fa fa-folder',
				'model' => \App\ProductCategory::class,
			],
			[
				'title' => 'Kolory produktów',
				'icon'  => 'fa fa-circle',
				'model' => \App\ProductColor::class,
			],
			[
				'title' => 'Metody dostawy',
				'icon'  => 'fa fa-arrows',
				'model' => \App\DeliveryItem::class,
			],
		],
	],
	[
		'title'    => 'Użytkownicy',
		'priority' => 100,
		'icon'     => 'fa fa-users',
		'model'    => \App\User::class,
	],
	[
		'title' => 'Role',
		'icon'  => 'fa fa-graduation-cap',
		'model' => \App\Role::class,
	],
	[
		'title' => 'Uprawnienia',
		'icon'  => 'fa fa-key',
		'model' => \App\Permission::class,
	],
];