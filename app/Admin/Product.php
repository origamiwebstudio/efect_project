<?php

use Illuminate\Pagination\PaginationServiceProvider;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Product;

AdminSection::registerModel(Product::class, function (ModelConfiguration $model) {

	$model->deleting(function (ModelConfiguration $model, Product $product) {
		$product->beforeDeleting();
	});

	$model->enableAccessCheck();
	$model->disableDeleting();

	$model->setTitle('Produkty');

	$model->onDisplay(function () {
		$display = AdminDisplay::datatables();
		$display->setColumns([

			AdminColumn::image('main_image.link', 'Icon')
				->setHtmlAttribute('class', 'hidden-sm hidden-xs')
				->setImageWidth('60px')
				->setOrderable(false),

			AdminColumn::text('id', '#'),

			AdminColumn::text('category.name', 'Kategoria')
				->append(
					AdminColumn::filter('category_id')
				)->setOrderable(false),
			AdminColumn::text('admin_price', 'Cena')
				->setOrderable(function ($query, $direction) {
					$query->orderBy('price', $direction);
				}),

			AdminColumn::custom('Nowa cena')
				->setCallback(function ($instance) {
					return '<div style="color: #FF9343; text-align: center">' . ($instance->offer_price > 0 ? $instance->offer_price : '-') . '</div>';
				}),

			AdminColumn::text('name', 'Nazwa'),

			$new = AdminColumn::custom('Nowość')
				->setHtmlAttribute('class', 'text-center')
				->setCallback(function ($instance) {
					return $instance->novelty ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
				})
				->setOrderable(function ($query, $direction) {
					$query->orderBy('novelty', $direction);
				}),

			$visible = AdminColumn::custom('Widoczny')
				->setHtmlAttribute('class', 'text-center')
				->setCallback(function ($instance) {
					return $instance->visible ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
				})
				->setOrderable(function ($query, $direction) {
					$query->orderBy('visible', $direction);
				}),

			$color = AdminColumn::custom('Kolory')
				->setHtmlAttribute('class', 'text-center')
				->setWidth(120)
				->setCallback(function ($instance) {
					$tmp = '<style>.color-item{width:24px;height:24px;border-radius:50%;display:inline-block;margin: 0 1px;border-bottom: 1px solid #FF9343}</style>';
					$counter = 1;

					foreach ($instance->colors as $color) {
						$tmp .= '<div class="color-item" style="background: ' . $color->hex . ';"></div>';

						if ($counter % 4 == 0) {
							$tmp .= '<br>';
						}

						$counter++;
					}

					return $tmp;
				}),

			$size = AdminColumn::custom('Rozmiary')
				->setHtmlAttribute('class', 'text-center')
				->setCallback(function ($instance) {
					$tmp = '<style>
										.size-item{display: inline-flex;align-items:center;justify-content:center;width:28px;height:28px;background:#fff;border:1px solid #FF9343;color:#FF9343;font-size:11px;border-radius:50%;position:relative;margin: 4px 1px;}
										.size-item-label{position:absolute;right:-5px;top:-12px;background:#fff;color:#888;border:1px solid #ff9343;width:20px;height:20px;border-radius:50%;display:flex;align-items:center;justify-content:center;font-size:10px}
									</style>';
					$counter = 1;

					for ($i = 34; $i <= 56; $i += 2) {
						if ($instance['s' . $i] > 0) {
							$tmp .= '<div class="size-item">' . $i . '<span class="size-item-label">' . $instance['s' . $i] . '</span></div>';

							if ($counter % 4 == 0) {
								$tmp .= '<br>';
							}

							$counter++;
						}
					}

					return $tmp;
				}),
		]);

		$new->getHeader()->setHtmlAttribute('class', 'text-center');
		$visible->getHeader()->setHtmlAttribute('class', 'text-center');
		$color->getHeader()->setHtmlAttribute('class', 'text-center');
		$size->getHeader()->setHtmlAttribute('class', 'text-center');

		$display->setOrder([[1, 'asc']]);

		$display->setFilters(
			AdminDisplayFilter::related('category_id', 'Kategoria')
		);

		$display->paginate(15);

		return $display;
	});

	$model->onCreateAndEdit(function () {
		$form = AdminForm::panel();
		$form->addBody(

			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::select('category_id', 'Kategoria')
						->setModelForOptions('App\ProductCategory')
						->setDisplay('name')
						->required(),
				], 2)
				->addColumn([
					AdminFormElement::text('name', 'Nazwa')
						->required(),
				], 4)
				->addColumn([
					AdminFormElement::text('admin_price', 'Cena')
						->required(),
				], 2)
				->addColumn([
					AdminFormElement::text('offer_value', 'Rabat')
						->setDefaultValue(0),
				], 2)
				->addColumn([
					AdminFormElement::select('offer_type', 'Typ rabatu')
						->setOptions([
							'percent'      => 'procent',
							'fix_discount' => 'stały rabat',
							'fix_price'    => 'stała cena',
						]),
				], 2)
				->addColumn([
					AdminFormElement::multiselect('colors', 'Kolory')
						->setModelForOptions('App\ProductColor')
						->setDisplay('name')
						->required(),
				], 12)
		);

		$form->addBody(


			AdminFormElement::columns()
				->addColumn([
					AdminFormElement::text('s34', '34')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s36', '36')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s38', '38')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s40', '40')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s42', '42')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s44', '44')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s46', '46')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s48', '48')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s50', '50')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s52', '52')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s54', '54')
						->setDefaultValue(0),
				], 1)
				->addColumn([
					AdminFormElement::text('s56', '56')
						->setDefaultValue(0),
				], 1)
		);

		$form->addBody(
			AdminFormElement::images('product_images', 'Images')
				->setAfterSaveCallback(function ($value, $model) {
					$model->processImages($value);
				})
				->required()
		);

		$form->addBody(
			AdminFormElement::checkbox('novelty', 'Nowość'),
			AdminFormElement::checkbox('visible', 'Widoczny')
		);

		return $form;
	});

});