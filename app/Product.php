<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Product extends Model
{
	protected $table = 'products';

	#Before removing the product - delete its images
	public function beforeDeleting()
	{
		foreach ($this->images as $image) {
			File::delete(public_path($image['link']));
		}

		$this->images()->delete();
	}

	#
	#
	#
	#--------------------------------------------------------------------

	public function images()
	{
		return $this->hasMany(ProductImage::class);
	}

	public function category()
	{
		return $this->hasOne(ProductCategory::class, 'id', 'category_id');
	}

	public function colors()
	{
		return $this->belongsToMany(ProductColor::class, 'product_color', 'product_id', 'color_id');
	}

	public function favorites()
	{
		return $this->belongsToMany(User::class, 'favorites', 'product_id', 'user_id');
	}

	#
	#
	#
	#--------------------------------------------------------------------
	public static function productsMaxPrice()
	{
		return round((1 + config('efect.surcharge.percents')) * Product::max('price') + config('efect.surcharge.fixed'), 2, PHP_ROUND_HALF_EVEN);
	}

	public static function getSpecialFilter($filter)
	{
		if (in_array($filter, ['sale', 'new'])) {
			return $filter;
		} elseif ($category = ProductCategory::where('name', '=', $filter)->first()) {
			return $category->id;
		}

		return null;
	}

	#
	#
	#
	#--------------------------------------------------------------------
	public function getUidAttribute()
	{
		return str_pad($this->attributes['id'], 6, "0", STR_PAD_LEFT);
	}

	public function getAvailabilityAttribute()
	{
		for ($i = 34; $i <= 56; $i += 2) {
			if ($this->attributes['s' . $i] > 0)
				return true;
		}

		return false;
	}

	public function getMainImageAttribute()
	{
		return $this['images']->first();
	}

	public function getPriceAttribute()
	{
		return round((1 + config('efect.surcharge.percents')) * $this->attributes['price'] + config('efect.surcharge.fixed'), 2, PHP_ROUND_HALF_EVEN);
	}

	public function getAdminPriceAttribute()
	{
		return $this->attributes['price'];
	}

	public function setAdminPriceAttribute($price)
	{
		$this->attributes['price'] = $price;
	}

	public function getOfferPriceAttribute()
	{
		$offer_price = null;

		if ($this['offer_value'] > 0) {
			switch ($this['offer_type']) {
				case 'percent':
					$offer_price = round((1 - ($this['offer_value'] / 100)) * $this['price'], 2, PHP_ROUND_HALF_EVEN);
					break;
				case 'fix_discount':
					$offer_price = round($this['price'] - $this['offer_value'], 2, PHP_ROUND_HALF_EVEN);
					break;
				case 'fix_price':
					$offer_price = round((1 + config('efect.surcharge.percents')) * $this['offer_value'] + config('efect.surcharge.fixed'), 2, PHP_ROUND_HALF_EVEN);
					break;
			}
		}

		return $offer_price;
	}

	public function getOfferValueAttribute()
	{
		if ($this->attributes['offer_value'] > 0) {
			if ($this->attributes['offer_type'] == 'percent') {
				return round($this->attributes['offer_value']);
			}

			return $this->attributes['offer_value'];
		}

		return null;
	}

	public function getProductImagesAttribute()
	{
		$images = ProductImage::where('product_id', '=', $this['id'])->select('link')->get();
		$links = [];

		foreach ($images as $image) {
			$links [] = $image['link'];
		}

		return $links;
	}

	public function setProductImagesAttribute($images)
	{
		return true;
	}

	public function getIsFavoriteAttribute()
	{
		return auth()->user() && auth()->user()->favorites->contains($this->attributes['id']) ? true : false;
	}

	#
	#
	#
	#--------------------------------------------------------------------

	public function processImages($new_images)
	{
		$p_images = ProductImage::where('product_id', '=', $this['id'])->select('link')->get();

		$old_images = [];

		foreach ($p_images as $p_image) {
			$old_images [] = $p_image['link'];
		}

		$intersect_images = array_intersect($old_images, $new_images);
		$images_to_del = array_diff($old_images, $intersect_images);

		foreach ($images_to_del as $item) {
			File::delete(public_path($item));
			ProductImage::where('link', '=', $item)->first()->delete();
		}

		$images_to_save = array_diff($new_images, $intersect_images);

		foreach ($images_to_save as $item) {
			$image = new ProductImage();
			$image['product_id'] = $this['id'];
			$image['link'] = $item;
			$image->save();
		}
	}

	public function saveViewedProducts()
	{
		$viewed = session('viewed_products') ? session('viewed_products') : collect();

		if (!$viewed->contains('id', $this->attributes['id'])) {
			$viewed->push(['id' => $this->attributes['id'], 'timestamp' => Carbon::now()->timestamp]);

			$this->attributes['views'] = $this->attributes['views'] + 1;
			$this->save();
		}

		session()->put('viewed_products', $viewed);
	}

	public function getViewedProducts($limit = 3)
	{
		$viewed = session('viewed_products') ? session('viewed_products') : collect();

		if (isset($this->attributes['id']))
			$viewed = $viewed->where('id', '!=', $this->attributes['id'])->sortByDesc('timestamp')->pluck('id');
		else
			$viewed = $viewed->sortByDesc('timestamp')->pluck('id');

		if (count($viewed) > 0) {
			return Product::where('visible', true)->whereIn('id', $viewed->reverse())->limit($limit)->inRandomOrder()->get();
		}

		return null;
	}

	public function checkSizeAvailability($size = null)
	{
		return in_array($size, config('efect.sizes')) && count($this->attributes['s' . $size]) > 0 ? $this->attributes['s' . $size] : false;
	}

	public function getAvailableSizes($return_type = 'array')
	{
		$result = [];

		if (in_array($return_type, ['array', 'string'])) {
			foreach (config('efect.sizes') as $size) {
				if ($this->attributes['s' . $size] > 0)
					array_push($result, $size);
			}

			switch ($return_type) {
				case 'string':
					$result = implode(', ', $result);
					break;
			}
		}

		return $result;
	}
}
