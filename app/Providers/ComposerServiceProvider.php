<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
	public function boot()
	{
		view()->composer('pages.profile.*','App\Http\ViewComposers\ProfileComposer');
	}

	public function register()
	{
		//
	}
}
