<?php

namespace App\Providers;

use App\Advantage;
use App\DeliveryItem;
use App\Order;
use App\Permission;
use App\Policies\AdvantagePolicy;
use App\Policies\DeliveryItemPolicy;
use App\Policies\OrderPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\ProductCategoryPolicy;
use App\Policies\ProductColorPolicy;
use App\Policies\ProductPolicy;
use App\Policies\RolePolicy;
use App\Policies\SliderImagePolicy;
use App\Policies\UserPolicy;
use App\Product;
use App\ProductCategory;
use App\ProductColor;
use App\Role;
use App\SliderImage;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
	protected $policies = [
		'App\Model'            => 'App\Policies\ModelPolicy',
		User::class            => UserPolicy::class,
		Role::class            => RolePolicy::class,
		Permission::class      => PermissionPolicy::class,
		Advantage::class       => AdvantagePolicy::class,
		ProductCategory::class => ProductCategoryPolicy::class,
		ProductColor::class    => ProductColorPolicy::class,
		SliderImage::class     => SliderImagePolicy::class,
		DeliveryItem::class    => DeliveryItemPolicy::class,
		Product::class         => ProductPolicy::class,
		Order::class           => OrderPolicy::class,
	];

	public function boot()
	{
		$this->registerPolicies();

		//
	}
}
