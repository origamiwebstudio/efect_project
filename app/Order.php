<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	public function cart()
	{
		return $this->hasOne(Cart::class, 'id', 'cart_id');
	}

	public static function getCountAllNewOrders()
	{
		return Order::where('order_status', 'realization')->count();
	}

	public static function getCountUserNewOrders()
	{
		if (auth()->user())
			return Order::where([
				['user_id', '=', auth()->user()->id],
				['order_status', '=', 'realization'],
			])->count();

		return 0;
	}

	public function getInitialsAttribute()
	{
		return $this->attributes['name'] . ' ' . $this->attributes['surname'];
	}

	public function getAddressAttribute()
	{
		return $this->attributes['country']
			. ', ' . $this->attributes['postal_code']
			. ' ' . $this->attributes['locality']
			. ', ' . $this->attributes['street']
			. ' ' . $this->attributes['house_number'];
	}

	public function getProductsCountAttribute()
	{
		$count = 0;
		if (isset($this->cart)) {
			$quantity = $this->cart->products()->sum('quantity');
			$count = $this->cart->products_count . ' (' . $quantity . ')';
		}

		return $count;
	}

	public function getProductsSummaryAttribute()
	{
		$summary = 0;
		if (isset($this->cart)) {
			$summary = $this->cart->products_summary_price;
		}

		return $summary;
	}

	public function getStatusAttribute()
	{
		switch ($this->attributes['order_status']) {
			case 'processing':
				$status = 'przetwarzanie';
				break;
			case 'canceled':
				$status = 'anulowane';
				break;
			case 'done':
				$status = 'gotowe';
				break;
			case 'realization':
				$status = 'realizacja';
				break;
			default:
				$status = 'none';
		}

		return $status;
	}
}
