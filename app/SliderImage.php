<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class SliderImage extends Model
{
	protected $table = 'slider_images';
	public $timestamps = false;

	#Before removing the product - delete its images
	public function beforeDeleting()
	{
		File::delete(public_path($this['link']));
	}

	#
	#
	#
	#--------------------------------------------------------------------

	//code here
}
