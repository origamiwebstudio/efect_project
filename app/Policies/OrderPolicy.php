<?php

namespace App\Policies;

use App\Order;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, Order $order)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, Order $order)
	{
		if (auth()->check() && $user->can('order-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		return false;
	}

	public function edit(User $user, Order $order)
	{
		if (auth()->check() && $user->can('order-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, Order $order)
	{
		return false;
	}
}
