<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, Product $product)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, Product $product)
	{
		if (auth()->check() && $user->can('product-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		if (auth()->check() && $user->can('product-create')) {
			return true;
		}

		return false;
	}

	public function edit(User $user, Product $product)
	{
		if (auth()->check() && $user->can('product-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, Product $product)
	{
		return false;
	}
}
