<?php

namespace App\Policies;

use App\DeliveryItem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeliveryItemPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, DeliveryItem $delivery_item)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, DeliveryItem $delivery_item)
	{
		if (auth()->check() && $user->can('delivery-type-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		if (auth()->check() && $user->can('delivery-type-create')) {
			return true;
		}

		return false;
	}

	public function edit(User $user, DeliveryItem $delivery_item)
	{
		if (auth()->check() && $user->can('delivery-type-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, DeliveryItem $delivery_item)
	{
		if (auth()->check() && $user->can('delivery-type-delete') && $delivery_item['id'] != 1) {
			return true;
		}

		return false;
	}
}
