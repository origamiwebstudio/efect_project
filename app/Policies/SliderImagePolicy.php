<?php

namespace App\Policies;

use App\SliderImage;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SliderImagePolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, SliderImage $slider_image)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, SliderImage $slider_image)
	{
		if (auth()->check() && $user->can('slider-image-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		if (auth()->check() && $user->can('slider-image-create')) {
			return true;
		}

		return false;
	}

	public function edit(User $user, SliderImage $slider_image)
	{
		if (auth()->check() && $user->can('slider-image-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, SliderImage $slider_image)
	{
		if (auth()->check() && $user->can('slider-image-delete')) {
			return true;
		}

		return false;
	}
}
