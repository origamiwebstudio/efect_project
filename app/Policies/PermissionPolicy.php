<?php

namespace App\Policies;

use App\Permission;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, Permission $permission)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, Permission $permission)
	{
		return false;
	}

	public function create(User $user)
	{
		return false;
	}

	public function edit(User $user, Permission $permission)
	{
		return false;
	}

	public function delete(User $user, Permission $permission)
	{
		return false;
	}
}
