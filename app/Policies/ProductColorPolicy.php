<?php

namespace App\Policies;

use App\ProductColor;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductColorPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, ProductColor $product_color)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, ProductColor $product_color)
	{
		if (auth()->check() && $user->can('product-color-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		if (auth()->check() && $user->can('product-color-create')) {
			return true;
		}

		return false;
	}

	public function edit(User $user, ProductColor $product_color)
	{
		if (auth()->check() && $user->can('product-color-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, ProductColor $product_color)
	{
		return false;
	}
}
