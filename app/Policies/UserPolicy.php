<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, User $item)
	{
		if ($ability != 'create' && $user->hasRole('developer'))
			return true;
	}

	public function display(User $user, User $item)
	{
		if (auth()->check() && $user->can('user-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		return false;
	}

	public function edit(User $user, User $item)
	{
		if (auth()->check() && $user->can('user-edit') && !$item->hasRole('developer')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, User $item)
	{
		return false;
	}
}
