<?php

namespace App\Policies;

use App\ProductCategory;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductCategoryPolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, ProductCategory $product_category)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, ProductCategory $product_category)
	{
		if (auth()->check() && $user->can('product-category-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		if (auth()->check() && $user->can('product-category-create')) {
			return true;
		}

		return false;
	}

	public function edit(User $user, ProductCategory $product_category)
	{
		if (auth()->check() && $user->can('product-category-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, ProductCategory $product_category)
	{
		return false;
	}
}
