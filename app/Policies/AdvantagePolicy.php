<?php

namespace App\Policies;

use App\Advantage;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvantagePolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, Advantage $advantage)
	{
		if ($user->hasRole('developer')) {
			return true;
		}
	}

	public function display(User $user, Advantage $advantage)
	{
		if (auth()->check() && $user->can('advantage-display')) {
			return true;
		}

		return false;
	}

	public function create(User $user)
	{
		return false;
	}

	public function edit(User $user, Advantage $advantage)
	{
		if (auth()->check() && $user->can('advantage-edit')) {
			return true;
		}

		return false;
	}

	public function delete(User $user, Advantage $advantage)
	{
		return false;
	}
}
