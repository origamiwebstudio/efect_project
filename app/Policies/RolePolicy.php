<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
	use HandlesAuthorization;

	public function before(User $user, $ability, Role $role)
	{
		if ($user->hasRole('developer'))
			return true;
	}

	public function display(User $user, Role $role)
	{
		return false;
	}

	public function create(User $user)
	{
		return false;
	}

	public function edit(User $user, Role $role)
	{
		return false;
	}

	public function delete(User $user, Role $role)
	{
		return false;
	}
}
