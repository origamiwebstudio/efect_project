<?php namespace App\Traits;

trait GeneratesMessages
{
	/**
	 * @param null $message_code
	 * @param null $type
	 * @param null $title
	 * @param null $message
	 * @param null $links
	 * @return $this
	 */
	protected function getMessagePage($message_code = null, $type = null, $title = null, $message = null, $links = null)
	{
		switch ($message_code) {
			case 1:
				$data = [
					'type'    => 'error',
					'title'   => 'Pusty koszyk',
					'message' => 'Nie masz żadnego produktu w koszyku. Dodaj chociaż jeden produkt i wracaj',
					'links'   => [
						[
							'type'  => 'button',
							'title' => 'kolekcja',
							'url'   => route('products'),
						],
						[
							'title' => 'ulubione',
							'url'   => route('profile-favorites'),
						],
					],
				];
				break;

			case 2:
				$data = [
					'type'    => 'error',
					'title'   => 'Nie ma sposobów dostawy',
					'message' => 'Oczekiwany jest chociaż jeden sposób dostawy. Można go dodać w panelu administratora',
				];
				break;

			case 3:
				$data = [
					'type'    => 'success',
					'title'   => 'dziękujemy!',
					'message' => 'Otrzymaliśmy twoją płatność i w najbliższy czas zrealizujemy twoje zamówienie',
					'links'   => [
						[
							'type'  => 'button',
							'title' => 'moje zamówienia',
							'url'   => route('profile-orders'),
						],
						[
							'title' => 'strona główna',
							'url'   => route('home'),
						],
					],
				];
				break;

			case 4:
				$data = [
					'type'    => 'success',
					'title'   => 'Wysłano email',
					'message' => 'Na twój adres mejlowy wysłaliśmy email z instrukcją zmiany hasła',
				];
				break;

			case 5:
				$data = [
					'type'    => 'success',
					'title'   => 'Zmieniono hasło',
					'message' => 'Zmieniliśmy twoje hasło na nowe',
				];
				break;

			case 6:
				$data = [
					'type'    => 'error',
					'title'   => 'Nie mamy tyle produktów',
					'message' => 'Przepraszamy, ale nie mamy wybranych produktów w takiej ilości',
					'links'   => [
						[
							'type'  => 'button',
							'title' => 'koszyk',
							'url'   => route('profile-basket'),
						],
						[
							'title' => 'strona główna',
							'url'   => route('home'),
						],
					],
				];
				break;

			default:
				$data = [
					'type'    => 'error',
					'title'   => 'Wystąpił błąd',
					'message' => 'Przepraszamy, ale mamy problem. Sprobój ponownie za kilka minut',
				];
		}

		if ($type) $data['type'] = $type;
		if ($title) $data['title'] = $title;
		if ($message) $data['message'] = $message;
		if ($links) $data['links'] = $links;

		return view('pages.message')->with($data);
	}

	public function getJsonMessage($text = null, $type = null, $timeout = 2000, $title = null)
	{
		switch ($type) {
			case 'success':
				$class = 'fixed-messages__item_success';
				break;
			case 'warning':
				$class = 'fixed-messages__item_error';
				break;
			default:
				$class = false;
		}

		$message['text'] = $text ?: trans('errors-messages.error_occured');
		if ($class) $message['class'] = $class;
		if ($title) $message['title'] = $title;
		$message['timeout'] = $timeout;

		return $message;
	}
}