<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class ProfileComposer
{
	private $data = [];

	public function __construct()
	{
		$this->data = [
			'styles'  => config('resources.profile.styles'),
			'scripts' => config('resources.profile.scripts'),
		];
	}

	public function compose(View $view)
	{
		$view->with([
			'styles'  => $this->data['styles'],
			'scripts' => $this->data['scripts'],
		]);
	}
}