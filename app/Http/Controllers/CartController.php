<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\ProductColor;
use App\Traits\GeneratesMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
	use GeneratesMessages;

	public function __construct()
	{
		$this->middleware('auth', ['except' => ['addProduct', 'getProductsCount']]);
	}

	#
	#------------------------------------------------------------------------
	#

	public static function getSameProductsQuantity($product_id, $size)
	{
		if ($user = auth()->user()) {
			if ($user->active_cart && $products = $user->active_cart->products->where('id', '=', $product_id)) {
				$quantity = 0;

				foreach ($products as $product) {
					if ($product->pivot->size == $size) {
						$quantity += $product->pivot->quantity;
					}
				}

				return $quantity;
			}

			return 0;
		}

		$guest_cart = session('guest_cart') ?: collect();

		return $guest_cart->where('product_id', '=', $product_id)->where('size', '=', $size)->sum('quantity');
	}

	private function checkAbilityToAdd($product_id, $size, $color, $quantity)
	{
		if ($product = Product::find($product_id)) {
			if (ProductColor::isColor($color) && in_array($size, config('efect.sizes'))) {
				$same_products_quantity = $this->getSameProductsQuantity($product_id, $size);

				if ($product['s' . $size] >= $quantity + $same_products_quantity) {
					return true;
				}

				return [
					'status'  => false,
					'message' => $this->getJsonMessage(trans('errors-messages.we_do_not_have_so_many_products'), 'warning'),
				];
			}
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_added_to_basket'), 'warning'),
		];
	}

	private function addProductToGuestCart($product_id, $size, $color, $quantity)
	{
		$guest_cart = session('guest_cart') ?: collect();

		$guest_cart->push([
			'product_id'       => $product_id,
			'color'            => $color,
			'size'             => $size,
			'quantity'         => $quantity,
			'time_of_addition' => Carbon::now()->timestamp,
		]);

		session()->put('guest_cart', $guest_cart);

		return [
			'status'  => true,
			'message' => $this->getJsonMessage(trans('errors-messages.added_to_basket'), 'success'),
		];
	}

	private function saveGuestCart()
	{
		$guest_cart = session('guest_cart') ?: collect();

		if (count($guest_cart) > 0) {
			$cart = auth()->user()->activeCart;

			foreach ($guest_cart as $item) {
				$product = Product::find($item['product_id']);

				if ($this->getSameProductsQuantity($product->id, $item['size']) + $item['quantity'] <= $product['s' . $item['size']]) {
					$cart->products()->attach($item['product_id'], [
						'size'         => $item['size'],
						'color'        => $item['color'],
						'quantity'     => $item['quantity'],
						'actual_price' => $product->offer_price ?: $product->price,
						'summary'      => ($product->offer_price ?: $product->price) * $item['quantity'],
					]);
				}
			}

			session()->forget('guest_cart');
		}
	}

	#
	#------------------------------------------------------------------------
	#

	//POST: /basket/add
	public function addProduct(Request $request)
	{
		$product_id = $request['product_id'];
		$size = $request['size'];
		$color = $request['color'];
		$quantity = $request['quantity'];

		$ability_to_add = $this->checkAbilityToAdd($product_id, $size, $color, $quantity);

		if ($ability_to_add === true) {
			if ($user = auth()->user()) {
				if ($cart = $user->active_cart) {
					if ($product = Product::find($product_id)) {
						$cart->products()->attach($product_id, [
							'size'         => $size,
							'color'        => $color,
							'quantity'     => $quantity,
							'actual_price' => $product->offer_price ?: $product->price,
							'summary'      => ($product->offer_price ?: $product->price) * $quantity,
						]);

						return [
							'status'  => true,
							'message' => $this->getJsonMessage(trans('errors-messages.added_to_basket'), 'success'),
						];
					}
				}

				return [
					'status'  => false,
					'message' => $this->getJsonMessage(trans('errors-messages.not_added_to_basket'), 'warning'),
				];
			}

			return $this->addProductToGuestCart($product_id, $size, $color, $quantity);
		}

		return $ability_to_add;
	}

	//POST: /basket/count
	public function getProductsCount()
	{
		return Cart::getProductsCount();
	}

	//POST: /basket/sidebar
	public function getCartShortInfo()
	{
		$this->saveGuestCart();

		$cart = auth()->user()->activeCart;
		$result = [];

		if (count($cart->products) > 0) {
			$summary = 0;

			foreach ($cart->products as $item) {
				$result['products'] [] = [
					'id'       => $item->id,
					'name'     => $item->name,
					'quantity' => $item->pivot->quantity,
				];

				$summary += ($item->offer_price ? $item->offer_price : $item->price) * $item->pivot->quantity;
			}

			$result['summary'] = $summary;
		}

		return $result;
	}

	//POST: /basket/remove
	public function removeProduct(Request $request)
	{
		$uid = $request['item_uid'];
		$cart = auth()->user()->activeCart;

		if ($cart->products()->wherePivot('id', '=', $uid)->detach()) {
			return [
				'status'  => true,
				'message' => $this->getJsonMessage(trans('errors-messages.removed_from_basket'), 'success'),
			];
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_removed_from_basket'), 'warning'),
		];
	}

	#POST /basket/quantity/change
	public function changeProductQuantity(Request $request)
	{
		$uid = $request['basket_uid'];
		$quantity = $request['quantity'];
		$cart_product = DB::table('cart_product')->find($uid);


		if ($cart_product) {
			if ($product = Product::find($cart_product->product_id)) {
				if ($product['s' . $cart_product->size] >= $this->getSameProductsQuantity($product->id, $cart_product->size) + $quantity - $cart_product->quantity) {
					$cart_product = DB::table('cart_product')->where('id', '=', $uid)->update([
						'quantity'     => $quantity,
						'actual_price' => $product->offer_price ?: $product->price,
						'summary'      => ($product->offer_price ?: $product->price) * $quantity,
					]);

					if ($cart_product) {
						return [
							'status'  => true,
							'message' => $this->getJsonMessage(trans('errors-messages.changed_basket_product_quantity'), 'success'),
						];
					}
				}

				return [
					'status'  => false,
					'message' => $this->getJsonMessage(trans('errors-messages.we_do_not_have_so_many_products'), 'warning'),
				];
			}
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_changed_basket_product_quantity'), 'warning'),
		];
	}

	//GET: /profile/basket
	public function showCartPage()
	{
		$this->saveGuestCart();

		$data = [
			'title' => trans('static.my_basket') . ' - ' . trans('static.internet_shop'),
			'cart'  => auth()->user()->activeCart,
		];

		return view('pages.profile.profile-basket')->with($data);
	}

	#
	#------------------------------------------------------------------------
	#

}
