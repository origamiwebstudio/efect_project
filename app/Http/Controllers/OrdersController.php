<?php

namespace App\Http\Controllers;

use App\Cart;
use App\DeliveryItem;
use App\Order;
use App\Traits\GeneratesMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Omnipay\Omnipay;

class OrdersController extends Controller
{
	use GeneratesMessages;

	public function __construct()
	{
		$this->middleware('auth');
	}

	private function makeOrderNumber()
	{
		$offers_today = Order::whereDate('created_at', '=', Carbon::today())->count();
		$result = Carbon::now()->timestamp;
		$result .= str_pad($offers_today, 3, "0", STR_PAD_LEFT);

		return base_convert($result, 10, 32);
	}

	private function changeProductsCount(Order $order)
	{
		if (isset($order->cart->products)) {
			foreach ($order->cart->products as $product) {
				$product['sales'] = $product['sales'] + $product->pivot->quantity;
				$product['s' . $product->pivot->size] = $product['s' . $product->pivot->size] - $product->pivot->quantity;
				$product->save();
			}
		}
	}

	private function validateOrder($data)
	{
		$validator = Validator::make($data, [
			'name'           => 'required|max:255',
			'surname'        => 'required|max:255',
			'email'          => 'required|email|max:255',
			'phone'          => 'required|max:32',
			'country'        => 'required|max:255',
			'locality'       => 'required|max:255',
			'postal_code'    => 'required|max:50',
			'street'         => 'required|max:255',
			'house_number'   => 'required|max:50',
			'privacy_policy' => 'required',
			'regimen'        => 'required',
			'payment'        => 'required',
		]);

		return $validator;
	}

	private function sendSuccessEmail(Order $order)
	{
		Mail::send('emails.order-success', ['order' => $order], function ($message) use ($order) {
			$message->from('contact@efect-moda.com', 'efect-moda.com');
			$message->to($order->email)->subject('Twoje zamówienie na efect-moda.com');
		});
	}

	private function saveOrder()
	{
		$user = auth()->user();

		if ($user && $user->activeCart && ($session_order = session('order')) && ($cart = $user->activeCart)) {

			$order = new Order();

			$order['user_id'] = $user->id;
			$order['cart_id'] = $cart->id;
			$order['order_number'] = session('order.order_number');
			$order['order_status'] = 'processing';
			$order['name'] = session('order.name');
			$order['surname'] = session('order.surname');
			$order['email'] = session('order.email');
			$order['phone'] = session('order.phone');
			$order['country'] = session('order.country');
			$order['locality'] = session('order.locality');
			$order['postal_code'] = session('order.postal_code');
			$order['street'] = session('order.street');
			$order['house_number'] = session('order.house_number');
			$order['delivery_type'] = session('order.delivery_type');
			$order['delivery_price'] = session('order.delivery_price');
			$order['payment_type'] = session('order.payment_type');
			$order['payment_status'] = 'none';

			$order->save();

			$cart->enabled = false;
			$cart->save();

			session()->put('order_in_process', session('order.order_number'));
			session()->forget('order');

			return true;
		}

		return false;
	}

	private function restoreCart($cart_id)
	{
		$user = auth()->user();

		if ($user && ($old_cart = $user->activeCart) && ($cart = Cart::find($cart_id))) {
			if ($old_cart->products->count() > 0) {
				foreach ($old_cart->products as $product) {
					$same_products = $cart->products()->where('products.id', '=', $product->id)->wherePivot('size', '=', $product->pivot->size)->get();
					$sp_quantity = 0;

					foreach ($same_products as $same_product) {
						$sp_quantity += $same_product->pivot->quantity;
					}

					if ($sp_quantity + CartController::getSameProductsQuantity($product->id, $product->pivot->size) <= $product['s' . $product->pivot->size]) {
						$old_cart->products()->attach($product->id, [
							'size'         => $product->pivot->size,
							'color'        => $product->pivot->color,
							'quantity'     => $product->pivot->quantity,
							'actual_price' => $product->offer_price ?: $product->price,
							'summary'      => ($product->offer_price ?: $product->price) * $product->pivot->quantity,
						]);
					}
				}

				$cart->enabled = true;
				$cart->save();

				$old_cart->products()->detach();
				$old_cart->delete();
			} else {
				$old_cart->delete();
				$cart->enabled = true;
				$cart->save();
			}
		}
	}

	#
	#------------------------------------------------------------------------
	#

	#GET /order
	public function getOrderPage()
	{
		$delivery_types = DeliveryItem::get();
		$user = auth()->user();

		if (Cart::getProductsCount() <= 0) {
			return $this->getMessagePage(1);
		}

//		if (!Cart::isAllProductsAvailable()) {
//			return $this->getMessagePage(6);
//		}

		if (Cart::getProductsSummaryPrice() < config('efect.free_delivery') && !$delivery_types) {
			if (auth()->user()->hasRole(['developer', 'owner']))
				return $this->getMessagePage(2);

			return $this->getMessagePage();
		}

		$data = [
			'styles'         => config('resources.auth.styles'),
			'scripts'        => config('resources.auth.scripts'),
			'user'           => $user,
			'delivery_types' => $delivery_types,
			'cart'           => $user->activeCart,
		];

		return view('pages.orders.order-add')->with($data);
	}

	#POST /order
	public function makeOrder(Request $request)
	{
		$validator = $this->validateOrder($request->all());

		if ($validator->passes()) {
			if ($request['payment'] && in_array($request['payment'], ['PayPal'])) {
				$delivery_item = false;

				if (isset($request['delivery']) && !empty($request['delivery'])) {
					$delivery = json_decode($request['delivery']);

					if (!isset($delivery->id) || !$delivery_item = DeliveryItem::find($delivery->id)) {
						return [
							'status'  => false,
							'message' => (auth()->user() && auth()->user()->hasRole(['developer']))
								? $this->getJsonMessage(trans('errors-messages.delivery_method_does_not_exist'), 'warning')
								: $this->getJsonMessage(),
						];
					}
				}

				$order = [
					'user_id'        => auth()->user()->id,
					'order_number'   => $this->makeOrderNumber(),
					'name'           => $request['name'],
					'surname'        => $request['surname'],
					'email'          => $request['email'],
					'phone'          => $request['phone'],
					'country'        => $request['country'],
					'locality'       => $request['locality'],
					'postal_code'    => $request['postal_code'],
					'street'         => $request['street'],
					'house_number'   => $request['house_number'],
					'delivery_type'  => $delivery_item ? $delivery_item->name : 'Za darmo',
					'delivery_price' => $delivery_item ? $delivery_item->price : 0,
					'payment_type'   => $request['payment'],
				];

				session()->put('order', $order);

				return [
					'status'   => true,
					'redirect' => route('order-accept'),
				];
			}

			return [
				'status'  => false,
				'message' => (auth()->user() && auth()->user()->hasRole(['developer']))
					? $this->getJsonMessage(trans('errors-messages.payment_method_does_not_exist'), 'warning')
					: $this->getJsonMessage(),
			];
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}

	#GET /order/accept/{uid}
	public function getOrderAcceptPage()
	{
		if (session()->has('order') && $user = auth()->user()) {
			$data = [
				'styles' => config('resources.auth.styles'),
				'order'  => session('order'),
				'cart'   => $user->activeCart,
			];

			return view('pages.report')->with($data);
		}

		return $this->getMessagePage();
	}

	#POST /order/accept
	public function makePayment()
	{
		if ($this->saveOrder()) {
			$order = Order::where('order_number', '=', session('order_in_process'))->first();

			if ($order->payment_type === 'PayPal') {
				return redirect()->route('make-order-paypal');
			}

			return $this->getMessagePage();
		} //TODO error: order not saved

		return $this->getMessagePage();
	}

	#GET /order/payment/cancel/{uid}
	public function cancelPayment(Request $request, $uid)
	{
		$order = Order::where('order_number', '=', $uid)->first();
		$order->order_status = 'canceled';
		$order->payment_status = 'canceled';
		$order->save();

		$this->restoreCart($order->cart_id);

		return redirect()->route('profile-basket');
	}

	#
	#------------------------------------------------------------------------
	#

	#GET /order/paypal
	public function paymentPayPalExpress(Request $request)
	{
		if (session()->has('order_in_process') && $order = Order::where('order_number', '=', session('order_in_process'))->first()) {
			$gateway = Omnipay::create('PayPal_Express');
			$gateway->setUsername('sanek.solodovnikov.94-facilitator_api1.gmail.com');
			$gateway->setPassword('65J9SF8SVX2QYJDV');
			$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31A8X8liG7tjU97y2UpyEUu6x-HSKJ');
			$gateway->setCurrency('PLN');
			$gateway->setTestMode(true);

			if ($request['token']) {
				$response = $gateway->completePurchase([
					'token'  => $request['token'],
					'amount' => $order->cart->products_summary_price,
				])->send();
			} else {
				$response = $gateway->purchase([
					'amount'      => $order->cart->products_summary_price,
					'returnUrl'   => route('make-order-paypal'),
					'cancelUrl'   => route('order-cancel-payment', ['uid' => $order->order_number]),
					'currency'    => 'PLN',
					'description' => 'Zamówienie na efect-moda.com',
				])->send();
			}

			if ($response->isSuccessful()) {
				$order->order_status = 'realization';
				$order->payment_status = $response->getData()['PAYMENTINFO_0_PAYMENTSTATUS'] ?: 'none';

				$order->save();

				session()->forget('order_in_process');
				$this->changeProductsCount($order);
				$this->sendSuccessEmail($order);

				return $this->getMessagePage(3, null, 'dziękujemy, ' . auth()->user()->name . '!');
			} elseif ($response->isRedirect()) {
				$response->redirect();
			} else {
				Log::info('PayPal payment error: ' . $response->getMessage());
				$message = $response->getMessage();

				if (auth()->user()->hasRole(['developer']))
					return $this->getMessagePage(null, null, null, $message);

				return $this->getMessagePage(null, null, 'Problem z płatnością');
			}
		}

		return $this->getMessagePage();
	}
}
