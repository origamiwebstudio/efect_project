<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\SliderImage;

class HomeController extends Controller
{
	#GET '/' or '/home'
	public function showHomePage()
	{
		$products = null;
		$tmp = Product::where('visible', true)->where('novelty', true)->limit(6)->get();
		if (count($tmp) > 0) $products['new'] = $tmp;

		$tmp = Product::where('visible', true)->orderBy('sales', 'DESC')->limit(6)->get();
		if (count($tmp) > 0) $products['bestseller'] = $tmp;

		$tmp = Product::where('visible', true)->where('offer_value', '>', 0)->inRandomOrder()->limit(6)->get();
		if (count($tmp) > 0) $products['sale'] = $tmp;

		$data = [
			'styles'        => config('resources.home.styles'),
			'scripts'       => config('resources.home.scripts'),
			'slider_images' => SliderImage::get(),
			'products'      => $products,
		];

		return view('pages.home')->with($data);
	}
}
