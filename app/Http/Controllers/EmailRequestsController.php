<?php

namespace App\Http\Controllers;

use App\Traits\GeneratesMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class EmailRequestsController extends Controller
{
	use GeneratesMessages;

	private function sendContactEmail($request)
	{
		Mail::send('emails.contacts-email', ['request' => $request], function ($message) {
			$message->from('contact@efect-moda.com', 'efect-moda.com');
			$message->to(config('app.owner_email'))
				->subject('Kontakt na efect-moda.com');
		});
	}


	public function sendContactForm(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name'                 => 'required|max:255',
			'email'                => 'required|email|max:255',
			'phone'                => 'max:32',
			'message'              => 'required|string|between:10,1000',
			'g-recaptcha-response' => 'required|captcha',
		]);

		if ($validator->passes()) {
			$this->sendContactEmail($request->all());

			return [
				'status'  => true,
				'message' => $this->getJsonMessage('Twój e-mail został pomyślnie wysłany', 'success'),
			];
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}
}
