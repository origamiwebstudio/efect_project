<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InformationController extends Controller
{
	public function getPolicyPage()
	{
		$data = [
			'styles' => config('resources.otherpages.styles'),
		];

		return view('pages.information.privacy_policy')->with($data);
	}

	public function getRegimenPage()
	{
		$data = [
			'styles' => config('resources.otherpages.styles'),
		];

		return view('pages.information.privacy_policy')->with($data);
	}

	public function getDeliveryAndPaymentPage()
	{
		$data = [
			'styles' => config('resources.otherpages.styles'),
		];

		return view('pages.information.privacy_policy')->with($data);
	}

	public function getComplaintsAndReturnsPage()
	{
		$data = [
			'styles' => config('resources.otherpages.styles'),
		];

		return view('pages.information.returns')->with($data);
	}
}
