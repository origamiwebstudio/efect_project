<?php

namespace App\Http\Controllers;

use App\Traits\GeneratesMessages;

class MessagesController extends Controller
{
	use GeneratesMessages;
}
