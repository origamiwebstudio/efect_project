<?php

namespace App\Http\Controllers;

use App\Product;
use App\Traits\GeneratesMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FavoritesController extends Controller
{
	use GeneratesMessages;

	public function __construct()
	{
		$this->middleware('auth');
	}

	#GET /favorites
	public function getFavoritesPage()
	{
		$data = [
			'styles'    => config('resources.profile.styles'),
			'scripts'   => config('resources.profile.scripts'),
			'favorites' => auth()->user()->favorites,
		];

		return view('pages.favorites')->with($data);
	}

	#POST /favorites
	public function getJsonFavorites()
	{
		$favorites = auth()->user()->favorites;

		if ($favorites && count($favorites) > 0) {
			$data = [];

			foreach (auth()->user()->favorites as $favorite) {
				$data [] = [
					'id'          => $favorite->id,
					'uid'         => $favorite->uid,
					'name'        => $favorite->name,
					'price'       => $favorite->price,
					'offer_price' => $favorite->offer_price,

				];
			}

			return $data;
		}

		return null;
	}

	#POST /favorites/add
	public function addProduct(Request $request)
	{
		$product_id = $request['product_id'];

		if ($product = Product::find($product_id)) {
			if (!auth()->user()->favorites->contains('id', $product_id)) {
				auth()->user()->favorites()->attach($product);
				$product->favorite = $product->favorite + 1;
				$product->save();

				return [
					'status'  => true,
					'message' => $this->getJsonMessage(trans('errors-messages.added_to_favorites'), 'success'),
				];
			}
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_added_to_favorites'), 'warning'),
		];
	}

	#POST /favorites/remove
	public function removeProduct(Request $request)
	{
		$product_id = $request['product_id'];

		if ($product = Product::find($product_id)) {
			if (auth()->user()->favorites->contains('id', $product_id)) {
				auth()->user()->favorites()->detach($product);
				$product->favorite = $product->favorite > 0 ? $product->favorite - 1 : 0;
				$product->save();

				return [
					'status'  => true,
					'message' => $this->getJsonMessage(trans('errors-messages.removed_from_favorites')),
				];
			}
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_removed_from_favorites'), 'warning'),
		];
	}
}
