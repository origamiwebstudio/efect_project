<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
	#GET /products/{filter?}
	public function showProductsPage($filter = null)
	{
		$products = Product::query();
		$categories = ProductCategory::get();

		if ($filter) {
			$available_filters = $categories->pluck('name')->toArray();
			array_push($available_filters, 'new', 'sale');

			if (in_array(strtolower($filter), array_map("strtolower", $available_filters))) {
				if ($filter == 'new') {
					$products = $products->where('novelty', true);
					$title = trans('static.newest_collection');
				} elseif ($filter == 'sale') {
					$products = $products->where('offer_value', '>', 0);
					$title = trans('static.special_offers');
				} else {
					$category = ProductCategory::where('name', '=', $filter)->first();
					$products = $products->where('category_id', '=', $category->id);
					$current_category_id = $category->id;
					$title = trans('static.fashionable_and_cheap', ['category' => strtolower($category->name)]);
				}
			} else {
				abort(404);
			}
		}

		$products = $products->where('visible', true);
		$products = $products->paginate(6);

		$data = [
			'styles'     => config('resources.products.all.styles'),
			'scripts'    => config('resources.products.all.scripts'),
			'title'      => isset($title) ? $title : trans('static.products_collection'),
			'categories' => $categories,
			'products'   => $products,
		];

		if (isset($current_category_id))
			$data['current_category_id'] = $current_category_id;

		return view('pages.products.products-all')->with($data);
	}

	#GET /products/{id}
	public function showSingleProductPage($id)
	{
		$product = Product::find($id);

		if (!$product) abort(404);

		$product->saveViewedProducts();
		$viewed_products = $product->getViewedProducts();

		$data = [
			'styles'          => config('resources.products.single.styles'),
			'scripts'         => config('resources.products.single.scripts'),
			'title'           => $product->name . ' - ' . trans('static.internet_shop'),
			'product'         => $product,
			'viewed_products' => (count(isset($viewed_products) ? $viewed_products : []) == 1 && $viewed_products[0]->id == $product->id) ? null : $viewed_products,
		];

		return view('pages.products.product-single')->with($data);
	}

	#POST /products
	public function getJsonProducts(Request $request)
	{
		$special = $request['spec_filter'] ? $request['spec_filter'] : null;
		$categories = $request['categories'] ? $request['categories'] : null;
		$sizes = $request['sizes'] ? $request['sizes'] : null;
		if (is_array($sizes)) sort($sizes);
		$prices = $request['prices'] ? $request['prices'] : null;
		$order = in_array($request['order'], ['newest', 'oldest', 'price_up', 'price_down']) ? $request['order'] : null;

		$products = Product::query();

		if (!is_null($special)) {
			switch ($special) {
				case 'new':
					$products = $products->where('novelty', true);
					break;
				case 'sale':
					$products = $products->where('offer_value', '>', 0);
					break;
			}
		}

		if (!is_null($categories)) {
			if (is_array($categories))
				$products = $products->whereIn('category_id', $categories);
			else
				$products = $products->where('category_id', '=', $categories);
		}

		if (!is_null($sizes)) {
			if (is_array($sizes)) {

				$products = $products->where(function ($query) use ($sizes) {
					foreach ($sizes as $size) {
						$query->orWhere('s' . $size, '>', 0);
					}
				});

			} else
				$products = $products->where('s' . $sizes, '>', 0);
		}

		if (is_array($prices) && count($prices) === 2 && is_numeric($prices[0]) && $prices[0] >= 0 && is_numeric($prices[1])) {
			$products = $products->whereRaw('IF(`offer_value` > 0, IF(`offer_type` = "fix_price", ' . (1 + config('efect.surcharge.percents')) . ' * `offer_value` + ' . config('efect.surcharge.fixed') . ', IF(`offer_type` = "fix_discount", (' . (1 + config('efect.surcharge.percents')) . ' * `price` + ' . config('efect.surcharge.fixed') . ' - `offer_value`), ((1 - (`offer_value` / 100)) * (' . (1 + config('efect.surcharge.percents')) . ' * `price` + ' . config('efect.surcharge.fixed') . ')))), `price`) BETWEEN ' . $prices['0'] . ' AND ' . $prices['1']);
		}

		if (!is_null($order)) {
			switch ($order) {
				case 'newest':
					$products = $products->orderBy('created_at', 'DESC');
					break;
				case 'oldest':
					$products = $products->orderBy('created_at', 'ASC');
					break;
				case 'price_up':
					$products = $products->orderByRaw('IF(`offer_value` > 0, IF(`offer_type` = "fix_price", ' . (1 + config('efect.surcharge.percents')) . ' * `offer_value` + ' . config('efect.surcharge.fixed') . ', IF(`offer_type` = "fix_discount", (' . (1 + config('efect.surcharge.percents')) . ' * `price` + ' . config('efect.surcharge.fixed') . ' - `offer_value`), ((1 - (`offer_value` / 100)) * (' . (1 + config('efect.surcharge.percents')) . ' * `price` + ' . config('efect.surcharge.fixed') . ')))), `price`) ASC');
					break;
				case 'price_down':
					$products = $products->orderByRaw('IF(`offer_value` > 0, IF(`offer_type` = "fix_price", ' . (1 + config('efect.surcharge.percents')) . ' * `offer_value` + ' . config('efect.surcharge.fixed') . ', IF(`offer_type` = "fix_discount", (' . (1 + config('efect.surcharge.percents')) . ' * `price` + ' . config('efect.surcharge.fixed') . ' - `offer_value`), ((1 - (`offer_value` / 100)) * (' . (1 + config('efect.surcharge.percents')) . ' * `price` + ' . config('efect.surcharge.fixed') . ')))), `price`) DESC');
					break;
			}
		}

//		$sql = $products->toSql();
		$products = $products->where('visible', true);
		$products = $products->paginate(6);

		$data = [
			'pagination' => [
				'item_first'    => $products->firstItem(),
				'item_last'     => $products->lastItem(),
				'page_last'     => $products->lastPage(),
				'page_current'  => $products->currentPage(),
				'page_next_url' => $products->nextPageUrl(),
				'page_prev_url' => $products->previousPageUrl(),
				'per_page'      => $products->perPage(),
				'total'         => $products->total(),
			],
			'products'   => [],
		];

		foreach ($products as $product) {
			$data['products'] [] = [
				'id'           => $product->id,
				'uid'          => $product->uid,
				'name'         => $product->name,
				'main_image'   => $product->main_image->link,
				'price'        => $product->price,
				'offer_price'  => $product->offer_price,
				'offer_value'  => $product->offer_value,
				'offer_type'   => $product->offer_type,
				'colors'       => $product->colors->pluck('hex'),
				'sizes'        => $product->getAvailableSizes(),
				'new'          => $product->novelty,
				'availability' => $product->availability,
				'favorite'     => $product->is_favorite,
				'created_at'   => $product->created_at->format('d.m.y H:i:s'),
			];
		}

		return $data;
	}
}
