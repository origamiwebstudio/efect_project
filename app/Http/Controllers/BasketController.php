<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductColor;
use App\Traits\GeneratesMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BasketController extends Controller
{
	use GeneratesMessages;

	public function __construct()
	{
		$this->middleware('auth', ['except' => ['addProduct', 'countProducts']]);
	}

	#
	#
	#
	#--------------------------------------------------------------------

	public static function getBasket()
	{
		if ($s_basket = session('basket')) {
			$basket = collect();

			foreach ($s_basket as $key => $item) {
				$product = Product::find($item['product_id']);

				if ($product) {
					$basket->push([
						'product'          => $product,
						'basket_uid'       => $item['item_uid'],
						'size'             => $item['size'],
						'color'            => $item['color'],
						'quantity'         => $item['quantity'],
						'time_of_addition' => $item['time_of_addition'],
					]);
				}
			}

			return $basket;
		}

		return null;
	}

	#GET /basket/sidebar
	public static function getBasketShortInfo()
	{
		if ($s_basket = session('basket')) {
			$basket = [];
			$summary = 0;

			foreach ($s_basket as $item) {
				$product = Product::find($item['product_id']);

				if ($product) {
					$basket['products'] [] = [
						'id'       => $product->id,
						'name'     => $product->name,
						'quantity' => $item['quantity'],
					];

					$summary += ($product->offer_price ? $product->offer_price : $product->price) * $item['quantity'];
				}
			}

			$basket['summary'] = $summary;

			return $basket;
		}

		return null;
	}

	#POST /basket/count
	public static function countProducts()
	{
		$basket = session('basket') ? session('basket') : collect();

		return count($basket);
	}

	#
	#
	#
	#--------------------------------------------------------------------

	#POST /basket/add
	public function addProduct(Request $request)
	{
		$product = Product::find($request['product_id']);

		if ($product && ProductColor::isColor($request['color']) && in_array($request['size'], config('efect.sizes'))) {
			$basket = session('basket') ? session('basket') : collect();

			if ($product['s' . $request['size']] >= $request['quantity'] + $basket->where('product_id', '=', $request['product_id'])->where('size', '=', $request['size'])->sum('quantity')) {

				$basket->push([
					'item_uid'         => uniqid(),
					'product_id'       => $request['product_id'],
					'color'            => $request['color'],
					'size'             => $request['size'],
					'quantity'         => $request['quantity'],
					'time_of_addition' => Carbon::now()->timestamp,
				]);

				session()->put('basket', $basket);

				return [
					'status'  => true,
					'message' => $this->getJsonMessage(trans('errors-messages.added_to_basket'), 'success'),
				];
			}

			return [
				'status'  => false,
				'message' => $this->getJsonMessage(trans('errors-messages.we_do_not_have_so_many_products'), 'warning'),
			];
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_added_to_basket'), 'warning'),
		];
	}

	#POST /basket/remove
	public function removeProduct(Request $request)
	{
		$uid = $request['item_uid'] ? $request['item_uid'] : null;
		$basket = session('basket') ? session('basket') : collect();

		if (count($basket) > 0) {
			$basket = $basket->reject(function ($item) use ($uid) {
				return $item['item_uid'] == $uid;
			});

			session()->put('basket', $basket);

			return [
				'status'  => true,
				'message' => $this->getJsonMessage(trans('errors-messages.removed_from_basket'), 'success'),
			];
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_removed_from_basket'), 'warning'),
		];
	}

	#POST /basket/quantity/change
	public function changeProductQuantity(Request $request)
	{
		$uid = $request['basket_uid'] ?? null;
		$quantity = is_numeric($request['quantity']) ? round($request['quantity']) : null;
		$basket = session('basket') ? session('basket') : collect();

		if ($uid && $quantity && count($basket) > 0 && $basket->contains('item_uid', $uid)) {
			$basket_item = $basket->where('item_uid', '=', $request['basket_uid'])->first();

			if ($basket_item && $product = Product::find($basket_item['product_id'])) {
				if ($product['s' . $basket_item['size']] >= $quantity + $basket->where('product_id', '=', $basket_item['product_id'])->where('size', '=', $basket_item['size'])->sum('quantity') - $basket_item['quantity']) {
					$basket = $basket->map(function ($item) use ($uid, $quantity) {
						if ($item['item_uid'] == $uid) {
							$item['quantity'] = $quantity;
						}

						return $item;
					});

					session()->put('basket', $basket);

					return [
						'status'  => true,
						'message' => $this->getJsonMessage(trans('errors-messages.changed_basket_product_quantity'), 'success'),
					];
				}

				return [
					'status'  => false,
					'message' => $this->getJsonMessage(trans('errors-messages.we_do_not_have_so_many_products'), 'warning'),
				];
			}
		}

		return [
			'status'  => false,
			'message' => $this->getJsonMessage(trans('errors-messages.not_changed_basket_product_quantity'), 'warning'),
		];
	}
}
