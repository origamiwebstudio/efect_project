<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Arcanedev\NoCaptcha\NoCaptcha;

class ContactsController extends Controller
{
	public function getContactsPage()
	{
		$data = [
			'styles'  => config('resources.otherpages.styles'),
			'scripts' => config('resources.otherpages.scripts'),
			'captcha' => new NoCaptcha(env('NOCAPTCHA_SECRET'), env('NOCAPTCHA_SITEKEY'), app()->getLocale())
		];

		return view('pages.contacts')->with($data);
	}
}
