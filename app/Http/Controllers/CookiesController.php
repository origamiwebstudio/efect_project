<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CookiesController extends Controller
{
	public function acceptCookies(Request $request)
	{
		session()->put('isAcceptedCookies', true);

		return [
			'status' => true,
		];
	}
}
