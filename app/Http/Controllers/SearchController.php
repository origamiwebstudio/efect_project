<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
	#POST /search
	public function getJsonResults(Request $request)
	{
		if ($search = $request['search'] ? $request['search'] : 'tes')
			$products = Product::where('name', 'like', '%' . $search . '%')->get();

		if (isset($products) && count($products) > 0) {
			$results = [];

			foreach ($products as $product) {
				$results [] = [
					'id'          => $product->id,
					'photo'       => $product->main_image->link,
					'name'        => $product->name,
					'price'       => $product->price,
					'offer_price' => $product->offer_price,
				];
			}

			return $results;
		}

		return null;
	}
}
