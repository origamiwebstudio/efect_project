<?php

namespace App\Http\Controllers;

use App\Product;
use App\Traits\GeneratesMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
	use GeneratesMessages;

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function showProfilePage()
	{
		$data = [
			'title' => trans('static.my_profile') . ' - ' . trans('static.internet_shop'),
			'user'  => auth()->user(),
		];

		return view('pages.profile.profile')->with($data);
	}

	public function showFavoritesPage()
	{
		$data = [
			'title'     => trans('static.favorites') . ' - ' . trans('static.internet_shop'),
			'favorites' => auth()->user()->favorites,
		];

		return view('pages.profile.profile-favorites')->with($data);
	}

	public function showOrdersPage()
	{
		$data = [
			'title'  => trans('static.my_orders') . ' - ' . trans('static.internet_shop'),
			'orders' => auth()->user()->orders,
		];

		return view('pages.profile.profile-orders')->with($data);
	}

	public function showLastViewedPage()
	{
		$viewed_products = new Product();

		$data = [
			'title'           => trans('static.last_viewed_products') . ' - ' . trans('static.internet_shop'),
			'viewed_products' => $viewed_products->getViewedProducts(9),
		];

		return view('pages.profile.profile-viewed')->with($data);
	}

	#POST /profile/edit
	public function changeProfileData(Request $request)
	{
		if (!$user = auth()->user()) {
			return [
				'status'   => false,
				'message'  => $this->getJsonMessage(trans('errors-messages.not_changed_profile_data'), 'warning'),
				'redirect' => route('login'),
			];
		}

		$validator = Validator::make($request->all(), [
			'name'         => 'required|max:255',
			'surname'      => 'required|max:255',
			'phone'        => 'max:32|unique:users,phone,' . $user->id,
			'country'      => 'max:255',
			'locality'     => 'max:255',
			'postal_code'  => 'max:50',
			'street'       => 'max:255',
			'house_number' => 'max:50',
		]);

		if ($validator->passes()) {
			$user->name = $request['name'];
			$user->surname = $request['surname'];
			$user->phone = $request['phone'] ?: null;
			$user->country = $request['country'];
			$user->locality = $request['locality'];
			$user->postal_code = $request['postal_code'];
			$user->street = $request['street'];
			$user->house_number = $request['house_number'];

			$user->save();

			return [
				'status'  => true,
				'message' => $this->getJsonMessage(trans('errors-messages.changed_profile_data'), 'success'),
			];
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}
}
