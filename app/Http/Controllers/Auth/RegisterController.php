<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Arcanedev\NoCaptcha\NoCaptcha;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name'                 => 'required|max:255',
			'surname'              => 'required|max:255',
			'email'                => 'required|email|max:255|unique:users',
			'phone'                => 'max:32|unique:users',
			'password'             => 'required|min:6|confirmed',
			'country'              => 'required_with:add_address|max:255',
			'locality'             => 'required_with:add_address|max:255',
			'postal_code'          => 'required_with:add_address|max:50',
			'street'               => 'required_with:add_address|max:255',
			'house_number'         => 'required_with:add_address|max:50',
			'privacy_policy'       => 'required',
			'regimen'              => 'required',
			'g-recaptcha-response' => 'required|captcha',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return User
	 */
	protected function create(array $data)
	{
		$user = new User();
		$user->name = $data['name'];
		$user->surname = $data['surname'];
		$user->email = $data['email'];
		$user->phone = $data['phone'] ?: null;
		$user->password = bcrypt($data['password']);

		$user->country = $data['country'] ?? $data['country'];
		$user->locality = $data['locality'] ?? $data['locality'];
		$user->postal_code = $data['postal_code'] ?? $data['postal_code'];
		$user->street = $data['street'] ?? $data['street'];
		$user->house_number = $data['house_number'] ?? $data['house_number'];

		$user->save();

		return $user;
	}

	public function showRegistrationForm()
	{
		$data = [
			'title'   => trans('static.register') . ' - ' . trans('static.internet_shop'),
			'styles'  => config('resources.auth.styles'),
			'scripts' => config('resources.auth.scripts'),
			'captcha' => new NoCaptcha(env('NOCAPTCHA_SECRET'), env('NOCAPTCHA_SITEKEY'), app()->getLocale()),
		];

		return view('auth.register')->with($data);
	}

	public function register(Request $request)
	{
		$validator = $this->validator($request->all());

		if ($validator->passes()) {

			event(new Registered($user = $this->create($request->all())));

			$this->guard()->login($user);

			return [
				'status'   => true,
				'redirect' => route('home'),
			];

//			return $this->registered($request, $user)
//				?: redirect($this->redirectPath());
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}
}
