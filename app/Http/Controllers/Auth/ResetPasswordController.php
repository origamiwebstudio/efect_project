<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\GeneratesMessages;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ResetPasswordController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords, GeneratesMessages;

	/**
	 * Where to redirect users after resetting their password.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('guest');
	}

	public function showResetForm(Request $request, $token = null)
	{
		$data = [
			'title'   => trans('static.reset_password') . ' - ' . trans('static.internet_shop'),
			'token'   => $token,
			'email'   => $request->email,
			'styles'  => config('resources.auth.styles'),
			'scripts' => config('resources.auth.scripts'),
		];

		return view('auth.passwords.reset')->with($data);
	}

	public function reset(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'token'    => 'required',
			'email'    => 'required|email',
			'password' => 'required|confirmed|min:6',
		]);

		if ($validator->passes()) {
			$response = $this->broker()->reset(
				$this->credentials($request), function ($user, $password) {
				$this->resetPassword($user, $password);
			}
			);

			if ($response == Password::PASSWORD_RESET) {
				return [
					'status'   => true,
					'redirect' => route('message', ['code' => 5]),
				];
			} else {
				return [
					'status' => false,
					'errors' => ['email' => [trans($response)]],
				];
			}
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}
}
