<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'logout']);
	}

	public function showLoginForm()
	{
		$data = [
			'title'   => trans('static.login') . ' - ' . trans('static.internet_shop'),
			'styles'  => config('resources.auth.styles'),
			'scripts' => config('resources.auth.scripts'),
		];

		return view('auth.login')->with($data);
	}

	public function login(Request $request)
	{
		$validator = $this->validateLogin($request);

		if ($validator->passes()) {
			if ($this->hasTooManyLoginAttempts($request)) {
				$this->fireLockoutEvent($request);

				return $this->sendLockoutResponse($request);
			}

			if ($this->attemptLogin($request)) {
				return $this->sendLoginResponse($request);
			}

			$this->incrementLoginAttempts($request);

			return [
				'status' => false,
				'errors' => [$this->username() => [Lang::get('auth.failed')]],
			];
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}

	protected function validateLogin(Request $request)
	{
		return Validator::make($request->all(), [
			$this->username() => 'required|string|between:2,255',
			'password'        => 'required|min:6',
		]);
	}

	protected function authenticated(Request $request, $user)
	{
		return session()->pull('url.intended', route('home'));
	}

	protected function sendLoginResponse(Request $request)
	{
		$request->session()->regenerate();

		$this->clearLoginAttempts($request);

		return $this->authenticated($request, $this->guard()->user())
			?: $this->redirectPath();
	}
}
