<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset emails and
	| includes a trait which assists in sending these notifications from
	| your application to your users. Feel free to explore this trait.
	|
	*/

	use SendsPasswordResetEmails;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('guest');
	}

	public function showLinkRequestForm()
	{
		$data = [
			'title'   => trans('static.reset_password') . ' - ' . trans('static.internet_shop'),
			'styles'  => config('resources.auth.styles'),
			'scripts' => config('resources.auth.scripts'),
		];

		return view('auth.passwords.email')->with($data);
	}

	public function sendResetLinkEmail(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
		]);

		if ($validator->passes()) {
			$response = $this->broker()->sendResetLink(
				$request->only('email')
			);

			if ($response == Password::RESET_LINK_SENT) {
				return [
					'status'   => true,
					'redirect' => route('message', ['code' => 4]),
				];
			} else {
				return [
					'status' => false,
					'errors' => ['email' => [trans($response)]],
				];
			}
		}

		return [
			'status' => false,
			'errors' => $validator->errors(),
		];
	}
}
