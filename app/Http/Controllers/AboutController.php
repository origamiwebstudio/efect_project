<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
	public function getAboutPage()
	{
		$data = [
			'styles'  => config('resources.otherpages.styles'),
			'scripts' => config('resources.otherpages.scripts'),
		];

		return view('pages.about')->with($data);
	}
}
