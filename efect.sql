-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 10 2018 г., 20:39
-- Версия сервера: 5.7.19
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `efect`
--

-- --------------------------------------------------------

--
-- Структура таблицы `advantages`
--

CREATE TABLE `advantages` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `advantages`
--

INSERT INTO `advantages` (`id`, `order_`, `name`, `text`) VALUES
(1, 4, 'darmowa dostawa', 'Wyślemy Twoje zamówienie w ciągu 2-4 dni roboczych. Darmowa wysyłka od 200zł'),
(2, 2, 'ololo', 'Rozmiar nie ten? Kolor nie ten? Masz możliwość zwrotu w ciągu 14'),
(3, 3, 'przewaga 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, s'),
(4, 1, '14 dni na zwrot', 'Rozmiar nie ten? Kolor nie ten? Masz możliwość zwrotu w ciągu 14');

-- --------------------------------------------------------

--
-- Структура таблицы `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '2018-01-10 16:57:44', '2018-01-10 17:33:45'),
(2, 1, 1, '2018-01-10 17:34:28', '2018-01-10 17:36:26');

-- --------------------------------------------------------

--
-- Структура таблицы `cart_product`
--

CREATE TABLE `cart_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `actual_price` decimal(6,2) NOT NULL,
  `summary` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `cart_product`
--

INSERT INTO `cart_product` (`id`, `cart_id`, `product_id`, `size`, `color`, `quantity`, `actual_price`, `summary`) VALUES
(1, 1, 4, '48', '#203769', 1, '36.55', '36.55'),
(2, 2, 4, '48', '#203769', 1, '36.55', '36.55'),
(3, 2, 3, '34', '#BB488D', 5, '52.50', '262.50');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_types`
--

CREATE TABLE `delivery_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `delivery_types`
--

INSERT INTO `delivery_types` (`id`, `order_`, `name`, `price`) VALUES
(1, 0, 'kurier dpd - przedplata', '13.00'),
(2, 0, 'kurier dpd - pobranie', '14.50'),
(3, 0, 'poczta polska - przedplata', '14.00'),
(4, 0, 'poczta polska - pobranie', '15.00');

-- --------------------------------------------------------

--
-- Структура таблицы `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `favorites`
--

INSERT INTO `favorites` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 2, 1, '2018-01-10 17:31:23', '2018-01-10 17:31:23'),
(3, 1, 1, '2018-01-10 17:31:25', '2018-01-10 17:31:25');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_22_124150_entrust_setup_tables', 2),
(4, '2017_11_23_153019_add_product_categories_table', 3),
(5, '2017_11_23_153410_add_products_table', 4),
(6, '2017_11_23_163130_add_product_images_table', 5),
(7, '2017_11_23_163600_add_product_colors_table', 6),
(8, '2017_11_23_164105_add_product_color_pivot_table', 7),
(9, '2017_11_23_170418_add_category_id_field_to_products_table', 8),
(11, '2017_11_26_154721_add_slider_images_table', 9),
(12, '2017_12_02_180353_create_favorites_table', 10),
(15, '2017_12_04_190535_create_advantages_table', 11),
(16, '2017_12_07_201222_add_address_fields_to_users_table', 12),
(17, '2017_12_11_114324_create_orders_table', 13),
(19, '2017_12_11_193518_add_summary_field_to_order_product_table', 15),
(20, '2017_12_15_132244_create_delivery_types_table', 16),
(21, '2018_01_07_163910_create_carts_table', 17),
(22, '2018_01_07_164221_create_cart_product_pivot_table', 18),
(23, '2018_01_09_182442_add_cart_id_field_to_orders_table', 19),
(24, '2018_01_10_172458_add_visible_field_to_products_table', 20);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `order_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `order_status` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `house_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_price` decimal(6,2) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `cart_id`, `order_number`, `order_status`, `name`, `surname`, `email`, `phone`, `country`, `locality`, `postal_code`, `street`, `house_number`, `delivery_type`, `delivery_price`, `payment_type`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1c3gi1plg', 'done', 'Sanek', 'Solodovnikov', 'sanek.solodovnikov.94@gmail.com', '+48535764974', 'Polska', 'Lodz', '93-008', 'Rzgowska', '17A', 'poczta polska - pobranie', '15.00', 'PayPal', 'Completed', '2018-01-10 17:33:45', '2018-01-10 17:35:08'),
(2, 1, 2, '1c3gi9hn1', 'canceled', 'Sanek', 'Solodovnikov', 'sanek.solodovnikov.94@gmail.com', '+48535764974', 'Polska', 'Lodz', '93-008', 'Rzgowska', '17A', 'poczta polska - pobranie', '15.00', 'PayPal', 'canceled', '2018-01-10 17:36:14', '2018-01-10 17:36:26');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('email@email.com', '298ce3de3842b57dbbac8168f0865c6c2eb491bf95b709c8fcc149e7bf30fde2', '2017-11-30 16:47:38'),
('sanek.solodovnikov.94@gmail.com', '894c9acc305882737b35b82372a82725171f136fdd32fe04d8e2e1e8955fa44c', '2018-01-04 18:29:12');

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user-display', 'Display users', NULL, NULL, NULL),
(3, 'user-edit', 'Edit users', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `offer_type` varchar(48) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_value` decimal(6,2) DEFAULT NULL,
  `novelty` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `sales` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `favorite` int(11) NOT NULL DEFAULT '0',
  `s34` int(11) NOT NULL DEFAULT '0',
  `s36` int(11) NOT NULL DEFAULT '0',
  `s38` int(11) NOT NULL DEFAULT '0',
  `s40` int(11) NOT NULL DEFAULT '0',
  `s42` int(11) NOT NULL DEFAULT '0',
  `s44` int(11) NOT NULL DEFAULT '0',
  `s46` int(11) NOT NULL DEFAULT '0',
  `s48` int(11) NOT NULL DEFAULT '0',
  `s50` int(11) NOT NULL DEFAULT '0',
  `s52` int(11) NOT NULL DEFAULT '0',
  `s54` int(11) NOT NULL DEFAULT '0',
  `s56` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `price`, `offer_type`, `offer_value`, `novelty`, `visible`, `sales`, `views`, `favorite`, `s34`, `s36`, `s38`, `s40`, `s42`, `s44`, `s46`, `s48`, `s50`, `s52`, `s54`, `s56`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sweter o prostym kroju', '60.00', 'fix_price', '50.00', 1, 1, 0, 1, 1, 0, 20, 50, 50, 50, 20, 20, 10, 5, 0, 0, 0, '2018-01-10 17:04:07', '2018-01-10 17:31:25'),
(2, 1, 'Sweter z metalową aplikacją', '90.00', 'fix_price', '50.00', 0, 1, 0, 1, 1, 0, 15, 10, 5, 5, 0, 0, 0, 0, 0, 0, 0, '2018-01-10 17:06:06', '2018-01-10 17:31:23'),
(3, 1, 'Sweter z golfem', '80.00', 'fix_price', '50.00', 1, 1, 0, 0, 0, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, '2018-01-10 17:08:06', '2018-01-10 17:08:06'),
(4, 2, 'Dwuwarstwowa bluzka', '70.00', 'percent', '50.00', 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 29, 30, 30, 30, 0, '2018-01-10 17:10:35', '2018-01-10 17:34:38'),
(5, 2, 'Bluzka typu hiszpanka podkreślająca talię', '60.00', 'fix_discount', '20.00', 1, 1, 0, 1, 0, 0, 0, 0, 50, 50, 50, 0, 0, 0, 0, 0, 0, '2018-01-10 17:12:01', '2018-01-10 17:12:29'),
(6, 2, 'Lekka bluzka z koronkową wstawką', '40.00', '', '0.00', 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2018-01-10 17:14:01', '2018-01-10 17:19:12'),
(7, 3, 'Koszula w kratę', '50.00', '', '0.00', 1, 1, 0, 0, 0, 0, 0, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, '2018-01-10 17:20:38', '2018-01-10 17:20:38'),
(8, 3, 'Denimowa koszula z efektem sprania', '80.00', 'percent', '50.00', 1, 1, 0, 0, 0, 0, 0, 0, 25, 5, 3, 0, 0, 0, 0, 0, 0, '2018-01-10 17:23:10', '2018-01-10 17:23:10'),
(9, 3, 'Koszula w kratę', '60.00', 'percent', '50.00', 1, 1, 0, 0, 0, 3, 3, 3, 3, 45, 3, 3, 3, 3, 3, 3, 3, '2018-01-10 17:25:21', '2018-01-10 17:25:21');

-- --------------------------------------------------------

--
-- Структура таблицы `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`) VALUES
(1, 'Swetry'),
(2, 'Bluzki'),
(3, 'Koszule');

-- --------------------------------------------------------

--
-- Структура таблицы `product_color`
--

CREATE TABLE `product_color` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_color`
--

INSERT INTO `product_color` (`id`, `product_id`, `color_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 1),
(4, 3, 4),
(5, 4, 3),
(6, 5, 4),
(7, 6, 1),
(8, 7, 1),
(9, 7, 2),
(10, 7, 3),
(11, 8, 2),
(12, 8, 3),
(13, 9, 2),
(14, 9, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `product_colors`
--

CREATE TABLE `product_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hex` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_colors`
--

INSERT INTO `product_colors` (`id`, `name`, `hex`) VALUES
(1, 'Czarny', '#1E1B16'),
(2, 'Kasztanowy', '#C8514B'),
(3, 'Niebieski', '#203769'),
(4, 'Różowy', '#BB488D');

-- --------------------------------------------------------

--
-- Структура таблицы `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `order_`, `link`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'images/uploads/41228c11a4ccca6a19c3e3b700b0aeba.jpg', '2018-01-10 17:04:07', '2018-01-10 17:04:07'),
(2, 1, 0, 'images/uploads/026c55471ec35a5914e1374ed2101cd5.jpg', '2018-01-10 17:04:07', '2018-01-10 17:04:07'),
(3, 1, 0, 'images/uploads/a60d35614b0d61ed32fbb25255f8a90c.jpg', '2018-01-10 17:04:07', '2018-01-10 17:04:07'),
(4, 1, 0, 'images/uploads/491f31192a5613f3f4b44eb056ee7cef.jpg', '2018-01-10 17:04:07', '2018-01-10 17:04:07'),
(5, 2, 0, 'images/uploads/1185a1ed243e96f02d2acb8f354d249d.jpg', '2018-01-10 17:06:06', '2018-01-10 17:06:06'),
(6, 2, 0, 'images/uploads/055398a6800856fda13faf96e6c1f52c.jpg', '2018-01-10 17:06:06', '2018-01-10 17:06:06'),
(7, 2, 0, 'images/uploads/58e7412377947737fa468de7dfaa5660.jpg', '2018-01-10 17:06:06', '2018-01-10 17:06:06'),
(8, 2, 0, 'images/uploads/8c538b1061c2ab8e7b6ae6a1f626f39b.jpg', '2018-01-10 17:06:06', '2018-01-10 17:06:06'),
(9, 3, 0, 'images/uploads/495c810d2e556c8a7a86512a94d17410.jpg', '2018-01-10 17:08:06', '2018-01-10 17:08:06'),
(10, 3, 0, 'images/uploads/38f3d9f9c42772d0620dc703b999f011.jpg', '2018-01-10 17:08:06', '2018-01-10 17:08:06'),
(11, 3, 0, 'images/uploads/11a4ae585cc2209517c8a9b4d01fe15a.jpg', '2018-01-10 17:08:06', '2018-01-10 17:08:06'),
(12, 4, 0, 'images/uploads/7ed4f6abe416f29e07b660940b71a5cd.jpg', '2018-01-10 17:10:35', '2018-01-10 17:10:35'),
(13, 4, 0, 'images/uploads/b5e2b25eeee24e2f6949134c9cc4ba69.jpg', '2018-01-10 17:10:35', '2018-01-10 17:10:35'),
(14, 4, 0, 'images/uploads/e983a2ba391658ee0b848e920ef9c68a.jpg', '2018-01-10 17:10:35', '2018-01-10 17:10:35'),
(15, 5, 0, 'images/uploads/cbdd3c5dff9a6a04828b5d2f95ba9c36.jpg', '2018-01-10 17:12:02', '2018-01-10 17:12:02'),
(16, 5, 0, 'images/uploads/fa58590acb2109c754a10a3b5f167549.jpg', '2018-01-10 17:12:02', '2018-01-10 17:12:02'),
(17, 5, 0, 'images/uploads/a1581b0f8ea1c022dc18d6ad73123c4b.jpg', '2018-01-10 17:12:02', '2018-01-10 17:12:02'),
(18, 6, 0, 'images/uploads/a07ef84a0bdde21bb6dc6ffcd09a3cf6.jpg', '2018-01-10 17:14:01', '2018-01-10 17:14:01'),
(19, 6, 0, 'images/uploads/a8103a87e667d3fbb65e8e4c998c1030.jpg', '2018-01-10 17:14:01', '2018-01-10 17:14:01'),
(20, 6, 0, 'images/uploads/141b23f98ad146461c60233eac0c48ef.jpg', '2018-01-10 17:14:01', '2018-01-10 17:14:01'),
(21, 6, 0, 'images/uploads/c826c9629f55ff99cb1110618d7e9114.jpg', '2018-01-10 17:14:01', '2018-01-10 17:14:01'),
(22, 7, 0, 'images/uploads/5cc857378fd88fc7dad3c63d0d97c6d4.jpg', '2018-01-10 17:20:38', '2018-01-10 17:20:38'),
(23, 7, 0, 'images/uploads/44dfe01b4fdbf39e48cac444f6d60482.jpg', '2018-01-10 17:20:38', '2018-01-10 17:20:38'),
(24, 7, 0, 'images/uploads/f71972b616f45f74e32423aa00e59af4.jpg', '2018-01-10 17:20:38', '2018-01-10 17:20:38'),
(25, 8, 0, 'images/uploads/4088b2a2f9362df743792831005a849e.jpg', '2018-01-10 17:23:10', '2018-01-10 17:23:10'),
(26, 8, 0, 'images/uploads/fbc7ad67a90bdd8bf50fcd376684527e.jpg', '2018-01-10 17:23:10', '2018-01-10 17:23:10'),
(27, 8, 0, 'images/uploads/bab97cc0b9cd95fab09f680e362dba0d.jpg', '2018-01-10 17:23:10', '2018-01-10 17:23:10'),
(28, 9, 0, 'images/uploads/a67999692889c7c99929d9ce604afe77.jpg', '2018-01-10 17:25:21', '2018-01-10 17:25:21'),
(29, 9, 0, 'images/uploads/19f12dd32d12268ef4181145e3fc58a2.jpg', '2018-01-10 17:25:21', '2018-01-10 17:25:21'),
(30, 9, 0, 'images/uploads/e158cc495e837b74b9def7e4413e4d4a.jpg', '2018-01-10 17:25:21', '2018-01-10 17:25:21');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'developer', 'Deweloper', 'Pełna obsługa strony', NULL, NULL),
(2, 'owner', 'Właściciel', 'Pełna obsługa strony', NULL, NULL),
(3, 'manager', 'Menerżer', 'Podstawowa obsługa strony', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_images`
--

CREATE TABLE `slider_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `slider_images`
--

INSERT INTO `slider_images` (`id`, `order_`, `link`, `name`) VALUES
(1, 0, 'images/uploads/f961f760d7015a8a6fe5117113168bde.jpg', 'Super Mega Rabat!');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `phone`, `house_number`, `street`, `postal_code`, `locality`, `country`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sanek', 'Solodovnikov', 'sanek.solodovnikov.94@gmail.com', '+48535764974', '17A', 'Rzgowska', '93-008', 'Lodz', 'Polska', '$2y$10$gE3yzoL9ICjNZ18EIJstfe4nlPFVLJbsvulS15NdVldNtN9S3H47m', 'JshLHh5ySfEWJ9NMa1pTWBVBT2HBRNsiEv1SghRlzlSWy6uv4mGdZhvC2vsT', '2017-11-21 14:00:06', '2018-01-10 16:02:04'),
(2, 'Roman', 'Velhas', 'email@email.com', '+48999666888', NULL, NULL, NULL, NULL, NULL, '$2y$10$EOsnHC/oJdBVsEKBOLVKbO9pJmKrBj.BrTycTNkjiK4q/3eJ/hI8W', 'TGEHnJCCofCWopvTRKBALvWITlX9bcIaz54rj0RoMlP5hXFfOZyAuH0o4lq4', '2017-11-23 07:43:55', '2017-11-23 12:34:00'),
(3, 'Hlib', 'Liapota', 'age007nt@gmail.com', '+48555555999', NULL, NULL, NULL, NULL, NULL, '$2y$10$QR62fhLV0tF0zyubXcMBNuRCbRHFUsxenesAPxwMuRxFz3HC7g2ZO', 'fyLB81XCWSzj0kvHADckK1s7WKCOcGIe5ZUyWToZYtP3D8CyVnoXf2yuPzCW', '2017-11-23 12:10:39', '2017-11-23 12:36:41'),
(4, 'test', 'testowych', 'testowych@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Sg2OGtM07YfGahT/bYu4fuHcdM8tDFmgBV9gIMALB4AiztcvQToqS', 'ZODKoaHXZ8JkQw5xsGAI5BiFCeJ93SJXMZVc4hXtQTOVlEP8ALgCBBuWofFE', '2017-12-07 19:02:28', '2017-12-07 19:18:37'),
(5, 'Grigoriy', 'Leps', 'leps@mail.com', '+48555555998', '17A', 'Rzgowska', '93-008', 'Lodz', 'Polska', '$2y$10$nsbzza4/gOHXyUTbnZ9Hx.Zk.OJDtw6JdBggk.abY.8.PUgYRPi4G', 'zvbk5u8pKuhpoZ7lhGXXWOlXiR7zR5r83A2nSjcarHUFUDE7Wjy4nJy7lLxr', '2017-12-10 18:26:55', '2017-12-11 10:24:46'),
(7, 'jo', 'man', 'man@email.com', '', '', '', '', '', '', '$2y$10$5Xlm2PWWvi701LJqKtu.W.gahES.xtwoMgaeVOyxBeE6rJ9MVCI8i', 'gJyryzqbXnmAeXtLs4bN1Q4g26WeBUPMSTKUk0QAhyUJ6byGHC7qK0Qv4Thh', '2018-01-02 14:08:57', '2018-01-02 14:09:08'),
(9, 'Coco', 'Chanel', 'coco@email.cmo', NULL, '', '', '', '', '', '$2y$10$EK2P6VAT9OzzkSLqkUzcRe7Ttr1pKHtEZpDh8LDnyQ3QicntnA/IK', '7mqU7e1aMHp8if9KIFpnbU73ZVlZFgrpQdrZh1Arbudl4pQWcsClQo8Tud63', '2018-01-02 14:11:43', '2018-01-02 14:12:00'),
(10, 'Lolo', 'Chanel', 'lolo@gmail.cmo', NULL, '', '', '', '', '', '$2y$10$NTHTQio4wXp5izTcR7b7bOftjxjfVcJFyRUDcyrp93G9rJBFDb37m', '3VkLYz1XHWyD7PjjPUeQJ1HSorC2QhPWSuzwNroLQULlBgfHMFM4hxgGy8eO', '2018-01-02 14:12:34', '2018-01-02 14:12:55');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `advantages`
--
ALTER TABLE `advantages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cart_product`
--
ALTER TABLE `cart_product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `delivery_types`
--
ALTER TABLE `delivery_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `favorites_product_id_user_id_unique` (`product_id`,`user_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_colors`
--
ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `advantages`
--
ALTER TABLE `advantages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `cart_product`
--
ALTER TABLE `cart_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `delivery_types`
--
ALTER TABLE `delivery_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `product_colors`
--
ALTER TABLE `product_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
