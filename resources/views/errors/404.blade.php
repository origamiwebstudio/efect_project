@extends('layouts.main')

@section('page')

	<section class="error-page">
		<div class="error-page__content">
			<div class="error-page__text">
				<span>404</span>
				<p>{{ trans('errors-messages.404_text') }}</p>
			</div>
			<div class="error-page__buttons">
				<a href="{{ route('home') }}" class="btn">Strona główna</a>
			</div>
		</div>
	</section>

@endsection