@extends('layouts.main')

@section('page')

	<section class="error-page">
		<div class="error-page__content">
			<div class="error-page__text">
				<span>403</span>
				<p>{{ trans('errors-messages.403_text') }}</p>
			</div>
			<div class="error-page__buttons">
				<a href="{{ route('login') }}" class="btn">Logowanie</a>
				<a href="{{ route('home') }}">Strona główna</a>
			</div>
		</div>
	</section>

@endsection