@extends('layouts.main')

@section('page')

    <div class="page-header">
        <div class="container">
            <h1>edytuj zamówienie</h1>
        </div>
    </div>

    <div id="orderpage">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-push-9 col-sm-4 col-sm-push-8">
                    <aside class="order-aside order-aside_loaded">
                        <header>
                            <h3>moje zamówienie</h3>
                            <span>illość produktów: {{ $order->products->count() }}</span>
                        </header>
                        <ul>
                            @foreach($order->products as $product)
                                <li><a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }} <span>({{ $product->pivot->quantity }}
                                            )</span></a></li>
                            @Endforeach
                        </ul>
                        <footer v-cloak>
                            <dl>
                                <dt>wartość zamówienia:</dt>
                                <dd ref="orderprice"
                                    data-price="{{ $order->products_summary }}">{{ $order->products_summary }} PLN
                                </dd>
                                <br>
                                <dt>koszt przesyłki:</dt>
                                <dd>
                                    @{{ deliveryData ? deliveryData.price : '0.00' }} PLN
                                    @if($order->delivery_type == 0)
                                        <div class="hint tooltip">
                                            <span class="hint__questioned-mark">?</span>
                                            <span class="tooltip__message">Darmowa dostawa przy zakupach od {{ config('efect.free_delivery') }}
                                                zł</span>
                                        </div>
                                    @endif
                                </dd>
                                <br>
                                <dt>do zapłaty:</dt>
                                <dd class="order-aside__total">@{{ totalPrice }} PLN</dd>
                            </dl>
                        </footer>
                    </aside>
                </div>
                <div class="col-md-9 col-md-pull-3 col-sm-8 col-sm-pull-4">
                    <section class="order-form">
                        <form class="efect-form" action="{{ route('update-order') }}" method="post"
                              @submit.prevent="makeOrder('update')" :class="{'efect-form_loading': sending }">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_number" value="{{ $order->order_number }}"
                                   ref="order_number">

                            <h3>dane kontaktowe</h3>
                            <div class="order-form__input-group">
                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="name"
                                         v-model="orderData.name" id="name" placeholder="imię" required="true"
                                         oldvalue="{{ $order->name }}" :error="orderErrors.name"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="surname"
                                         v-model="orderData.surname" id="surname" placeholder="nazwisko" required="true"
                                         oldvalue="{{ $order->surname }}" :error="orderErrors.surname"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="email" name="email"
                                         v-model="orderData.email" id="email" placeholder="email" required="true"
                                         oldvalue="{{ $order->email }}" :error="orderErrors.email"
                                         readonly="true"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="phone"
                                         v-model="orderData.phone" id="phone" placeholder="telefon"
                                         oldvalue="{{ $order->phone }}" :error="orderErrors.phone"></div>
                                </div>
                            </div>

                            <h3>dane do wysyłki</h3>
                            <div class="order-form__input-group">
                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="country"
                                         v-model="orderData.country" id="country" placeholder="kraj" required="true"
                                         oldvalue="{{ $order->country }}" :error="orderErrors.country"></div>
                                </div>

                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="locality"
                                         v-model="orderData.locality" id="locality" placeholder="miejscowość"
                                         required="true"
                                         oldvalue="{{ $order->locality }}" :error="orderErrors.locality"></div>
                                </div>

                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="postal_code"
                                         v-model="orderData.postal_code" id="postal_code" placeholder="kod posztowy"
                                         required="true"
                                         oldvalue="{{ $order->postal_code }}" :error="orderErrors.postal_code"></div>
                                </div>

                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="street"
                                         v-model="orderData.street" id="street" placeholder="ulica" required="true"
                                         oldvalue="{{ $order->street }}" :error="orderErrors.street"></div>
                                </div>

                                <div class="efect-form__form-group">
                                    <div is="efect-input" class="efect-form__input" type="text" name="house_number"
                                         v-model="orderData.house_number" id="house_number" placeholder="numer domu"
                                         required="true"
                                         oldvalue="{{ $order->house_number }}" :error="orderErrors.house_number"></div>
                                </div>
                            </div>

                            <h3>dostawa</h3>
                            @if($order->delivery_price > 0)
                                <div class="order-form__checkbox-group">
                                    @foreach($delivery_types as $key => $item)
                                        @if($key % 2 == 0)
                                            <div class="order-form__checkbox-group-col">
                                                @endif
                                                <label class="checkbox efect-form__checkbox">
                                                    <input type="radio" name="delivery" v-model="orderData.delivery"
                                                           :value="JSON.stringify({ id: {{ $item->id }}, price: {{ $item->price }}})"
                                                            {{ $item->name === $order->delivery_type ? 'checked ref=chosenDelivery' : '' }}>
                                                    <span class="checkbox__marker"></span>
                                                    {{ $item->name }}: {{ $item->price }} zł
                                                </label>
                                                @if(($key + 1) % 2 == 0)
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @else
                                <p class="info-list__info-line">za darmo</p>
                            @endif

                            <h3>płatność</h3>
                            <div class="order-form__checkbox-group">
                                <label class="checkbox efect-form__checkbox">
                                    <input type="radio" name="payment" v-model="orderData.payment"
                                           value="PayPal" {{ $order->payment_type === 'PayPal' ? ('checked ref=chosenPayment') : '' }}>
                                    <span class="checkbox__marker"></span>
                                    PayPal
                                </label>
                            </div>

                            <footer class="order-form__footer">
                                <div class="checkbox efect-form__checkbox">
                                    <input type="checkbox" id="privacy_policy" name="privacy_policy"
                                           v-model="orderData.privacy_policy">
                                    <label for="privacy_policy" class="checkbox__marker"></label>
                                    akceptuję <a href="#" target="_blank" class="efect-form__checkbox-link">politykę
                                        prywatności</a><span
                                            class="efect-form__required-star">*</span>
                                </div>
                                <div class="checkbox efect-form__checkbox">
                                    <input type="checkbox" id="regimen" name="regimen" v-model="orderData.regimen">
                                    <label for="regimen" class="checkbox__marker"></label>
                                    akceptuję <a href="#" target="_blank"
                                                 class="efect-form__checkbox-link">regulamin</a><span
                                            class="efect-form__required-star">*</span>
                                </div>
                                <p><span class="efect-form__required-star">*</span> pola wymagane</p>
                                <div class="efect-form__buttons" v-cloak>
                                    <button class="btn" type="submit"
                                            :class="{ 'btn_disabled': (sending || uncheckedCheckboxes)}"
                                            :disabled="(sending || uncheckedCheckboxes)">dalej
                                    </button>
                                    <a href="{{ session()->has('editable_order')
                                    ? route('profile-basket')
                                    : route('order-basket-restore', ['uid' => $order->order_number]) }}">wróć do
                                        koszyka</a>
                                </div>
                            </footer>

                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection