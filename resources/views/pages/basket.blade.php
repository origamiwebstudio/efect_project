@extends('layouts.main')

@section('page')
	<div id="basketpage">
		<div class="page-header">
			<div class="container">
				{{--<h1>twój koszyk @if($basket && count($basket) > 0)<span v-if="!scriptTriggered">({{ count($basket) }} produkty)</span><span v-cloak v-else-if="scriptTriggered && itemsLength">(@{{ itemsLength }})</span>@endif</h1>--}}
				<h1>
					twój koszyk
					@if($basket && count($basket) > 0)
						<span v-if="!scriptTriggered">({{ count($basket) }})</span>
						<span v-cloak v-else-if="scriptTriggered && itemsLength">(@{{ itemsLength }})</span>
					@endif
				</h1>
				<a href="{{ route('home') }}" class="back-btn">wróć do strony głównej</a>
			</div>
		</div>

		<div class="basket">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						@if($basket && count($basket) > 0)
							<ul class="basket-list" ref="basketItems">
								@foreach($basket as $item)
									<li>
										<button class="close-btn" @click="removeBasketItem($event, '{{ $item['basket_uid']}}', 'basket')"></button>
										<div class="basket-list__image-part">
											<a href="{{ route('product', ['id' => $item['product']->id]) }}">
												<img src="{{ asset($item['product']->main_image->link) }}" alt="{{ $item['product']->name }}">
												<span class="basket-list__overlay"></span>
												<span class="basket-list__special-mark"></span>
											</a>
										</div>
										<div class="basket-list__info-part">
											<header>
												<h2><a href="{{ route('product', ['id' => $item['product']->id]) }}">{{ $item['product']->name }}</a></h2>
												<span>#{{ $item['product']->uid }}</span>
											</header>
											@if(is_null($item['product']->offer_price))
												<span class="price">{{ $item['product']->price }} PLN</span>
											@else
												<span class="price price_new">{{ $item['product']->offer_price }} PLN</span>
												<span class="price price_old">{{ $item['product']->price }} PLN</span>
											@endif
											<ul class="basket-list__additional-info">
												<li>
													<p>rozmiar</p>
													<span>{{ $item['size'] }}</span>
												</li>
												<li>
													<p>kolor</p>
													<span style="background-color: {{ $item['color'] }};"></span>
												</li>
											</ul>
										</div>
										{{--<div class="basket-list__right-part">--}}
											{{--<select class="select">--}}
												{{--@for($i = 1; $i <= 5; $i++)--}}
													{{--<option value="{{ $i }}" {{ $item['quantity'] == $i ? 'selected' : '' }}>{{ $i }}</option>--}}
												{{--@endfor--}}
											{{--</select>--}}
											{{--<span class="price">{{ round(($item['product']->offer_price ? $item['product']->offer_price : $item['product']->price) * $item['quantity'], 2) }} PLN</span>--}}
										{{--</div>--}}
										<basket-product-quantity
												:quantity="{{ $item['quantity'] }}"
												:price="{{ $item['product']->offer_price ? $item['product']->offer_price : $item['product']->price }}"
												:uid="'{{ $item['basket_uid']}}'"
												@refetchbasket="refetchBasket"
										></basket-product-quantity>
									</li>
								@endforeach
							</ul>
						@endif
						<p class="basket__no-products" v-cloak v-if="itemsLength < 1">Nie dodano żadnego produktu</p>
					</div>
					<div class="col-md-3">
						<aside is="basket-sidebar"
							   ref="basketSidebar"
							   :button-text="'złóź zamówienie'"
							   :link="'{{ route('order') }}'">
						</aside>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection