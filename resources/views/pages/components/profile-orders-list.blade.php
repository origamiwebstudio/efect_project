@foreach($orders as $order)
	<li class="profile-orders__item" is="profile-order-item" key="order_{{ $order->order_number }}">
		<div class="short-info">
			<div class="info-group info-group_order-number">
				<span class="order-number">zamówienie #{{ $order->order_number }}</span>
			</div>
			<div class="info-group">
				<span class="title">produktów: </span>
				<span class="value">{{ $order->cart->products->count() }}</span>
			</div>
			<div class="info-group">
				<span class="title">data: </span>
				<span class="value">{{ $order->created_at->format('d.m.Y') }}</span>
			</div>
			<div class="info-group">
				<span class="title">kwota: </span>
				<span class="value">{{ $order->cart->products_summary_price }} PLN</span>
			</div>
			<div class="info-group info-group_status">
				<span class="title">status: </span>
				<span class="value">{{ $order->status }}</span>
			</div>
			<span class="arrow-down"></span>
		</div>
		<div class="detail-info" slot="detail-info" v-cloak>
			<div class="order-info-list">
				<div class="list-column">
					<div class="info-column info-column_first">
						<span class="title">imię:</span>
						<span class="title">nazwisko:</span>
					</div>
					<div class="info-column">
						<span class="value">{{ $order->name }}</span>
						<span class="value">{{ $order->surname }}</span>
					</div>
				</div>
				<div class="list-column">
					<div class="info-column info-column_first">
						<span class="title">telefon:</span>
						<span class="title">e-mail:</span>
					</div>
					<div class="info-column">
						<span class="value">{{ $order->phone }}</span>
						<span class="value">{{ $order->email }}</span>
					</div>
				</div>
				<div class="list-column">
					<div class="info-column info-column_first">
						<span class="title">miejscowość:</span>
						<span class="title">kod pocztowy:</span>
					</div>
					<div class="info-column">
						<span class="value">{{ $order->locality }}</span>
						<span class="value">{{ $order->postal_code }}</span>
					</div>
				</div>
				<div class="list-column">
					<div class="info-column info-column_first">
						<span class="title">ulica:</span>
						<span class="title">numer domu:</span>
					</div>
					<div class="info-column">
						<span class="value">{{ $order->street }}</span>
						<span class="value">{{ $order->house_number }}</span>
					</div>
				</div>
			</div>

			@if(isset($order->cart->products) && count($order->cart->products) > 0)
				<div class="order-products-list">
					<ul class="basket-list">
						@foreach($order->cart->products as $product)
							<li>
								<div class="basket-list__image-part">
									<a href="{{ route('product', ['id' => $product->id]) }}">
										<img src="{{ asset($product->main_image->link) }}" alt="{{ $product->name }}">
										<span class="basket-list__overlay"></span>
										<span class="basket-list__special-mark"></span>
									</a>
								</div>
								<div class="basket-list__info-part">
									<header>
										<h2><a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a></h2>
										<span>#{{ $product->uid }}</span>
									</header>
									<span class="price">{{ $product->pivot->actual_price }} PLN</span>
									<ul class="basket-list__additional-info">
										<li>
											<p>rozmiar</p>
											<span>{{ $product->pivot->size }}</span>
										</li>
										<li>
											<p>kolor</p>
											<span style="background-color: {{ $product->pivot->color }};"></span>
										</li>
									</ul>
								</div>
								<div class="basket-list__right-part basket-list__right-part_loaded">
									<span class="basket-list__quantity-ordered">illość produktów: <span>{{ $product->pivot->quantity }}</span></span>
									<span class="basket-list__right-part-price">{{ round($product->pivot->quantity * $product->pivot->actual_price, 2) }} PLN</span>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			@endif

		</div>
	</li>
@endforeach