@foreach($products as $product)
	<li class="product {{ $product->availability ? '' : 'product_absent' }}">

		<div class="product__image-wrapper">

			<img v-lazy="'{{ asset($product->main_image->link) }}'" alt="{{ $product->name }}">

			<add-to-basket-modal
					:id="{{ $product->id }}"
					:name="'{{ $product->name }}'"
					:uid="'{{ $product->uid }}'"
					:link="'{{ route('product', ['id' => $product->id]) }}'"
					:image="'{{ asset($product->main_image->link) }}'"
					:price="'{{ $product->price }}'"
					@if($product->is_favorite):favorite="true" @endif
					@if(auth()->user()) :authorized="true" @endif
					@if($product->offer_price):offer_price="'{{ $product->offer_price }}'" @endif
					:colors="{{ $product->colors->pluck('hex') }}"
					:sizes="[{{ $product->getAvailableSizes('string') }}]"
			></add-to-basket-modal>

			<div class="special-marks">
				@if(!$product->availability)
					<p class="special-marks__info">brak</p><br>
				@elseif($product->novelty)
					<p class="special-marks__info">nowość</p><br>
				@endif
				@if(!is_null($product->offer_price))
					@if($product->offer_type == 'fix_price')
						<span class="special-marks__offer">SUPER CENA</span>
					@else
						@if($product->offer_type == 'fix_discount')
							<span class="special-marks__offer special-marks__offer_discount">{{ $product->offer_value }} zł</span>
						@else
							<span class="special-marks__offer special-marks__offer_discount">{{ $product->offer_value }}%</span>
						@endif
					@endif
				@endif
			</div>

		</div>

		<div class="product__description">
			<a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a>
			@if(is_null($product->offer_price))
				<span class="price">{{ $product->price }} PLN</span>
			@else
				<span class="price price_new">{{ $product->offer_price }} PLN</span>
				<span class="price price_old">{{ $product->price }} PLN</span>
			@endif
		</div>

	</li>
@endforeach