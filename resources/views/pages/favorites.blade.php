@extends('layouts.main')

@section('page')
	<div id="basketpage">

		<div class="page-header">
			<div class="container">
				<h1>
					ulubione
					@if($favorites && count($favorites) > 0)
						<span v-if="!scriptTriggered">({{ count($favorites) }})</span>
						<span v-cloak v-else-if="scriptTriggered && itemsLength">(@{{ itemsLength }})</span>
					@endif
				</h1>
				<a href="{{ route('home') }}" class="back-btn">wróć do strony głównej</a>
			</div>
		</div>

		<div class="basket">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						@if($favorites && count($favorites) > 0)
							<ul class="basket-list" ref="basketItems">
								@foreach($favorites->sortByDesc('created_at') as $key => $product)
									<li>
										<button class="close-btn" @click="removeBasketItem($event, {{ $product->id }}, 'favorite')"></button>
										<div class="basket-list__image-part">
											<a href="{{ route('product', ['id' => $product->id]) }}">
												<img src="{{ asset($product->main_image->link) }}" alt="">
												<span class="basket-list__overlay"></span>
												<span class="basket-list__special-mark"></span>
											</a>
										</div>
										<div class="basket-list__info-part">
											<header>
												<h2><a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a></h2>
												<span>#{{ $product->uid }}</span>
											</header>
											@if(is_null($product->offer_price))
												<span class="price">{{ $product->price }} PLN</span>
											@else
												<span class="price price_new">{{ $product->offer_price }} PLN</span>
												<span class="price price_old">{{ $product->price }} PLN</span>
											@endif
										</div>
										@if($product->availability)
										<add-to-basket-modal-favorite
												@refetchbasket="refetchBasket"
												:id="{{ $product->id }}"
												:name="'{{ $product->name }}'"
												:uid="'{{ $product->uid }}'"
												:link="'{{ route('product', ['id' => $product->id]) }}'"
												:image="'{{ asset($product->main_image->link) }}'"
												:price="'{{ $product->price }}'"
												@if($product->offer_price):offer_price="'{{ $product->offer_price }}'" @endif
												:colors="{{ $product->colors->pluck('hex') }}"
												:sizes="[{{ $product->getAvailableSizes('string') }}]"
										></add-to-basket-modal-favorite>
										@else
											<div class="basket-list__right-part">
												<button class="btn btn_disabled" disabled title="brak produktu">dodaj w koszyk</button>
											</div>
										@endif
									</li>
								@endforeach
							</ul>
						@endif
						<p class="basket__no-products" v-cloak v-if="itemsLength < 1">Nie dodano żadnego produktu</p>
					</div>
					<div class="col-md-3">
						<aside is="basket-sidebar"
							   ref="basketSidebar"
							   :button-text="'mój koszyk'"
							   :link="'{{ route('basket') }}'">
						</aside>
					</div>
				</div>
			</div>
		</div>

	</div>

@endsection