@extends('pages.profile.profile-layout')

@section('profile-page')

	<div class="profile-content" id="userorders">
		<div class="container">
			<div class="profile-content__tab-links">
				<a href="#openedOrders" :class="{ 'active': (currentTab === 'openedorders' ) }" @click.prevent="currentTab = 'openedorders'">otwarte zamówienia</a>
				@if($orders->where('order_status', '=', 'done')->count() > 0)
					<br>
					<a href="#closedOrders" :class="{ 'active': (currentTab === 'closedorders' ) }" @click.prevent="currentTab = 'closedorders'">zamknięte zamówienia</a>
				@endif
			</div>
			@if(isset($orders))
				<ul class="profile-orders" v-if="currentTab === 'openedorders'">
					@include('pages.components.profile-orders-list', ['orders' => $orders->where('order_status', '=', 'realization')])
				</ul>
				@if($orders->where('order_status', '=', 'done')->count() > 0)
					<ul class="profile-orders" v-cloak v-if="currentTab === 'closedorders'">
						@include('pages.components.profile-orders-list', ['orders' => $orders->where('order_status', '=', 'done')])
					</ul>
				@endif
			@endif
		</div>
	</div>

@endsection