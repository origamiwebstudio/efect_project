@extends('layouts.main')

@section('page')

	<div class="profile-pages">

		<header class="profile-header">
			<div class="container">
				<nav>
					<ul>
						<li>
							@if(request()->route()->getName() === 'profile')
								<span class="active">moje konto</span>
							@else
								<a href="{{ route('profile') }}">moje konto</a>
							@endif
						</li>
						<li>
							@if(request()->route()->getName() === 'profile-favorites')
								<span class="active">uliubione <span class="counter" v-if="!favoritesCount.show">{{ auth()->user()->favorites()->count() }}</span><span class="counter" v-cloak v-else-if="favoritesCount.show">@{{ favoritesCount.value }}</span></span>
							@else
								<a href="{{ route('profile-favorites') }}">uliubione <span class="counter" v-if="!favoritesCount.show" ref="favoritesCount" data-count="{{ auth()->user()->favorites()->count() }}">{{ auth()->user()->favorites()->count() }}</span><span class="counter" v-cloak v-else-if="favoritesCount.show">@{{ favoritesCount.value }}</span></a>
							@endif
						</li>
						<li>
							@if(request()->route()->getName() === 'profile-basket')
								<span class="active">mój koszyk <span class="counter" v-if="!basketCount.show">{{ \App\Cart::getProductsCount() }}</span><span class="counter" v-cloak v-else-if="basketCount.show">@{{ basketCount.value }}</span></span>
							@else
								<a href="{{ route('profile-basket') }}">mój koszyk <span class="counter" v-if="!basketCount.show" ref="basketCount" data-count="{{ \App\Cart::getProductsCount() }}">{{ \App\Cart::getProductsCount() }}</span><span class="counter" v-cloak v-else-if="basketCount.show">@{{ basketCount.value }}</span></a>
							@endif
						</li>
						<li>
							@if(request()->route()->getName() === 'profile-orders')
								<span class="active">moje zamówienia <span class="counter">{{ \App\Order::getCountUserNewOrders() }}</span></span>
							@else
								<a href="{{ route('profile-orders') }}">moje zamówienia <span class="counter">{{ \App\Order::getCountUserNewOrders() }}</span></a>
							@endif
						</li>
						<li>
							@if(request()->route()->getName() === 'profile-last-viewed')
								<span class="active">ostatnio oglądane produkty</span>
							@else
								<a href="{{ route('profile-last-viewed') }}">ostatnio oglądane produkty</a>
							@endif
						</li>
					</ul>
				</nav>
			</div>
		</header>

		@yield('profile-page')

	</div>

@endsection