@extends('pages.profile.profile-layout')

@section('profile-page')
	<div class="container">
		@if(isset($viewed_products) && count($viewed_products) > 0)
			<section class="last-seen-products" id="lastseenproducts">
				<ul>
					@foreach($viewed_products as $viewed_product)
						<li>
							<div class="product {{ $viewed_product->availability ? '' : 'product_absent' }}">
								<div class="product__image-wrapper">
									<img v-lazy="'{{ asset($viewed_product->main_image->link) }}'" alt="{{ $viewed_product->name }}">
									<add-to-basket-modal
											id="{{ $viewed_product->id }}"
											:name="'{{ $viewed_product->name }}'"
											:uid="'{{ $viewed_product->uid }}'"
											:link="'{{ route('product', ['id' => $viewed_product->id]) }}'"
											:image="'{{ asset($viewed_product->main_image->link) }}'"
											:price="'{{ $viewed_product->price }}'"
											@if($viewed_product->is_favorite):favorite="true" @endif
											@if(auth()->user()) :authorized="true" @endif
											@if($viewed_product->offer_price):offer_price="'{{ $viewed_product->offer_price }}'" @endif
											:colors="{{ $viewed_product->colors->pluck('hex') }}"
											:sizes="[{{ $viewed_product->getAvailableSizes('string') }}]"
											@addedfavorite="incrementFavorite"
											@removedfavorite="decrementFavorite"
											@addedbasket="incrementBasket"
									></add-to-basket-modal>

									<div class="special-marks">
										@if(!$viewed_product->availability)
											<p class="special-marks__info">brak</p><br>
										@elseif($viewed_product->novelty)
											<p class="special-marks__info">nowość</p><br>
										@endif
										@if(!is_null($viewed_product->offer_price))
											@if($viewed_product->offer_type == 'fix_price')
												<span class="special-marks__offer">SUPER CENA</span>
											@else
												@if($viewed_product->offer_type == 'fix_discount')
													<span class="special-marks__offer special-marks__offer_discount">{{ $viewed_product->offer_value }} zł</span>
												@else
													<span class="special-marks__offer special-marks__offer_discount">{{ $viewed_product->offer_value }}%</span>
												@endif
											@endif
										@endif
									</div>
								</div>
								<div class="product__description">
									<a href="{{ route('product', ['id' => $viewed_product->id]) }}">{{ $viewed_product->name }}</a>
									@if(is_null($viewed_product->offer_price))
										<span class="price">{{ $viewed_product->price }} PLN</span>
									@else
										<span class="price price_new">{{ $viewed_product->offer_price }} PLN</span>
										<span class="price price_old">{{ $viewed_product->price }} PLN</span>
									@endif
								</div>
							</div>
						</li>
					@endforeach
				</ul>
			</section>
		@else
			<p class="basket__no-products">Nie oglądano żadnego produktu</p>
		@endif
	</div>
@endsection