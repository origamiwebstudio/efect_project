@extends('pages.profile.profile-layout')

@section('profile-page')

    <div class="profile-content" id="profilepage">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-lg-push-10 col-md-3 col-md-push-9 col-sm-4 col-sm-push-8">
                    <aside class="profile-aside">
                        <h1>moje konto</h1>
                        <nav>
                            <ul>
                                <li><a href="#edit_data" @click.prevent="currentTab = 'edit_data'">edytuj dane
                                        osobowe</a></li>
                                <li><a href="{{ url('/password/reset') }}">zmień hasło</a></li>
                                <li>
                                    <form action="{{ url('/logout') }}" method="POST">
                                        {{ csrf_field() }}
                                        <button>wyjść</button>
                                    </form>
                                </li>
                            </ul>
                        </nav>
                    </aside>
                </div>
                <div class="col-lg-pull-2 col-md-9 col-md-pull-3 col-sm-8 col-sm-pull-4">

                    <div class="profile-content__tab" id="view_data" v-show="currentTab === 'view_data'">
                        <div class="info-list">

                            <h3>dane kontaktowe</h3>
                            <dl>
                                <div class="info-list__item">
                                    <dt>imię:</dt>
                                    <dd>
                                        <span v-if="!updatedData">{{ $user->name }}</span>
                                        <span v-else v-cloak>@{{ updatedData.name }}</span>
                                    </dd>
                                </div>
                                <div class="info-list__item">
                                    <dt>nazwisko:</dt>
                                    <dd>
                                        <span v-if="!updatedData">{{ $user->surname }}</span>
                                        <span v-else v-cloak>@{{ updatedData.surname }}</span>
                                    </dd>
                                </div>
                                <div class="info-list__item">
                                    <dt>e-mail:</dt>
                                    <dd>
                                        <span v-if="!updatedData">{{ $user->email }}</span>
                                        <span v-else v-cloak>@{{ updatedData.email }}</span>
                                    </dd>
                                </div>
                                @if($user->phone)
                                    <div class="info-list__item">
                                        <dt>telefon:</dt>
                                        <dd>
                                            <span v-if="!updatedData">{{ $user->phone }}</span>
                                            <span v-else v-cloak>@{{ updatedData.phone }}</span>
                                        </dd>
                                    </div>
                                @endif
                            </dl>

                            @if($user->locality && $user->postal_code && $user->street && $user->house_number)
                                <h3>dane do wysyłki</h3>
                                <dl>
                                    <div class="info-list__item">
                                        <dt>miejscowość:</dt>
                                        <dd>
                                            <span v-if="!updatedData">{{ $user->locality }}</span>
                                            <span v-else v-cloak>@{{ updatedData.locality }}</span>
                                        </dd>
                                    </div>
                                    <div class="info-list__item">
                                        <dt>kod posztowy:</dt>
                                        <dd>
                                            <span v-if="!updatedData">{{ $user->postal_code }}</span>
                                            <span v-else v-cloak>@{{ updatedData.postal_code }}</span>
                                        </dd>
                                    </div>
                                    <div class="info-list__item">
                                        <dt>ulica:</dt>
                                        <dd>
                                            <span v-if="!updatedData">{{ $user->street }}</span>
                                            <span v-else v-cloak>@{{ updatedData.street }}</span>
                                        </dd>
                                    </div>
                                    <div class="info-list__item">
                                        <dt>numer domu:</dt>
                                        <dd>
                                            <span v-if="!updatedData">{{ $user->house_number }}</span>
                                            <span v-else v-cloak>@{{ updatedData.house_number }}</span>
                                        </dd>
                                    </div>
                                </dl>
                            @endif

                        </div>
                    </div>

                    <div class="profile-content__tab" id="edit_data" v-cloak v-show="currentTab === 'edit_data'">
                        <form action="{{ route('profile-edit') }}" class="efect-form" method="post"
                              @submit.prevent="editProfile" :class="{'efect-form_loading': sending }">
                            {{ csrf_field() }}
                            <h3>dane kontaktowe</h3>
                            <div class="profile-content__input-groups profile-content__input-groups_col-2">
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="name"
                                         key="name"
                                         id="name"
                                         placeholder="imię"
                                         required="true"
                                         v-model="editData.name"
                                         :error="errors.name"
                                         :oldvalue="'{{ $user->name }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="surname"
                                         key="surname"
                                         id="surname"
                                         placeholder="nazwisko"
                                         required="true"
                                         v-model="editData.surname"
                                         :error="errors.surname"
                                         :oldvalue="'{{ $user->surname }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="email"
                                         name="email"
                                         key="email"
                                         id="email"
                                         readonly="true"
                                         placeholder="email"
                                         required="true"
                                         v-model="editData.email"
                                         :error="errors.email"
                                         :oldvalue="'{{ $user->email }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="phone"
                                         key="phone"
                                         id="phone"
                                         placeholder="telefon"
                                         v-model="editData.phone"
                                         :error="errors.phone"
                                         :oldvalue="'{{ $user->phone }}'"></div>
                                </div>
                            </div>
                            <h3>dane do wysyłki</h3>
                            <div class="profile-content__input-groups profile-content__input-groups_col-2">
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="country"
                                         key="country"
                                         id="country"
                                         placeholder="kraj"
                                         v-model="editData.country"
                                         :error="errors.country"
                                         :oldvalue="'{{ $user->country }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="locality"
                                         key="locality"
                                         id="locality"
                                         placeholder="miejscowość"
                                         v-model="editData.locality"
                                         :error="errors.locality"
                                         :oldvalue="'{{ $user->locality }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="postal_code"
                                         key="postal_code"
                                         id="postal_code"
                                         placeholder="kod posztowy"
                                         v-model="editData.postal_code"
                                         :error="errors.postal_code"
                                         :oldvalue="'{{ $user->postal_code }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="street"
                                         key="street"
                                         id="street"
                                         placeholder="ulica"
                                         v-model="editData.street"
                                         :error="errors.street"
                                         :oldvalue="'{{ $user->street }}'"></div>
                                </div>
                                <div class="efect-form__form-group">
                                    <div is="efect-input"
                                         class="efect-form__input"
                                         type="text"
                                         name="house_number"
                                         key="house_number"
                                         id="house_number"
                                         placeholder="numer domu"
                                         v-model="editData.house_number"
                                         :error="errors.house_number"
                                         :oldvalue="'{{ $user->house_number }}'"></div>
                                </div>
                            </div>
                            <div class="efect-form__buttons">
                                <div class="efect-form__buttons">
                                    <button type="submit" class="btn" :class="{'btn_disabled': sending}"
                                            :disabled="sending">zmienić
                                    </button>
                                    <a href="#view_data" @click.prevent="currentTab = 'view_data'">wróć do profila</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection