@extends('pages.profile.profile-layout')

@section('profile-page')

	<div class="basket" id="basketpage">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-8">
					@if($cart->products && count($cart->products) > 0)
						<ul class="basket-list basket-list_basket" ref="basketItems" data-type="basket">
							@foreach($cart->products as $product)
								<li>
									<button class="close-btn"
													@click="removeBasketItem($event, '{{ $product->pivot->id }}')"></button>
									<div class="basket-list__image-part">
										<a href="{{ route('product', ['id' => $product->id]) }}">
											<img src="{{ asset($product->main_image->link) }}"
													 alt="{{ $product->name }}">
											<span class="basket-list__overlay"></span>
											<span class="basket-list__special-mark"></span>
										</a>
									</div>
									<div class="basket-list__info-part">
										<header>
											<h2>
												<a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a>
											</h2>
											<span>#{{ $product->uid }}</span>
										</header>
										@if(is_null($product->offer_price))
											<span class="price">{{ $product->price }} PLN</span>
										@else
											<span class="price price_new">{{ $product->offer_price }} PLN</span>
											<span class="price price_old">{{ $product->price }} PLN</span>
										@endif
										<ul class="basket-list__additional-info">
											<li>
												<p>rozmiar</p>
												<span>{{ $product->pivot->size }}</span>
											</li>
											<li>
												<p>kolor</p>
												<span style="background-color: {{ $product->pivot->color }};"></span>
											</li>
										</ul>
									</div>
									<div class="basket-list__right-part" is="basket-product-quantity"
											 :quantity="{{ $product->pivot->quantity }}"
											 :price="{{ $product->offer_price ?: $product->price }}"
											 :uid="'{{ $product->pivot->id }}'"
											 @refetchbasket="refetchBasket"
									></div>
								</li>
							@endforeach
						</ul>
					@endif
					<p class="basket__no-products" v-cloak v-if="basketCount.value < 1">Nie dodano żadnego produktu</p>
				</div>
				<div class="col-md-3 col-sm-4">
					<aside class="order-aside order-aside_favorite"
								 is="basket-sidebar"
								 ref="basketSidebar"
								 :button-text="'złóź zamówienie'"
								 :link="'{{ route('order') }}'">
					</aside>
				</div>
			</div>
		</div>
	</div>
@endsection