@extends('layouts.main')

@section('page')

    @if(isset($slider_images) && count($slider_images) > 0)
        <section class="s-hero">
            <div class="swiper" is="swiper" :options="sliderOptions">
                @foreach($slider_images->sortBy('order_') as $image)
                    <swiper-slide>
                        <img data-src="{{ asset($image->link) }}" alt="{{ $image->name }}" class="swiper-lazy">
                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                    </swiper-slide>
                @endforeach
                <div class="swiper-pagination" slot="pagination"></div>
            </div>
        </section>
    @endif

    @if(isset($products))
        <section class="s-products" id="products">
            <div class="filters">
                <div class="container">
                    <a href="#new" :class="{'active' : currentTab === 'new'}" @click.prevent="currentTab = 'new'">new
                        collection</a>
                    <a href="#bestseller" :class="{'active' : currentTab === 'bestseller'}"
                       @click.prevent="currentTab = 'bestseller'">bestseller</a>
                    <a href="#sale" :class="{'active' : currentTab === 'sale'}" @click.prevent="currentTab = 'sale'">sale</a>
                </div>
            </div>

            <div class="products">
                <div class="container">
                @if(isset($products['bestseller']) && count($products['bestseller']) > 0)
                    <ul id="bestseller" v-show="currentTab === 'bestseller'">
                        @include('pages.components.products-list', ['products' => $products['bestseller']])
                    </ul>
                @endif
                @if(isset($products['new']) && count($products['new']) > 0)
                    <ul id="new" v-cloak v-show="currentTab === 'new'">
                        @include('pages.components.products-list', ['products' => $products['new']])
                    </ul>
                @endif
                @if(isset($products['sale']) && count($products['sale']) > 0)
                    <ul id="sale" v-cloak v-show="currentTab === 'sale'">
                        @include('pages.components.products-list', ['products' => $products['sale']])
                    </ul>
                @endif
                </div>
            </div>
            <a href="{{ route('products') }}" class="btn s-products__see-more-btn">zobacz wszystkie</a>
        </section>
    @endif

@endsection