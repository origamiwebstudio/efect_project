@extends('layouts.main')

@section('page')

	<div class="page-header">
		<div class="container">
			<h1>kontakt</h1>
			<a href="{{ route('home') }}" class="back-btn">wróć do strony głównej</a>
		</div>
	</div>

	<div id="contactspage" class="contacts-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-7">
					<div class="contacts-left-part">
						<figure class="contacts-left-part__image">
							<img src="{{ asset('images/contacts-bg.jpg') }}" alt="Efect market">
						</figure>
						<div class="contacts-left-part__information">
							<div class="contacts-left-part__information-block">
								<h3>sklep internetowy</h3>
								<p><span>tel:</span> 790 211 096 / pon-piątek w godz. 9.00 - 18.00</p>
								<p><span>email: </span><a href="#">efect1@gmail.com</a></p>
							</div>
							<div class="contacts-left-part__information-block">
								<h3>biuro</h3>
								<p><span>tel:</span> 790 211 096 / pon-piątek w godz. 9.00 - 18.00</p>
								<p><span>email: </span><a href="#">efect2@gmail.com</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-5">
					<div class="contacts-right-part">
						<h2>chcesz się przywitać z <span>E</span>FECT?</h2>
						<form action="{{ route('contacts-send') }}"
									method="post"
									class="efect-form"
									id="contactsform"
									@submit.prevent="sendForm"
									:class="{'efect-form_loading': sending }">
							{{ csrf_field() }}
							<div class="efect-form__form-group">
								<div is="efect-input"
										 class="efect-form__input"
										 type="text"
										 name="name"
										 placeholder="imię"
										 required="true"
										 v-model="formData.name"
										 :error="errors.name"
										 :oldvalue="'{{ old('name') ?: '' }}'"></div>
							</div>
							<div class="efect-form__form-group">
								<div is="efect-input"
										 class="efect-form__input"
										 type="email"
										 name="email"
										 placeholder="e-mail"
										 required="true"
										 v-model="formData.email"
										 :error="errors.email"
										 :oldvalue="'{{ old('email') ?: '' }}'"></div>
							</div>
							<div class="efect-form__form-group">
								<div is="efect-input"
										 class="efect-form__input"
										 type="text"
										 name="phone"
										 placeholder="telefon"
										 v-model="formData.phone"
										 :error="errors.phone"
										 :oldvalue="'{{ old('phone') ?: '' }}'"></div>
							</div>
							<div class="efect-form__form-group">
								<div is="efect-input"
										 class="efect-form__input"
										 type="textarea"
										 name="message"
										 placeholder="message"
										 required="true"
										 v-model="formData.message"
										 :error="errors.message"
										 :oldvalue="'{{ old('message') ?: '' }}'"></div>
							</div>
							<div class="efect-form__form-group efect-form__form-group_captcha" ref="captcha">
								{!! $captcha->display() !!}
								<span class="efect-form__error-message" v-if="errors['g-recaptcha-response']" v-cloak>@{{ errors['g-recaptcha-response'][0] }}</span>
							</div>
							<button type="submit" class="btn" :class="{ 'btn_disabled': sending }" :disabled="sending">wysłać</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! $captcha->script()!!}

@endsection