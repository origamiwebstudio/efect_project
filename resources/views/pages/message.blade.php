@extends('layouts.main')

@section('page')

	<section class="message-block">
		<div class="message-block__content">

			<div class="{{ isset($type) && $type === 'error' ? 'fail-mark' : 'success-mark' }}"></div>

			@if(isset($title))
				<h1>{{ $title }}</h1>
			@endif

			@if(isset($message))
				<p>{{ $message }}</p>
			@endif

			<div class="message-block__buttons">

				@if(isset($links) && count($links) > 0)

					@foreach($links as $link)
						<a href="{{ $link['url'] }}" class="{{ isset($link['type']) && $link['type'] === 'button' ? 'btn' : '' }}">{{ $link['title'] }}</a>
					@endforeach

				@else
					<a href="{{ route('home') }}" class="btn">strona główna</a>
				@endif

			</div>
		</div>
	</section>

@endsection