@extends('layouts.main')

@section('page')

    <div class="page-header">
        <div class="container">
            <h1>o nas</h1>
            <a href="{{ route('home') }}" class="back-btn">wróć do strony głównej</a>
        </div>
    </div>

    <section class="s-about">
        <div class="container">
            <div class="s-about__image">
                <figure>
                    <img src="{{ asset('images/about-bg-sample.jpg') }}" alt="about background sample">
                </figure>
            </div>
            <p>Jesteśmy firmą, która od wielu lat z sukcesem kultywuje tradycję branży odzieżowej, czerpiemy inspirację z ulic europejskich stolic świata mody.</p>
            <p>Nasza oferta skierowana jest do osób ceniących wygodę, funkcjonalność oraz nowoczesność. Nasze ubrania są starannie wyselekcjonowane dzięki czemu są zawsze na czasie, dzięki naszym ubraniom będziesz wyglądać modnie i indywidualnie.</p>
            <p>Proponujemy kompletne stylizacje, które mogą stać się Waszą inspiracją. Nasze propozycje to coś więcej niż ubrania, to najnowsze trendy w zasięgu Twojej ręki. Niezmiennie dbamy o zadowolenie i satysfakcję naszych klientów.</p>
            <p>Produkty zamówione przez Was, nasz zespół stara się pakować ze szczególną starannością i dbałością o szczegóły.</p>
            <p>Zależy nam na 100% satysfakcji naszych klientów.</p>
        </div>
    </section>



@endsection