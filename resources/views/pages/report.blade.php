@extends('layouts.main')

@section('page')

	<div class="page-header">
		<div class="container">
			<h1>potwierdź dane</h1>
		</div>
	</div>

	<section class="report-page">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-md-push-9 col-sm-4 col-sm-push-8">
					<aside class="order-aside order-aside_loaded">
						<header>
							<h3>moje zamówienie</h3>
							<span>illość produktów: {{ $cart->getProductsCount() }}</span>
						</header>
						<ul>
							@foreach($cart->products as $product)
								<li><a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }} <span>({{ $product->pivot->quantity }})</span></a></li>
							@Endforeach
						</ul>
						<footer>
							<dl>
								<dt>wartość zamówienia:</dt>
								<dd>{{ $cart->getProductsSummaryPrice() }} PLN</dd>
								<dt>koszt przesyłki:</dt>
								<dd>
									{{ $order['delivery_price'] == 0 ? '0.00' : $order['delivery_price'] }} PLN
									@if($order['delivery_type'] == 0)
										<div class="hint tooltip">
											<span class="hint__questioned-mark">?</span>
											<span class="tooltip__message">Darmowa dostawa przy zakupach od {{ config('efect.free_delivery') }} zł</span>
										</div>
									@endif
								</dd>
								<dt>do zapłaty:</dt>
								<dd class="order-aside__total">{{ $cart->getProductsSummaryPrice() + $order['delivery_price'] }} PLN</dd>
							</dl>
						</footer>
					</aside>
				</div>
				<div class="col-md-9 col-md-pull-3 col-sm-8 col-sm-pull-4">
					<form action="{{ route('make-payment') }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="order_number" value="{{ $order['order_number'] }}">
						<div class="info-list">
							<h3 class="info-list__title">dane kontaktowe</h3>
							<dl>
								<div class="info-list__item">
									<dt>imię:</dt>
									<dd>{{ $order['name'] }}</dd>
								</div>
								<div class="info-list__item">
									<dt>nazwisko:</dt>
									<dd>{{ $order['surname'] }}</dd>
								</div>
								<div class="info-list__item">
									<dt>e-mail:</dt>
									<dd>{{ $order['email'] }}</dd>
								</div>
								<div class="info-list__item">
									<dt>telefon:</dt>
									<dd>{{ $order['phone'] }}</dd>
								</div>
							</dl>
							<h3 class="info-list__title">dane do wysyłki</h3>
							<p class="info-list__info-line">{{ $order['country'] }}, {{ $order['locality'] }}, {{ $order['postal_code'] }}, {{ $order['street'] . ' ' . $order['house_number'] }}</p>
							<h3 class="info-list__title">dostawa</h3>
							<p class="info-list__info-line">{{ $order['delivery_price'] == 0 ? $order['delivery_type'] : $order['delivery_type'] .': '.$order['delivery_price'] }}</p>
							<h3 class="info-list__title">płatność</h3>
							<p class="info-list__info-line">{{ $order['payment_type'] }}</p>
							<div class="efect-form__buttons">
								<button type="submit" class="btn">kupuję</button>
								<a href="{{ route('order') }}">wróć do edytowania</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

@endsection