@extends('pages.information.information-layout')

@section('information-page')

	<div class="s-other-pages__right-part">
		<div class="s-other-pages__content">
			<h1>polityka prywatności</h1>
			<ol>
				<li>Niniejsza Polityka Prywatności określa zasady przetwarzania danych osobowych pozyskanych
					za pośrednictwem sklepu internetowego www.mystiqbutik.pl (zwanego dalej: „Sklepem
					Internetowym”).
				</li>
				<li>Właścicielem Sklepu Internetowego i jednocześnie administratorem danych jest Anna
					Wendolowska prowadząca działalność gospodarczą pod firmą ANNA WENDOŁOWSKA MYSTiQ -
					WENDOŁOWSKA z siedzibą w Otwocku (05-400), ul. Karczewska 14/16 paw.10, wpisana do
					Centralnej Ewidencji i Informacji o Działalności Gospodarczej prowadzonej przez Ministra
					Rozwoju, NIP: 1250965070, REGON: 015242645, zwana dalej eFECT.
				</li>
				<li>eFECT dokłada szczególnej staranności do poszanowania prywatności Klientów
					odwiedzających Sklep Internetowy.
				</li>
			</ol>
			<h2>&#167; 1 Jak zbieramy dane?</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur beatae cupiditate
				eaque et, fugiat fugit in ipsa ipsam molestiae officia porro provident, quas recusandae
				repellat reprehenderit similique soluta, voluptatem?</p>
			<ol>
				<li>eFECT zbiera informacje dotyczące osób fizycznych dokonujących czynności prawnej
					niezwiązanej bezpośrednio z ich działalnością, osób fizycznych prowadzących we własnym
					imieniu działalność gospodarczą lub zawodową oraz osób fizycznych reprezentujących osoby
					prawne lub jednostki organizacyjne niebędące osobami prawnymi, którym ustawa przyznaje
					zdolność prawną, prowadzące we własnym imieniu działalność gospodarczą lub zawodową,
					zwanych dalej łącznie Klientami.
				</li>
				<li>Dane osobowe Klientów są zbierane w przypadku:
					<ol>
						<li>rejestracji konta w Sklepie Internetowym;</li>
						<li>składania zamówienia w Sklepie Internetowym;</li>
						<li>subskrypcji biuletynu informacyjnego (Newsletter).</li>
					</ol>
				</li>
				<li>
					Podczas rejestracji konta w Sklepie Internetowym Klient samodzielnie ustala indywidualne
					hasło dostępu do swojego konta. Klient może zmienić hasło, w późniejszym czasie, na
					zasadach opisanych w &#167;4.
				</li>
				<li>
					W przypadku składania zamówienia w Sklepie Internetowym, Klient podaje następujące dane:
					<ol>
						<li>adres e-mail;</li>
						<li>
							dane adresowe:
							<ol>
								<li>kod pocztowy i miejscowość;</li>
								<li>kraj (państwo);</li>
								<li>ulica wraz z numerem domu/mieszkania.</li>
							</ol>
						</li>
						<li>imię i nazwisko;</li>
						<li>numer telefonu.</li>
					</ol>
				</li>
				<li>
					W przypadku Przedsiębiorców, powyższy zakres danych jest dodatkowo poszerzony o:
					<ol>
						<li>firmę Przedsiębiorcy;</li>
						<li>numer NIP.</li>
					</ol>
				</li>
			</ol>
		</div>
	</div>

@endsection