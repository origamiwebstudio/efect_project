@extends('pages.information.information-layout')

@section('information-page')

	<div class="s-other-pages__right-part">
		<div class="s-other-pages__content">
			<h1>reklamacje i zwroty</h1>
			<h2>1 Reklamacje</h2>
			<p>Wszelkie reklamacje należy składać poprzez przesłanie wypełnionego formularza pocztą elektroniczną na adres MYSTIQSKLEP@VP.PL lub przesłać na adres:</p>
			<address>
				M&H Fashion s. c. <br>
				Reymonta 19a/2 <br>
				05-091 Ząbki <br>
				<a href="#">Formularz reklamacyjny</a>
			</address>
			<p>Uwaga:</p>
			<ol>
				<li>Nie przyjmujemy paczek wysłanych za pobraniem lub paczkomatami.</li>
				<li>Koszt odesłania towaru ponosi Kupujący.</li>
				<li>Każdy zwracany produkt powinien być odpowiednio zapakowany oraz zabezpieczony.</li>
			</ol>
		</div>
	</div>

@endsection