@extends('layouts.main')

@section('page')

	<div class="container">

		<div class="breadcrumbs">
			<ul>
				<li>
					<a href="{{ route('home') }}">strona główna</a>
				</li>
				<li>
					<span>
						@if(request()->route()->getName() === 'privacy-policy') polityka prywatności
						@elseif(request()->route()->getName() === 'regimen') regulamin
						@elseif(request()->route()->getName() === 'delivery-payment') dostawa i płatności
						@elseif(request()->route()->getName() === 'complaints-returns') reklamacje i zwroty
						@endif
					</span>
				</li>
			</ul>
		</div>

		<section class="s-other-pages">
			<div class="row">

				<div class="col-sm-3">
					<aside>
						<nav>
							<ul>
								<li>
									<a href="{{ route('privacy-policy') }}"
										 class="{{ request()->route()->getName() === 'privacy-policy' ? 'active' : '' }}">polityka prywatności</a>
								</li>
								<li>
									<a href="{{ route('regimen') }}"
										 class="{{ request()->route()->getName() === 'regimen' ? 'active' : '' }}">regulamin</a>
								</li>
								<li>
									<a href="{{ route('delivery-payment') }}"
										 class="{{ request()->route()->getName() === 'delivery-payment' ? 'active' : '' }}">dostawa i płatności</a>
								</li>
								<li>
									<a href="{{ route('complaints-returns') }}"
										 class="{{ request()->route()->getName() === 'complaints-returns' ? 'active' : '' }}">reklamacje i zwroty</a>
								</li>
							</ul>
						</nav>
					</aside>
				</div>

				<div class="col-sm-9">
					@yield('information-page')
				</div>

			</div>
		</section>

	</div>

@endsection