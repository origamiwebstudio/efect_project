@extends('layouts.main')

@section('page')

	<div class="container">

		<div class="breadcrumbs">

			<ul>
				<li>
					<a href="{{ route('home') }}">strona główna</a>
				</li>
				<li>
					<a href="{{ route('products') }}">kolekcja</a>
				</li>
				@if($product && $product->category)
					<li>
						<a href="{{ route('products', ['filter' => strtolower($product->category->name)]) }}">{{ $product->category->name }}</a>
					</li>
				@endif
				<li>
					<span>{{ $product->name }}</span>
				</li>
			</ul>

			<a href="{{ route('products') }}" class="back-btn">wróć do kolekcji</a>
		</div>

		<section class="product-content" id="productsingle">
			<div class="product-content__left-part">
				@if($product->images)
					<div class="product-content__images">
						<div class="special-marks">
							@if(!$product->visible || !$product->availability)
								<p class="special-marks__info">brak</p><br>
							@elseif($product->novelty)
								<p class="special-marks__info">nowość</p><br>
							@endif
							@if(!is_null($product->offer_price))
								@if($product->offer_type == 'fix_price')
									<span class="special-marks__offer">SUPER CENA</span>
								@else
									@if($product->offer_type == 'fix_discount')
										<span class="special-marks__offer special-marks__offer_discount">{{ $product->offer_value }} zł</span>
									@else
										<span class="special-marks__offer special-marks__offer_discount">{{ $product->offer_value }}%</span>
									@endif
								@endif
							@endif
						</div>
						<div v-cloak class="product-content__lightbox-loader">
							<lightbox id="product-images" :images="[ @foreach($product->images as $image){src : '{{ asset($image->link) }}'}, @endforeach ]" :options="options"></lightbox>
						</div>
					</div>
				@endif
			</div>

			<div class="product-content__right-part">
				<div class="product-order {{ $product->availability && $product->visible ? '' : 'product-order_disabled' }}" :class="{'product-order_loading' : sending}">

					<header>
						<h1>{{ $product->name }}</h1>

						<p>#{{ $product->uid }}</p>

						<span class="price price_new">{{ $product->offer_price ? $product->offer_price : $product->price }} PLN</span>
						@if($product->offer_price)
							<span class="price price_old">{{ $product->price }} PLN</span>
						@endif
					</header>

					<form action="{{ route('add-product-to-basket') }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="product_id" value="{{ $product->id }}">

						<div class="product-order__radiobutton-part">
							@if(isset($product->colors) && count($product->colors) > 0)
								<div class="product-order__form-group">
									<h3>kolor</h3>
									<div class="product-order__color-radiobuttons">
										@php $checkedColor = ""; @endphp
										@foreach($product->colors as $color)
											<label class="color-radiobutton {{ $product->availability && $product->visible ? '' : 'color-radiobutton_disabled' }}">
												<input v-model="productData.color" type="radio" name="color" value="{{$color->hex}}" {{ $product->availability && $loop->first ? 'checked' : '' }} {{ $product->availability && $product->visible ? '' : 'disabled' }}>
												<span><i style="background-color: {{$color->hex}}"></i></span>
											</label>
											@php if ($product->availability && $product->visible && $loop->first) $checkedColor = $color->hex; @endphp
										@endforeach
									</div>
								</div>
							@endif

							<div class="product-order__form-group">
								<h3>rozmiar</h3>
								<div class="product-order__size-radiobuttons">
									@php $switcher = true; $checkedSize = 0; @endphp
									@foreach(config('efect.sizes') as $size)
										<label class="size-radiobutton {{  !$product->visible || $product['s'.$size] == 0 ? 'size-radiobutton_absent' : '' }}">
											<input v-model="productData.size"
														 type="radio"
														 name="size"
														 value="{{ $size }}" {{ !$product->visible || $product['s'.$size] == 0 ? 'disabled' : '' }}
													{{  $product->availability && $switcher ? 'checked' : '' }}>
											<span>{{ $size }}</span>
										</label>
										@php
											if ($product['s'.$size] > 0 && $product->availability && $product->visible && $switcher) $checkedSize = $size;
											if ($product['s'.$size] > 0) $switcher = false;
										@endphp
									@endforeach
								</div>

								<span class="product-order__size-table" @click="showSizeTable">tabela rozmiarowa</span>
								<size-table ref="sizeTable"></size-table>

							</div>

						</div>

						@if($product->availability)
							<div class="product-order__form-group">
								<label for="quantity">ilość produktów</label>
								<select v-model="productData.quantity" class="select product-order__quantity-select" name="quantity" id="quantity">
									@for($i = 1; $i <= 5; $i++)
										<option value="{{ $i }}">{{ $i }}</option>
									@endfor
								</select>
							</div>
						@endif

						<button class="btn {{ $product->availability && $product->visible ? '' : 'btn_disabled' }}" :class="{ 'btn_disabled' : sending }" :disabled="sending ? true : false" type="submit" {{ $product->availability && $product->visible ? '' : 'disabled' }} @click.prevent="addToBasket">dodaj do koszyka</button>
						<br class="product-order__br">
						<span class="product-order__favorite" :class="{ 'product-order__favorite_chosen' : isFavorite, 'product-order__favorite_sending': favoriteSending}" @click="addToFavorite">dodaj do ulubionych</span>
					</form>
					<added-to-basket ref="addedMessage"></added-to-basket>
				</div>
			</div>
		</section>

		<script>
          window.productData = {
            id: {{ $product->id }},
			@if($product->is_favorite) favorite: true, @endif
			checkedSize: {{ $checkedSize }},
			checkedColor: "{{ $checkedColor }}",
			@if(auth()->user()) authorized: true @endif
          }
		</script>

		@if(isset($viewed_products) && count($viewed_products) > 0)
			<section class="last-seen-products">

				<h2>ostatnio oglądane produkty</h2>

				<ul class="row" id="lastseenproducts">
					@foreach($viewed_products as $viewed_product)
						@continue($viewed_product->id === $product->id)
						<li>
							<div class="product {{ $viewed_product->availability ? '' : 'product_absent' }}">
								<div class="product__image-wrapper">
									<img v-lazy="'{{ asset($viewed_product->main_image->link) }}'" alt="{{ $viewed_product->name }}">
									<add-to-basket-modal
											id="{{ $viewed_product->id }}"
											:name="'{{ $viewed_product->name }}'"
											:uid="'{{ $viewed_product->uid }}'"
											:link="'{{ route('product', ['id' => $viewed_product->id]) }}'"
											:image="'{{ asset($viewed_product->main_image->link) }}'"
											:price="'{{ $viewed_product->price }}'"
											@if($viewed_product->is_favorite):favorite="true" @endif
											@if(auth()->user()) :authorized="true" @endif
											@if($viewed_product->offer_price):offer_price="'{{ $viewed_product->offer_price }}'" @endif
											:colors="{{ $viewed_product->colors->pluck('hex') }}"
											:sizes="[{{ $viewed_product->getAvailableSizes('string') }}]"
									></add-to-basket-modal>

									<div class="special-marks">
										@if(!$viewed_product->availability)
											<p class="special-marks__info">brak</p><br>
										@elseif($viewed_product->novelty)
											<p class="special-marks__info">nowość</p><br>
										@endif
										@if(!is_null($viewed_product->offer_price))
											@if($viewed_product->offer_type == 'fix_price')
												<span class="special-marks__offer">SUPER CENA</span>
											@else
												@if($viewed_product->offer_type == 'fix_discount')
													<span class="special-marks__offer special-marks__offer_discount">{{ $viewed_product->offer_value }} zł</span>
												@else
													<span class="special-marks__offer special-marks__offer_discount">{{ $viewed_product->offer_value }}%</span>
												@endif
											@endif
										@endif
									</div>
								</div>
								<div class="product__description">
									<a href="{{ route('product', ['id' => $viewed_product->id]) }}">{{ $viewed_product->name }}</a>
									@if(is_null($viewed_product->offer_price))
										<span class="price">{{ $viewed_product->price }} PLN</span>
									@else
										<span class="price price_new">{{ $viewed_product->offer_price }} PLN</span>
										<span class="price price_old">{{ $viewed_product->price }} PLN</span>
									@endif
								</div>
							</div>
						</li>
					@endforeach
				</ul>
			</section>
		@endif

	</div>

@endsection