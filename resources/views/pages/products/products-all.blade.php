@extends('layouts.main')

@section('page')

    <script>
      window.sliderProps = {
        min: 0,
        max: {{ round(\App\Product::productsMaxPrice()) }},
        spec_filter: "{{ isset(request()->route()->parameters()['filter']) ? \App\Product::getSpecialFilter(request()->route()->parameters()['filter']) : null }}"
      }
    </script>

    <div id="allproducts">

        <header class="page-header">
            <div class="container">
                <h1>
                    kolekcja
                    @if($products->total() > 0)
                        <span v-if="!showTakenProducts">({{ $products->total() }})</span>
                        <span v-cloak v-else-if="showTakenProducts && products.length">(@{{ pagination.total }})</span>
                    @endif
                </h1>

                <div is="dropdown" class="dropdown" v-cloak>
                    <span class="dropdown__opener" slot="text">@{{ currentOrder }} <i class="caret"></i></span>
                    <li>
                        <button @click="setOrder('newest')">najnowsze</button>
                    </li>
                    <li>
                        <button @click="setOrder('oldest')">najstarsze</button>
                    </li>
                    <li>
                        <button @click="setOrder('price_up')">cena rosnąnco</button>
                    </li>
                    <li>
                        <button @click="setOrder('price_down')">cena malejąco</button>
                    </li>
                </div>

            </div>

        </header>

        <section class="s-all-products">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <button class="s-all-products__filter-on-btn" @click.prevent="filtersOn = !filtersOn">filtruj +</button>
                        <div class="filters" v-cloak :class="{'filters_sending': sending, 'filters_open': filtersOn}">
                            <header>
                                <button @click="clearFilters">usuń filtry <span class="close-btn"></span></button>
                                <button class="back-btn" @click.prevent="filtersOn = false">wróć</button>
                            </header>

                            @if(isset($categories) && count($categories) > 0)
                                <div class="filters__item">
                                    <h3 @click="categoryFiltersShow = !categoryFiltersShow" :class="{'filters__title_open': categoryFiltersShow}">klasyfikacja</h3>
                                    <div class="filters__item-content" :class="{'filters__item-content_open' : categoryFiltersShow}">
                                        <label class="checkbox">
                                            <input type="checkbox" name="classification" value="all"
                                                   v-model="allcategories"
                                                   @change="setAllCategories" {{ isset($current_category_id) ? '' : 'checked' }}>
                                            <span class="checkbox__marker"></span>
                                            wszystko
                                        </label>
                                        @foreach($categories as $category)
                                            <label class="checkbox">
                                                <input type="checkbox" name="classification"
                                                       v-model="filterData.categories" value="{{ $category->id }}"
                                                        {{ isset($current_category_id) && $current_category_id === $category->id ? 'checked' : '' }}>
                                                <span class="checkbox__marker"></span>
                                                {{ $category->name }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            @endif

                            <div class="filters__item">
                                <h3 @click="sizeFiltersShow = !sizeFiltersShow" :class="{'filters__title_open': sizeFiltersShow}">rozmiar</h3>
                                <div class="filters__item-content" :class="{'filters__item-content_open' : sizeFiltersShow}">
                                    <label class="checkbox">
                                        <input type="checkbox" name="size" value="all" checked v-model="allsizes"
                                               @change="setAllSizes">
                                        <span class="checkbox__marker"></span>
                                        wszystkie rozmiary
                                    </label>
                                    <div class="filters__size-checkboxes">
                                        @foreach(config('efect.sizes') as $size)
                                            <label class="checkbox">
                                                <input type="checkbox" v-model="filterData.sizes" name="size"
                                                       value="{{ $size }}">
                                                <span class="checkbox__marker"></span>
                                                {{ $size }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                            </div>

                            <div class="filters__item filters__item_price" v-cloak>
                                <h3 @click="priceFiltersShow = !priceFiltersShow" :class="{'filters__title_open': priceFiltersShow}">cena</h3>
                                <div class="filters__item-content" :class="{'filters__item-content_open' : priceFiltersShow}">
                                    <vue-slider
                                            v-model="filterData.prices"
                                            :min='min'
                                            :max='max'
                                            :height="2"
                                            :interval="{{ round(\App\Product::productsMaxPrice() / 10, 1) }}"
                                            :dot-size="10"
                                            :tooltip="false"
                                            :lazy="true"
                                            :slider-style="{
                                                'background-color': '#151515',
                                                'box-shadow': 'none'
                                            }"
                                            :process-style="{
                                                'background-color': '#151515'
                                            }"
                                            :style="{
                                                'margin-bottom': '10px'
                                            }"
                                    ></vue-slider>
                                    <span style="float: left">@{{ filterData.prices[0] }} PLN</span>
                                    <span style="float: right">@{{ filterData.prices[1] }} PLN</span>
                                </div>
                            </div>
                            <footer>
                                <button class="btn" @click.prevent="filtersOn = false">zobać wyniki</button>
                            </footer>
                        </div>
                    </div>

                    <div class="col-sm-8 col-md-offset-1">
                        <div class="all-products-wrapper" :class="{'all-products-wrapper_loading': sending}">
                            <ul class="all-products" v-if="!showTakenProducts">
                                @if($products && count($products) > 0)
                                    @foreach($products as $product)
                                        <li class="product {{ $product->availability ? '' : 'product_absent' }}">

                                            <div class="product__image-wrapper">

                                                <img v-lazy="'{{ asset($product->main_image->link) }}'"
                                                     alt="{{ $product->name }}">

                                                <add-to-basket-modal
                                                        :id="{{ $product->id }}"
                                                        :name="'{{ $product->name }}'"
                                                        :uid="'{{ $product->uid }}'"
                                                        :link="'{{ route('product', ['id' => $product->id]) }}'"
                                                        :image="'{{ asset($product->main_image->link) }}'"
                                                        :price="'{{ $product->price }}'"
                                                        @if($product->is_favorite):favorite="true" @endif
                                                        @if(auth()->user()) :authorized="true" @endif
                                                        @if($product->offer_price):offer_price="'{{ $product->offer_price }}'"
                                                        @endif
                                                        :colors="{{ $product->colors->pluck('hex') }}"
                                                        :sizes="[{{ $product->getAvailableSizes('string') }}]"
                                                ></add-to-basket-modal>

                                                <div class="special-marks">
                                                    @if(!$product->availability)
                                                        <p class="special-marks__info">brak</p><br>
                                                    @elseif($product->novelty)
                                                        <p class="special-marks__info">nowość</p><br>
                                                    @endif
                                                    @if(!is_null($product->offer_price))
                                                        @if($product->offer_type == 'fix_price')
                                                            <span class="special-marks__offer">SUPER CENA</span>
                                                        @else
                                                            @if($product->offer_type == 'fix_discount')
                                                                <span class="special-marks__offer special-marks__offer_discount">{{ $product->offer_value }}
                                                                    zł</span>
                                                            @else
                                                                <span class="special-marks__offer special-marks__offer_discount">{{ $product->offer_value }}
                                                                    %</span>
                                                            @endif
                                                        @endif
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="product__description">
                                                <a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a>
                                                @if(is_null($product->offer_price))
                                                    <span class="price">{{ $product->price }} PLN</span>
                                                @else
                                                    <span class="price price_new">{{ $product->offer_price }} PLN</span>
                                                    <span class="price price_old">{{ $product->price }} PLN</span>
                                                @endif
                                            </div>

                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            <template v-if="showTakenProducts">
                                <ul class="all-products" v-if="products.length">
                                    <li class="product" v-cloak v-for="product in products" :key="product.id"
                                        :class="{'product_absent' : !product.availability}">
                                        <div class="product__image-wrapper">
                                            <img v-lazy="this.window.location.origin + '/' + product.main_image"
                                                 :alt="product.name">
                                            <add-to-basket-modal
                                                    is="add-to-basket-modal"
                                                    :id="product.id"
                                                    :name="product.name"
                                                    :uid="product.uid"
                                                    :link="this.window.location.origin + '/products/'+ product.id"
                                                    :image="this.window.location.origin + '/' + product.main_image"
                                                    :price="product.price"
                                                    :favorite="product.favorite"
                                                    @if(auth()->user()) :authorized="true" @endif
                                                    :offer_price="(product.offer_price === null) ? '' : product.offer_price"
                                                    :colors="product.colors"
                                                    :sizes="product.sizes"
                                            ></add-to-basket-modal>
                                            <div class="special-marks">
                                                <p class="special-marks__info"
                                                   v-if="!product.availability || product.new === 1">
                                                    <template v-if="product.availability && product.new === 1">
                                                        nowość
                                                    </template>
                                                    <template v-else>
                                                        brak
                                                    </template>
                                                </p>
                                                <br>
                                                <span class="special-marks__offer"
                                                      :class="{'special-marks__offer_discount': (product.offer_type !== 'fix_price')}"
                                                      v-if="product.offer_value !== null">
                                                <template v-if="product.offer_type !== 'fix_price'">@{{ product.offer_value }}</template><template
                                                            v-else>SUPER CENA</template><template
                                                            v-if="product.offer_type === 'fix_discount'"> zł</template><template
                                                            v-else-if="product.offer_type === 'percent'">%</template>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="product__description">
                                            <a :href="this.window.location.origin + '/products/'+ product.id">@{{
                                                product.name }}</a>
                                            <template v-if="product.offer_price === null">
                                                <span class="price">@{{ product.price }} PLN</span>
                                            </template>
                                            <template v-else>
                                                <span class="price price_new">@{{ product.offer_price }} PLN</span>
                                                <span class="price price_old">@{{ product.price }} PLN</span>
                                            </template>
                                        </div>
                                    </li>
                                </ul>
                                <p class="s-all-products__no-products" v-else>Nie znależono żadnych produktów</p>
                            </template>

                            <template v-if="!showTakenProducts">
                                @if($products->lastPage() > 1)
                                    <paginate
                                            :page-count="{{ $products->lastPage() }}"
                                            :page-range="1"
                                            :click-handler="paginate"
                                            :prev-class="'pagination__prev'"
                                            :next-class="'pagination__next'"
                                            :prev-text="''"
                                            :next-text="''"
                                            :container-class="'pagination'">
                                    </paginate>
                                @endif
                            </template>
                            <template v-else>
                                <paginate v-if="pagination.page_last > 1"
                                          :page-count="pagination.page_last"
                                          :page-range="1"
                                          :click-handler="paginate"
                                          :force-page="forcePaginatePage"
                                          :prev-class="'pagination__prev'"
                                          :next-class="'pagination__next'"
                                          :prev-text="''"
                                          :next-text="''"
                                          :container-class="'pagination'">
                                </paginate>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection