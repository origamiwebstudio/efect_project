<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Efect Market</title>
</head>
<body style="font-family: FuturaPT, sans-serif; font-size: 16px; font-weight: 500; margin: 0; padding: 0; color: #151515;">
<link href="{{ asset('css/fonts.css') }}" rel="stylesheet">
<table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%;">
	<tbody>
	<tr>
		<td>
			<table bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0"
						 style="margin:0; padding-left: 0; padding-right: 0; width: 100%; padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #ececec;">
				<tbody>
				<tr>
					<td>
						<a style="color: #151515; font: 16px FuturaPT, sans-serif; line-height: 30px; -webkit-text-size-adjust:none;"
							 href="{{ asset('/') }}" target="_blank">
							<img src="{{ asset('images/logotype.png') }}" alt="efect logo" border="0" width="235"
									 style="max-width: 100%; margin: auto; display: block;">
						</a>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0"
						 style="margin: 60px auto 93px; padding:0 15px; width: 100%; max-width: 600px;">
				<tbody>
				<tr>
					<td>
						<span style="color: #151515; font: 600 36px FuturaPT, sans-serif; line-height: 2.5; -webkit-text-size-adjust:none; display: block;">Witam!</span>
						<span style="color: #151515; font: 500 16px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 30px;">Napisał do nas użytkownik {{ $request['name'] }} przez formę kontaktową:<br>
							"{{ $request['message'] }}"
							<br><br>
							Email: <a href="mailto:{{ $request['email'] }}">{{ $request['email'] }}</a><br>
							@if($request['phone'])
								Telefon: {{ $request['phone'] }}
							@endif
						</span>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0"
						 style="margin:0; padding-left: 0; padding-right: 0; width: 100%; padding-top: 25px; padding-bottom: 20px; border-top: 1px solid #ececec;">
				<tbody>
				<tr>
					<td>
						<span
								style="max-width: 600px; width: 100%; margin: 0 auto 10px; color: #151515; font-size: 0; line-height: 1; -webkit-text-size-adjust:none; display: block; text-align: center;">
							<a href="#" target="_blank"
								 style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block;">Polityka prywatności</a>
							<a href="#" target="_blank"
								 style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block;">Dostawa i płatność</a>
							<a href="#" target="_blank"
								 style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block;">Reklamacje i zwroty</a>
						</span>
						<span
								style="max-width: 600px; width: 100%; margin: auto; color: #151515; font-size: 0; line-height: 1; -webkit-text-size-adjust:none; display: block; text-align: center;">
						 <a href="#" target="_blank"
								style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block; vertical-align: middle;">
								 <img src="{{ asset('images/facebook-logo.png') }}" alt="facebook logo" width="18">
						 </a>
						 <a href="#" target="_blank"
								style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block; vertical-align: middle;">
								 <img src="{{ asset('images/instagram-logo.png') }}" alt="instagram logo" width="12">
						 </a>
						</span>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
</body>
</html>