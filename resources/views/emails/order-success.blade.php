<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Efect Market</title>
</head>
<body style="font-family: FuturaPT, sans-serif; font-size: 16px; font-weight: 500; margin: 0; padding: 0; color: #151515;">
<link href="{{ asset('css/fonts.css') }}" rel="stylesheet">
<table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%;">
    <tbody>
    <tr>
        <td>
            <table bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0"
                   style="margin:0; padding-left: 0; padding-right: 0; width: 100%; padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #ececec;">
                <tbody>
                <tr>
                    <td>
                        <a style="color: #151515; font: 16px FuturaPT, sans-serif; line-height: 30px; -webkit-text-size-adjust:none;"
                           href="{{ asset('/') }}" target="_blank">
                            <img src="{{ asset('images/logotype.png') }}" alt="efect logo" border="0" width="235"
                                 style="max-width: 100%; margin: auto; display: block;">
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0"
                   style="margin: 60px auto 93px; padding:0 15px; width: 100%; max-width: 600px;">
                <tbody>
                <tr>
                    <td>
                        <span style="color: #151515; font: 600 36px FuturaPT, sans-serif; line-height: 2.5; -webkit-text-size-adjust:none; display: block;">hej, {{ $order->name }}</span>
                        <span style="color: #151515; font: 500 16px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 30px;">Kupiłeś, opłaciłeś, zrobiłeś zamówienie i tak dalej, coś tutaj jeszcze. Jakikowiek teks.</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #151515; font: 600 24px FuturaPT, sans-serif; line-height: 1.3; -webkit-text-size-adjust:none; display: block;">zamówienie #{{ $order->order_number }}</span>
                        <span style="color: #c8c8c8; font: 500 16px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 11px;">Ilość produktów: {{ $order->cart->products->count() }}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%;">
                            <tbody>
                            @foreach($order->cart->products as $product)
                                <tr>
                                <td style="padding-bottom: 30px; border-bottom: 1px solid #ececec;">
                                    <table border="0" cellpadding="0" cellspacing="0"
                                           style="margin:0; padding:0; width: 100%;">
                                        <tbody>
                                        <tr>
                                            <td width="170">
                                                <a href="{{ route('product', ['id' => $product->id]) }}" target="_blank">
                                                    <img border="0" width="170"
                                                         src="{{ asset($product->main_image->link) }}"
                                                         alt="{{ $product->name }}"
                                                         style="max-width: 100%; display: block; min-width: 100px;">
                                                </a>
                                            </td>
                                            <td width="190" style="padding-left: 30px; padding-right: 20px;">
                                                <table border="0" cellpadding="0" cellspacing="0"
                                                       style="margin:0; padding:0; width: 100%; border-bottom: 1px solid #ececec;">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <a href="{{ route('product', ['id' => $product->id]) }}" target="_blank" style="color: #151515; font: 500 24px FuturaPT, sans-serif; line-height: 1.4; -webkit-text-size-adjust:none; display: block;">{{ $product->name }}</a>
                                                            <span style="color: #c8c8c8; font: 500 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 5px;">#{{ $product->uid }}</span>
                                                            <span style="color: #151515; font: 500 18px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 12px;">{{ $product->pivot->actual_price }} PLN</span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table border="0" cellpadding="0" cellspacing="0"
                                                       style="margin:0; padding:0; text-align: center; padding-top: 13px;">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" style="padding-right: 23px;">
                                                            <span style="color: #151515; font: 500 14px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 6px;">rozmiar</span>
                                                            <span style="color: #ffffff; font: 500 11px FuturaPT, sans-serif; line-height: 30px; -webkit-text-size-adjust:none; display: block; width: 30px; height: 30px; background-color: #ff9343; border-radius: 50%;">{{ $product->pivot->size }}</span>
                                                        </td>
                                                        <td valign="top">
                                                            <span style="color: #151515; font: 500 14px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-bottom: 6px;">kolor</span>
                                                            <span style="color: #ffffff; font: 500 11px FuturaPT, sans-serif; line-height: 30px; -webkit-text-size-adjust:none; display: block; width: 30px; height: 30px; background-color: {{ $product->pivot->color }}; border-radius: 50%;"></span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="padding-right: 20px;">
                                                <span style="color: #151515; font: 500 11px FuturaPT, sans-serif; line-height: 40px; -webkit-text-size-adjust:none; display: block; width: 66px; border: 1px solid #ebebeb; text-align: center;">{{ $product->pivot->quantity }}</span>
                                            </td>
                                            <td style="text-align: right;">
                                                <span style="color: #151515; font: 500 18px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block;">{{ $product->pivot->summary }} PLN</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #151515; font: 500 16px FuturaPT, sans-serif; text-align: right; line-height: 1.6; -webkit-text-size-adjust:none; display: block; margin-top: 18px; margin-bottom: 35px;">wartość zamówienia: <span
                                    style="color: #151515; font: 600 20px FuturaPT, sans-serif; text-align: right; line-height: 1.6; -webkit-text-size-adjust:none; ">{{ $order->cart->products_summary_price }} PLN</span></span>
                        <span style="color: #151515; font: 500 16px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; display: block;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0"
                   style="margin:0; padding-left: 0; padding-right: 0; width: 100%; padding-top: 25px; padding-bottom: 20px; border-top: 1px solid #ececec;">
                <tbody>
                <tr>
                    <td>
                        <span style="max-width: 600px; width: 100%; margin: 0 auto 10px; color: #151515; font-size: 0; line-height: 1; -webkit-text-size-adjust:none; display: block; text-align: center;">
                            <a href="{{ route('privacy-policy') }}" target="_blank"
                               style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block;">Polityka prywatności</a>
                            <a href="{{ route('delivery-payment') }}" target="_blank"
                               style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block;">Dostawa i płatność</a>
                            <a href="{{ route('complaints-returns') }}" target="_blank"
                               style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block;">Reklamacje i zwroty</a>
                        </span>
                        <span style="max-width: 600px; width: 100%; margin: auto; color: #151515; font-size: 0; line-height: 1; -webkit-text-size-adjust:none; display: block; text-align: center;">
                               <a href="#" target="_blank"
                                  style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block; vertical-align: middle;">
                                   <img src="{{ asset('images/facebook-logo.png') }}" alt="facebook logo" width="18">
                               </a>
                               <a href="#" target="_blank"
                                   style="color: #151515; font: 600 12px FuturaPT, sans-serif; line-height: 1.6; -webkit-text-size-adjust:none; text-transform: uppercase; padding: 0 10px; text-decoration: none; display: inline-block; vertical-align: middle;">
                                   <img src="{{ asset('images/instagram-logo.png') }}" alt="instagram logo" width="12">
                               </a>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>