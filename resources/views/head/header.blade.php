<div id="headerapp">
	<header class="main-header" :class="{ menuOpen: navMenu, searchOpen: search }">
		<div class="main-header__wrapper">
			<div class="container">
				<div class="main-header__left-side">
					<div class="burger" :class="{ open: navMenu}" @click="navMenu = !navMenu">
						<span class="burger__menu"></span>
					</div>
					<div class="search" @click="searchOn">
						<span class="search__button"></span>
					</div>
				</div>
				<a href="{{ route('home') }}">
					<img src="{{ asset('images/logotype.svg') }}" alt="efect logo" class="main-header__logo">
				</a>
				<div class="main-header__right-side">
					@if(auth()->guest())
						<a href="{{ route('login') }}" class="main-header__login">logowanie</a>
					@elseif(auth()->user())
						<span class="main-header__greet">hej, </span>
						<div is="dropdown" class="dropdown">
							<span class="dropdown__opener" slot="text">{{ auth()->user()->name }} <i class="caret"></i></span>
							<template v-cloak>
								<li>
									<a href="{{ route('profile') }}">moje konto</a>
								</li>
								<li>
									<a href="{{ route('profile-orders') }}">moje zamówienia</a>
								</li>
								<li>
									<form action="{{ url('/logout') }}" method="POST">
										{{ csrf_field() }}
										<button>wyloguj się</button>
									</form>
								</li>
								@if(auth()->user()->hasRole(['developer', 'owner', 'manager']))
									<li>
										<hr>
										<a href="{{ url('admin') }}" target="_blank">panel administracyjny
											({{ \App\Order::getCountAllNewOrders() }})</a>
									</li>
								@endif
							</template>
						</div>
					@endif
					<a href="{{ route('profile-favorites') }}" class="main-header__favorites"><span>ulubione</span></a>
					<a href="{{ route('profile-basket') }}" class="main-header__basket"
						 :class="{ 'main-header__basket_loading' : basketData.loading }">
						<span class="counter" v-if="basketData.count" v-cloak>@{{ basketData.count }}</span>
					</a>
				</div>
			</div>
		</div>
		<transition name="main-header__navigation">
			<nav class="main-header__navigation" v-if="navMenu" v-cloak>
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<ul class="navigation">
								<li>
									<a href="{{ route('products', ['filter' => 'new']) }}">nowości</a>
								</li>
								<li>
									<div class="nested-links">
										<a href="{{ route('products') }}">kolekcja</a> <i class="caret"></i>
										<ul>
											@foreach(\App\ProductCategory::getAvailableCategories() as $category)
												<li>
													<a href="{{ route('products', ['filter' => strtolower($category->name)]) }}">{{ strtolower($category->name) }}</a>
												</li>
											@endforeach
										</ul>
									</div>
								</li>
								<li>
									<a href="{{ route('about') }}">o nas</a>
								</li>
								<li>
									<a href="{{ route('contacts') }}">kontakt</a>
								</li>
								<li>
									<a href="{{ route('regimen') }}">regulamin</a>
								</li>
							</ul>
						</div>
						@if(\App\Advantage::getAdvantages() && count(\App\Advantage::getAdvantages()) > 0)
							<div class="col-sm-7">
								<div class="info">
									@foreach(\App\Advantage::getAdvantages() as $key => $advantage)
										@if($key % 2 == 0)
											<div class="row">
												@endif
												<div class="col-md-6">
													<h3>{{ $advantage->name }}</h3>
													<p>{{ $advantage->text }}</p>
												</div>
												@if(($key + 1) % 2 == 0)
											</div>
										@endif
									@endforeach
								</div>
							</div>
						@endif
						<div class="social">
							<a href="#"><img src="{{ asset('images/facebook-icon.svg') }}" alt="facebook icon"></a>
							<a href="#"><img src="{{ asset('images/instagram-icon.svg') }}" alt="instagram icon"></a>
						</div>
					</div>
				</div>
			</nav>
		</transition>
		<transition name="search-content">
			<div class="search-content" v-cloak v-if="search">
				<div class="search-content__input">
					<div class="container">
						<input type="text" v-model="searchText" ref="searchInput" placeholder="Czego  będziemy szukać?">
						<button class="close-btn" @click="searchOn"></button>
					</div>
				</div>
				<div class="search-content__output" v-if="searchText.length > 1">
					<div class="container">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1">
								<p class="not-found" v-if="loading">Poszukiwanie...</p>
								<ul v-if="searchProducts.length">
									<li v-for="product in searchProducts" :key="product.id">
										<a :href="this.window.location.origin + '/products/'+ product.id">
											<figure>
												<div class="search-content__image-wrapper">
													<img :src="this.window.location.origin + '/'+ product.photo" :alt="product.name">
												</div>
												<figcaption>
													<h3>@{{ product.name }}</h3>
													<template v-if="!product.offer_price">
														<span class="price">@{{ product.price }} PLN</span>
													</template>
													<template v-else>
														<span class="price price_new">@{{ product.offer_price }} PLN</span>
														<span class="price price_old">@{{ product.price }} PLN</span>
													</template>
												</figcaption>
											</figure>
										</a>
									</li>
								</ul>
								<p class="not-found" v-if="!searchProducts.length && !loading">Nie znaleziono produktów odpowiadających kryterium wyszukiwania. Spróbuj
									ponownie!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</transition>
		<div class="overlay"></div>
	</header>
	<ul class="fixed-messages" v-cloak>
		<transition-group name="fixed-messages__item">
			<li class="fixed-messages__item" v-for="message in messagesList" :class="message.stateClass" :key="message.id">
				<h6 v-if="message.title">@{{ message.title }}</h6>
				<p>@{{ message.message }}</p>
			</li>
		</transition-group>
	</ul>
	{{--@if(!session()->has('isAcceptedCookies') && session('isAcceptedCookies') != true)--}}
		<div class="cookies-warn" v-if="!cookiesConfirmed" v-cloak>
			<div class="container">
				<p>Korzystając z naszej strony wyrażasz zgodę na wykorzystywanie przez nas plików cookies w celu zapewnienia Ci wygody podczas przeglądania strony.</p>
				<form action="{{ route('accept-cookies') }}" method="post">
					{{ csrf_field() }}
					<button class="btn" @click.prevent="cookiesAccept($event)">Zgadzam się</button>
				</form>
			</div>
		</div>
	{{--@endif--}}
</div>