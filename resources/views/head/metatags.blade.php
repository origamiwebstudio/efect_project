<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
<script>
	window.LaravelToken = "{{csrf_token()}}";
	window.basketData = {
      count: {{ \App\Cart::getProductsCount() }},
      loading: false
	};
	window.messages = [];
</script>