<!DOCTYPE html>
<html lang="en">

<head>
	@include('head.metatags')
	<title>{{ isset($title) ? $title : trans('static.internet_shop') }} - EFECT</title>
	@include('head.styles')
</head>

<body>
@include('head.header')

<main class="main">
	@yield('page')
</main>

@include('foot.footer')
@include('foot.scripts')
</body>
</html>