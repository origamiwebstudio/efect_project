<link rel="stylesheet" href="{{ asset('packages/sleepingowl/styles/order.css') }}">

<div class="row">
	<div class="col-sm-6 col-md-4 col-lg-3">
		<div class="delivery-info">
			<div class="title">Dostawa:</div>
			<div class="info__item">
				<strong>Państwo: </strong>
				<span>{{ $model->country }}</span>
			</div>
			<div class="info__item">
				<strong>Miejscowość: </strong>
				<span>{{ $model->locality }}</span>
			</div>
			<div class="info__item">
				<strong>Kod pocztowy: </strong>
				<span>{{ $model->postal_code }}</span>
			</div>
			<div class="info__item">
				<strong>Ulica: </strong>
				<span>{{ $model->street . '&nbsp;' . $model->house_number }}</span>
			</div>
			<div class="info__item map-link">
				<a href="https://maps.google.com/?q={{ $model->address }}" target="_blank">
					<i class="fa fa-map-marker"></i>
					<span>Pokaż na mapie</span>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4 col-lg-3">
		<div class="client-info">
			<div class="title">Klient:</div>
			<div class="info__item">
				<strong>Imie: </strong>
				<span>{{ $model->name }}</span>
			</div>
			<div class="info__item">
				<strong>Nazwisko: </strong>
				<span>{{ $model->surname }}</span>
			</div>
			<div class="info__item">
				<strong>Email: </strong>
				<a href="mailto:{{ $model->email }}">{{ $model->email }}</a>
			</div>
			<div class="info__item">
				<strong>Telefon: </strong>
				<span>{{ $model->phone }}</span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-4 col-lg-6">
		<div class="products-info">
			<div class="main-info">
				<div class="title">Informacja o zamówieniu:</div>
				<div class="info__item">
					<strong>Numer zamówienia: </strong>
					<span>#{{ $model->order_number }}</span>
				</div>
				<div class="info__item">
					<strong>Produktów: </strong>
					<span>{{ $model->cart->products_count }}</span>
				</div>
				<div class="info__item">
					<strong>Kwota zamówienia: </strong>
					<span>{{ $model->cart->products_summary_price }} zł</span>
				</div>
				<div class="info__item">
					<strong>Dostawa: </strong>
					<span>{{ $model->delivery_type }}</span>
					@if($model->delivery_price > 0)
						<span>({{ $model->delivery_price }} zł)</span>
					@endif
				</div>
				<div class="info__item">
					<strong>Status płatności: </strong>
					<span>"{{ $model->payment_status }}"</span>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>