<link rel="stylesheet" href="{{ asset('packages/sleepingowl/styles/order.css') }}">

<div class="row">
	<div class="col-md-12">
		<hr>
		<ul class="row products-list">
			@foreach($model->cart->products as $product)
				<li class="col-xs-6 col-md-4 col-lg-3 col-xlg-2 product">
					<div class="product__image">
						<a href="{{ route('product', ['id' => $product->id]) }}" target="_blank">
							<img src="{{ asset($product->main_image->link) }}" alt="{{ $product->name }}">
						</a>
					</div>
					<div class="product__name">
						<a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a>
					</div>
					<div class="product__size-color">
						<span class="color"><i style="background: {{ $product->pivot->color }};"></i></span>
						<div class="size">{{ $product->pivot->size }}</div>
					</div>
					<hr>
					<div class="product__price-quantity">
						<span class="quantity">{{ $product->pivot->quantity }} szt.</span>
						<span> &times; </span>
						<span class="price">{{ $product->pivot->actual_price }} zł.</span>
						<span> = </span>
						<span class="summary">{{ $product->pivot->summary }} zł.</span>
					</div>
					<hr>
				</li>
			@endforeach
		</ul>
	</div>
</div>