<footer class="main-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-push-3">
				<ul>
					<li>
						<a href="{{ route('privacy-policy') }}">polityka prywatności</a>
					</li>
					<li>
						<a href="{{ route('delivery-payment') }}">dostawa płatności</a>
					</li>
					<li>
						<a href="{{ route('complaints-returns') }}">reklamacje i zwroty</a>
					</li>
				</ul>
			</div>
			<div class="col-md-3 col-md-pull-6 col-xs-5">
				<p><span>efect</span> &copy; 2017</p>
			</div>
			<div class="col-md-3 col-xs-7">
				<div class="main-footer__right-side">
					<p>Realizaja: <a href="https://origami.team" target="_blank">origami.team</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>