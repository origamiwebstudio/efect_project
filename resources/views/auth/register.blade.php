@extends('layouts.main')

@section('page')
    <header class="auth-header">
        <h1>rejestracja</h1>
    </header>

    <section class="register-page" id="registerpage">
        <form @submit.prevent="registerUser" novalidate method="POST" action="{{ url('/register') }}"
              class="efect-form" :class="{'efect-form_loading': sending }">

            {{ csrf_field() }}

            <div class="register-page__inputs-group">
                <div class="efect-form__row">
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="name" id="name" placeholder="imię"
                             required="true" v-model="formData.name" :error="errors.name"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="surname" id="surname"
                             placeholder="nazwisko" required="true" v-model="formData.surname"
                             :error="errors.surname"></div>
                    </div>
                </div>
                <div class="efect-form__row">
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="email" name="email" id="email"
                             placeholder="email" required="true" v-model="formData.email" :error="errors.email"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="phone" id="phone"
                             placeholder="telefon" v-model="formData.phone" :error="errors.phone"></div>
                    </div>
                </div>
                <div class="efect-form__row">
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="password" name="password" id="password"
                             placeholder="haslo" required="true" v-model="formData.password" :error="errors.password"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="password" name="password_confirmation"
                             id="password_confirmation" placeholder="powtórz hasło" required="true"
                             v-model="formData.password_confirmation" :error="errors.password_confirmation"></div>
                    </div>
                </div>
            </div>

            <div class="register-page__additional">
                <label class="checkbox efect-form__checkbox">
                    <input type="checkbox" true-value="on" false-value="" name="add_address"
                           v-model="formData.add_address">
                    <span class="checkbox__marker"></span>
                    dodaj adres
                </label>

                <p><span class="efect-form__required-star">*</span> pola wymagane</p>
            </div>

            <div class="register-page__inputs-group register-page__inputs-group_second" v-cloak
                 v-if="formData.add_address === 'on'">

                <div class="efect-form__row">
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="country" id="country"
                             placeholder="kraj" required="true" v-model="formData.country" :error="errors.country"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="locality" id="locality"
                             placeholder="miejscowość" required="true" v-model="formData.locality"
                             :error="errors.locality"></div>
                    </div>
                </div>
                <div class="efect-form__row">
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="street" id="street"
                             placeholder="ulica" required="true" v-model="formData.street" :error="errors.street"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="postal_code" id="postal_code"
                             placeholder="kod posztowy" required="true" v-model="formData.postal_code"
                             :error="errors.postal_code"></div>
                    </div>
                </div>
                <div class="efect-form__row">
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="text" name="house_number" id="house_number"
                             placeholder="numer domu" required="true" v-model="formData.house_number"
                             :error="errors.house_number"></div>
                    </div>
                </div>
            </div>

            <div class="register-page__checkboxes-group">

                <div class="checkbox efect-form__checkbox">
                    <input type="checkbox" id="privacy_policy" name="privacy_policy" v-model="formData.privacy_policy">
                    <label class="checkbox__marker" for="privacy_policy"></label>
                    akceptuję <a href="#" target="_blank" class="efect-form__checkbox-link">politykę prywatności</a><span
                            class="efect-form__required-star">*</span>
                </div>
                <template v-if="errors.privacy_policy" v-cloak>
                    <span class="efect-form__error-message register-page__checkbox-error">@{{ errors.privacy_policy[0] }}</span>
                </template>
                <div class="checkbox efect-form__checkbox">
                    <input type="checkbox" id="regimen" name="regimen" v-model="formData.regimen">
                    <label class="checkbox__marker" for="regimen"></label>
                    akceptuję <a href="#" target="_blank" class="efect-form__checkbox-link">regulamin</a><span
                            class="efect-form__required-star">*</span>
                </div>
                <template v-if="errors.regimen" v-cloak>
                    <span class="efect-form__error-message register-page__checkbox-error">@{{ errors.regimen[0] }}</span>
                </template>

            </div>

            <div class="efect-form__form-group efect-form__form-group_captcha" ref="captcha">
                {!! $captcha->display() !!}
                <span class="efect-form__error-message" v-if="errors['g-recaptcha-response']" v-cloak>@{{ errors['g-recaptcha-response'][0] }}</span>
            </div>

            <div class="efect-form__buttons" v-cloak>
                <button type="submit" class="btn" :class="{ 'btn_disabled': (sending || uncheckedRegimen)}"
                        :disabled="(sending || uncheckedRegimen)">zarejestruj się
                </button>
                <a href="{{ url('/login') }}">wróć do logowania</a>
            </div>

        </form>
    </section>
    {!! $captcha->script()!!}

@endsection
