@extends('layouts.main')

@section('page')
	<header class="auth-header">
		<h1>logowanie</h1>
	</header>

	<section class="login-block">

		<div class="login-block__content">

			<div class="login-block__left-side">
				<div class="login-block__overlay">
					<div class="login-block__left-side-content">
						<h2>witamy</h2>
						<p>lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
					</div>
				</div>
			</div>

			<div class="login-block__right-side">

				<h2>zaloguj się</h2>

				<form method="POST" action="{{ url('/login') }}" class="efect-form" @submit.prevent="login" :class="{'efect-form_loading': sending }">
					{{ csrf_field() }}

					<div class="efect-form__form-group">
						<div is="efect-input" class="efect-form__input" type="email" name="email" id="email" placeholder="e-mail" oldvalue="{{ old('email') }}" v-model="loginData.email" :error="loginErrors.email"></div>
					</div>

					<div class="efect-form__form-group">
						<div is="efect-input" class="efect-form__input" type="password" name="password" id="password" placeholder="haslo" v-model="loginData.password" :error="loginErrors.password"></div>
					</div>

					<div class="login-block__forget-pass">
						<a href="{{ url('/password/reset') }}">nie pamiętasz hasła?</a>
					</div>

					<div class="login-block__buttons">
						<button type="submit" class="btn" :class="{ 'btn_disabled': sending}" :disabled="sending">zaloguj się</button>
						<a href="{{ url('/register') }}">zarejestruj się</a>
					</div>

				</form>
			</div>

		</div>

	</section>

	{{--<div class="container">--}}
		{{--<div class="row">--}}
			{{--<div class="col-md-8 col-md-offset-2">--}}
				{{--<div class="panel panel-default">--}}
					{{--<div class="panel-heading">Login</div>--}}
					{{--<div class="panel-body">--}}
						{{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">--}}
							{{--{{ csrf_field() }}--}}

							{{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
								{{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

								{{--<div class="col-md-6">--}}
									{{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

									{{--@if ($errors->has('email'))--}}
										{{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
									{{--@endif--}}
								{{--</div>--}}
							{{--</div>--}}

							{{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
								{{--<label for="password" class="col-md-4 control-label">Password</label>--}}

								{{--<div class="col-md-6">--}}
									{{--<input id="password" type="password" class="form-control" name="password" required>--}}

									{{--@if ($errors->has('password'))--}}
										{{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
									{{--@endif--}}
								{{--</div>--}}
							{{--</div>--}}

							{{--<div class="form-group">--}}
								{{--<div class="col-md-6 col-md-offset-4">--}}
									{{--<div class="checkbox">--}}
										{{--<label>--}}
											{{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me--}}
										{{--</label>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}

							{{--<div class="form-group">--}}
								{{--<div class="col-md-8 col-md-offset-4">--}}
									{{--<button type="submit" class="btn btn-primary">--}}
										{{--Login--}}
									{{--</button>--}}

									{{--<a class="btn btn-link" href="{{ url('/password/reset') }}">--}}
										{{--Forgot Your Password?--}}
									{{--</a>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</form>--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--</div>--}}
@endsection
