@extends('layouts.main')

<!-- Main Content -->
@section('page')

	<section class="login-block">
		<div class="login-block__content">
			<div class="login-block__left-side">
				<div class="login-block__overlay">
					<div class="login-block__left-side-content">
						@if(auth()->guest())
							<h2>nie pamiętasz hasła?</h2>
							<p>Wpisz adres mejlowy na który jesteś zarejestrowany i naciśnij przycisk</p>
						@else
							<h2>Chcesz zmienić hasło?</h2>
							<p>Jeżeli chcesz zmienić hasło - sprawdź czy na ten adres mejlowy jesteś zarejestrowany i naciśnij przycisk</p>
						@endif
					</div>
				</div>
			</div>

			<div class="login-block__right-side">

				<h2>nowe hasło</h2>

				<form method="POST" action="{{ url('/password/email') }}" class="efect-form" @submit.prevent="resetEmail" :class="{'efect-form_loading': sending }">
					{{ csrf_field() }}

					<p class="login-block__if-dont-remember-pass">Uważaj, że po wysłaniu mejla - link będzie aktywny w ciągu {{ config('auth.passwords.users.expire') }} minut</p>

					<div class="efect-form__form-group">
						<div is="efect-input"
								 class="efect-form__input"
								 type="email"
								 name="email"
								 v-model="resetEmailData.email"
								 id="email"
								 placeholder="e-mail"
								 oldvalue="{{ auth()->user() ? (old('email') ?: auth()->user()->email) : old('email') }}"
								 :error="resetEmailErrors.email"></div>
					</div>

					<div class="login-block__buttons login-block__buttons_reset">
						<button type="submit" class="btn" :class="{ 'btn_disabled': sending}" :disabled="sending">wyślij</button>
						@if(auth()->guest())
							<a href="{{ url('/login') }}">wróć do logowania</a>
						@else
							<a href="{{ route('profile') }}">moje konto</a>
						@endif
					</div>

				</form>
			</div>
		</div>
	</section>


	{{--<div class="container">--}}
	{{--<div class="row">--}}
	{{--<div class="col-md-8 col-md-offset-2">--}}
	{{--<div class="panel panel-default">--}}
	{{--<div class="panel-heading">Reset Password</div>--}}
	{{--<div class="panel-body">--}}
	{{--@if (session('status'))--}}
	{{--<div class="alert alert-success">--}}
	{{--{{ session('status') }}--}}
	{{--</div>--}}
	{{--@endif--}}

	{{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">--}}
	{{--{{ csrf_field() }}--}}

	{{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
	{{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

	{{--<div class="col-md-6">--}}
	{{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

	{{--@if ($errors->has('email'))--}}
	{{--<span class="help-block">--}}
	{{--<strong>{{ $errors->first('email') }}</strong>--}}
	{{--</span>--}}
	{{--@endif--}}
	{{--</div>--}}
	{{--</div>--}}

	{{--<div class="form-group">--}}
	{{--<div class="col-md-6 col-md-offset-4">--}}
	{{--<button type="submit" class="btn btn-primary">--}}
	{{--Send Password Reset Link--}}
	{{--</button>--}}
	{{--</div>--}}
	{{--</div>--}}
	{{--</form>--}}
	{{--</div>--}}
	{{--</div>--}}
	{{--</div>--}}
	{{--</div>--}}
	{{--</div>--}}
@endsection
