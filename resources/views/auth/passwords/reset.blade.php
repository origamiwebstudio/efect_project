@extends('layouts.main')

@section('page')

    <section class="login-block">
        <div class="login-block__content">
            <div class="login-block__left-side">
                <div class="login-block__overlay">
                    <div class="login-block__left-side-content">
                        <h2>wprowadź nowe hasło</h2>
                        <p>Wpisz adres mejlowy i nowe hasło</p>
                    </div>
                </div>
            </div>

            <div class="login-block__right-side">

                <h2>zmień hasło</h2>

                <form method="POST" action="{{ url('/password/reset') }}" ref="resetPassForm" data-token="{{ $token }}" class="efect-form" :class="{'efect-form_loading': sending }" @submit.prevent="resetPassword">

                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="email" name="email" v-model="resetPassData.email" id="email" placeholder="email" oldvalue="{{ $email or old('email') }}" :error="resetPassErrors.email"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="password" name="password" v-model="resetPassData.password" id="password" placeholder="hasło" :error="resetPassErrors.password"></div>
                    </div>
                    <div class="efect-form__form-group">
                        <div is="efect-input" class="efect-form__input" type="password" name="password_confirmation" v-model="resetPassData.password_confirmation" id="password_confirmation" placeholder="potwierdzenie hasła" :error="resetPassErrors.password_confirmation"></div>
                    </div>

                    <div class="login-block__buttons login-block__buttons_reset">
                        <button type="submit" class="btn" :class="{ 'btn_disabled': sending}" :disabled="sending">zmień</button>
                        @if(auth()->guest())
                            <a href="{{ url('/login') }}">wróć do logowania</a>
                        @else
                            <a href="{{ route('profile') }}">moje konto</a>
                        @endif
                    </div>

                </form>
            </div>
        </div>
    </section>

{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Reset Password</div>--}}

                {{--<div class="panel-body">--}}
                    {{--@if (session('status'))--}}
                        {{--<div class="alert alert-success">--}}
                            {{--{{ session('status') }}--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<input type="hidden" name="token" value="{{ $token }}">--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}

                                {{--@if ($errors->has('password_confirmation'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Reset Password--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
@endsection
