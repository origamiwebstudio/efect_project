<?php

return [

	#SERVER ERRORS

	'403_text' => 'Nie masz uprawnień do przeglądania tej strony',
	'404_text' => 'Taka strona nie istnieje',
	'503_text' => 'Przepraszamy, serwis tymczasowo niedostępny',


	#JSON ERRORS AND MESSAGES

	#---common

	'success'       => 'Sukces',
	'failure'       => 'Niepowodzenie',
	'error_occured' => 'Wystąpił błąd. Nie udało nam się wykonać to polecenie',

	#---favorites

	'added_to_favorites'         => 'Dodano do ulubionych',
	'not_added_to_favorites'     => 'Nie udało nam się dodać ten produkt do ulubionych',
	'removed_from_favorites'     => 'Usunięto z ulubionych',
	'not_removed_from_favorites' => 'Nie udało nam się usunąć ten produkt z ulubionych',

	#---basket

	'changed_basket_product_quantity'     => 'Zmieniono ilość produktów',
	'we_do_not_have_so_many_products'     => 'Niestety, nie mamy takiej ilości produktów',
	'not_changed_basket_product_quantity' => 'Nie udało nam się zmienić ilość produktów',
	'removed_from_basket'                 => 'Usunięto z kosza',
	'not_removed_from_basket'             => 'Nie udało nam się usunąć ten produkt z kosza',
	'added_to_basket'                     => 'Dodano do kosza',
	'not_added_to_basket'                 => 'Nie udało nam się dodać ten produkt do kosza',

	#---profile

	'changed_profile_data'     => 'Zmieniono dane konta',
	'not_changed_profile_data' => 'Nie udało nam się zmienić dane konta',

	#---order

	'payment_method_does_not_exist'  => 'Taka metoda płatności nie istnieje',
	'empty_basket'                   => 'Masz pusty koszyk',
	'order_does_not_exist'           => 'Takie zamówienie nie istnieje',
	'delivery_method_does_not_exist' => 'Taka metoda dostawy nie istnieje',


	#STATIC ERRORS AND MESSAGES

];
