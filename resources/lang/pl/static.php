<?php

return [

	#Metatags
	'meta_description' => 'Origami Web Studio is professional developers team which developing high-quality web-sites any level complexity and for all types of devices',
	'meta_keywords'    => 'Website development, site, web studio, development, order',

	'internet_shop'         => 'Sklep internetowy odzieży',
	'products_collection'   => 'Kolekcja produktów',
	'newest_collection'     => 'Najnowsza kolekcja',
	'special_offers'        => 'Oferty specjalne',
	'fashionable_and_cheap' => 'Modne i tanie :category',
	'my_profile'            => 'Moje konto',
	'favorites'             => 'Ulubione',
	'my_basket'             => 'Mój koszyk',
	'my_orders'             => 'Moje zamówienia',
	'last_viewed_products'  => 'Ostatnio oglądane produkty',
	'login'                 => 'Logowanie',
	'register'              => 'Rejestracja',
	'reset_password'        => 'Resetowanie hasła',

];
