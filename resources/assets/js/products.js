require('./common');

import vueSlider from 'vue-slider-component';
import dropdown from './components/dropdown.vue';
import addModal from './components/add-to-basket.modal.vue';
import Paginate from 'vuejs-paginate';

let VueLazyload = require('vue-lazyload');
Vue.use(VueLazyload,{
  error: `${window.location}/images/image-error.jpg`
});

new Vue({
  el: '#allproducts',
  components: {
    'vue-slider': vueSlider,
    'dropdown': dropdown,
    'add-to-basket-modal': addModal,
    'paginate': Paginate
  },
  data: {
    filtersOn: false,
    min: window.sliderProps.min,
    max: window.sliderProps.max,
    filterData: {
      spec_filter: '',
      categories: [],
      sizes: [],
      prices: [
        window.sliderProps.min,
        window.sliderProps.max
      ],
      order: ''
    },
    products: [],
    pagination: [],
    allcategories: '',
    allsizes: true,
    currentOrder: 'sortuj według',
    filterQuery: 0,
    showTakenProducts: false,
    forcePaginatePage: 0,
    sending: false,
    sizeFiltersShow: false,
    categoryFiltersShow: false,
    priceFiltersShow: false
  },
  watch: {
    filterData: {
      handler: function (val) {
        let self = this;
        self.filterQuery += 1;
        if(self.filterQuery > 1){
          if(!val.categories.length){
            self.allcategories = true;
          }else{
            if(this.allcategories) self.allcategories = false;
          }
          if(!val.sizes.length){
            self.allsizes = true;
          }else{
            if(self.allsizes) self.allsizes = false;
          }
          self.sendData();
        }
      },
      deep: true
    }
  },
  methods: {
    sendData(){
      const self = this;
      self.sending = true;
     axios.post('/products',this.filterData).then(response => {
       self.products = response.data.products;
       self.pagination = response.data.pagination;
       self.forcePaginatePage = response.data.pagination.page_current - 1;
       self.sending = false;
       if(!self.showTakenProducts) self.showTakenProducts = true;
     }).catch(err => {
       self.sending = false;
       console.log(err.response);
       let messageId = '_' + Math.random().toString(36).substr(2, 9);
       window.messages.push({
         id: messageId,
         title: 'Niepowodzenie',
         message: `Błąd z kodem ${err.response.status}`,
         stateClass: 'fixed-messages__item_error',
         timeout: 9000
       })
     });
    },
    paginate(page){
      const self = this;
      self.sending = true;
      axios.post('/products?page='+page, this.filterData).then(response => {
        self.products = response.data.products;
        self.pagination = response.data.pagination;
        self.sending = false;
        if(!self.showTakenProducts) self.showTakenProducts = true;
      }).catch(err => {
        self.sending = false;
        console.log(err.response);
        let messageId = '_' + Math.random().toString(36).substr(2, 9);
        window.messages.push({
          id: messageId,
          title: 'Niepowodzenie',
          message: `Błąd z kodem ${err.response.status}`,
          stateClass: 'fixed-messages__item_error',
          timeout: 9000
        })
      });
      this.forcePaginatePage = page - 1;
    },
    setAllCategories(){
      if(this.allcategories){
        this.filterData.categories = [];
      }else{
        this.allcategories = true;
      }
    },
    setAllSizes(){
      if(this.allsizes){
        this.filterData.sizes = [];
      }else{
        this.allsizes= true;
      }
    },
    setOrder(val){
      let self = this;
      this.filterData.order = val;
      switch(val){
        case 'newest':
          self.currentOrder = 'najnowsze';
          break;
        case 'oldest':
          self.currentOrder = 'najstarsze';
          break;
        case 'price_up':
          self.currentOrder = 'cena rosnąnco';
          break;
        case 'price_down':
          self.currentOrder = 'cena malejąco';
          break;
        default:
          self.currentOrder = 'sortuj według'
      }
    },
    clearFilters(){
      this.filterData = {
        spec_filter: '',
        categories: [],
        sizes: [],
        prices: [
          window.sliderProps.min,
          window.sliderProps.max
        ],
        order: ''
      };
      this.filtersOn = false;
    }
  },
  beforeMount(){
    let specVar = window.sliderProps.spec_filter,
      self = this;
    if(specVar){
      if(specVar === "new" || specVar === "sale" || specVar === "bestseller"){
        self.filterData.spec_filter = specVar;
        self.allcategories = true;
      }else{
        self.filterQuery--;
        self.filterData.categories.push(specVar);
        self.allcategories = false;
      }
    }else{
      self.allcategories = true;
    }
  }
});