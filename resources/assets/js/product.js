require('./common');

import LightBox from 'vue-simple-lightbox';
import addedToBasket from './components/added-to-basket.vue';
import sizeTable from './components/size-table.vue';

let VueLazyload = require('vue-lazyload');
Vue.use(VueLazyload,{
  error: `${window.location}/images/image-error.jpg`
});

new Vue({
  el: '#productsingle',
  components: {
    'lightbox': LightBox,
    'added-to-basket': addedToBasket,
    'size-table': sizeTable
  },
  data: {
    options: {
      animationSpeed: 150,
      alertError: false
    },
    productData:{
      _token: window.LaravelToken,
      product_id: window.productData.id,
      color: window.productData.checkedColor,
      size: window.productData.checkedSize,
      quantity: 1
    },
    isFavorite: window.productData.favorite,
    authorized: window.productData.authorized,
    sending: false,
    favoriteSending: false
  },
  methods:{
    showMessage(){
      let messageInst = this.$refs.addedMessage;
      messageInst.showMessage();
    },
    addToBasket(){
      let self = this,
        messageInst = this.$refs.addedMessage;
      self.sending = true;
      axios.post('/basket/add', self.productData).then(response => {
        if (response.data.status === true) {
          messageInst.showMessage();
          window.basketData.loading = true;
          axios.post('/basket/count').then(response => {
            window.basketData.loading = false;
            window.basketData.count = response.data;
          });
        }else{
          let messageId = '_' + Math.random().toString(36).substr(2, 9);
          window.messages.push({
            id: messageId,
            title: response.data.message.title,
            message: response.data.message.text,
            stateClass: response.data.message.class,
            timeout: response.data.message.timeout ? response.data.message.timeout : 5000
          });
        }
        self.sending = false;
      }).catch(err => {
        self.sending = false;
        console.log(err.response);
        let messageId = '_' + Math.random().toString(36).substr(2, 9);
        window.messages.push({
          id: messageId,
          title: 'Niepowodzenie',
          message: `Błąd z kodem ${err.response.status}`,
          stateClass: 'fixed-messages__item_error',
          timeout: 9000
        })
      });
    },
    addToFavorite(){
      let self = this;
      if(!self.favoriteSending){
        self.favoriteSending = true;
        if(self.authorized){
          let favoriteData = {
            _token: window.LaravelToken,
            product_id: self.productData.product_id
          };
          if(self.isFavorite){
            axios.post('/favorites/remove', favoriteData).then(response => {
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: response.data.message.title,
                message: response.data.message.text,
                stateClass: response.data.message.class,
                timeout: response.data.message.timeout ? response.data.message.timeout : 3000
              });
              if(response.data.status === true){
                self.isFavorite = false;
              }
              self.favoriteSending = false;
            }).catch(err => {
              self.favoriteSending = false;
              console.log(err.response);
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: 'Niepowodzenie',
                message: `Błąd z kodem ${err.response.status}`,
                stateClass: 'fixed-messages__item_error',
                timeout: 9000
              })
            })
          }else{
            axios.post('/favorites/add', favoriteData).then(response => {
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: response.data.message.title,
                message: response.data.message.text,
                stateClass: response.data.message.class,
                timeout: response.data.message.timeout ? response.data.message.timeout : 3000
              });
              if(response.data.status === true){
                self.isFavorite = true;
              }
              self.favoriteSending = false;
            }).catch(err => {
              self.favoriteSending = false;
              console.log(err.response);
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: 'Niepowodzenie',
                message: `Błąd z kodem ${err.response.status}`,
                stateClass: 'fixed-messages__item_error',
                timeout: 9000
              })
            })
          }
        }else{
          window.location = `${window.location.origin}/login`;
        }
      }
    },
    showSizeTable(){
      let sizeTableInst = this.$refs.sizeTable;
      sizeTableInst.showTable();
    }
  }
});

import addModal from './components/add-to-basket.modal.vue';

if(document.getElementById("lastseenproducts")){
  new Vue({
    el: "#lastseenproducts",
    components:{
      'add-to-basket-modal': addModal
    }
  });
}