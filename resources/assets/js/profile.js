require('./common');

import basketSidebar from './components/basket-sidebar.vue';
import efectInput from './components/input-text.vue';
import basketProductQuantity from './components/basket-product-quantity.vue';
import addModalFavorite from './components/add-to-basket_favorite.modal.vue';
import addModal from './components/add-to-basket.modal.vue';
import profileOrderItem from './components/profile-order-item.vue';

let VueLazyload = require('vue-lazyload');
Vue.use(VueLazyload,{
  error: `${window.location}/images/image-error.jpg`
});

if(document.getElementById("basketpage")){
  new Vue({
    el: '.profile-pages',
    components:{
      'add-to-basket-modal-favorite': addModalFavorite,
      'basket-sidebar': basketSidebar,
      'basket-product-quantity': basketProductQuantity
    },
    data:{
      favoritesCount: {
        show: false,
        value: 0
      },
      basketCount: {
        show: false,
        value: 0
      },
      listType: '',
      itemsLength: ''
    },
    methods:{
      basketCountUpdate(){
        this.basketCount.value = window.basketData.count;
        this.basketCount.show = true;
      },
      refetchBasket(){
        let basketSidebar = this.$refs.basketSidebar;
        basketSidebar.fetchBasket();
      },
      removeBasketItem(el, id){
        let self = this;
        el.path[1].className = "removing";
        if(self.listType === 'favorite'){
          let favoriteData = {
            _token: window.LaravelToken,
            product_id: id
          };
          axios.post('/favorites/remove',favoriteData).then(response => {
            console.log(response);
            if(response.data.status === true){
              el.path[2].removeChild(el.path[1]);
              if(!self.favoritesCount.show) self.favoritesCount.show = true;
              self.favoritesCount.value -= 1;
              if(self.favoritesCount.value === 0) self.$refs.basketItems.hidden = true;
            }else{
              el.path[1].className = '';
            }
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: response.data.message.title,
              message: response.data.message.text,
              stateClass: response.data.message.class,
              timeout: response.data.message.timeout ? response.data.message.timeout : 3000
            });
          }).catch(err => {
            el.path[1].className = '';
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          });
        }else{
          let basketData = {
            _token: window.LaravelToken,
            item_uid: id
          };
          axios.post('/basket/remove',basketData).then(response => {
            if(response.data.status === true){
              window.basketData.loading = true;
              axios.post('/basket/count').then(response => {
                window.basketData.loading = false;
                window.basketData.count = response.data;
              });
              el.path[2].removeChild(el.path[1]);
              if(!self.basketCount.show) self.basketCount.show = true;
              self.basketCount.value -= 1;
              if(self.basketCount.value === 0) self.$refs.basketItems.hidden = true;
              let basketSidebar = self.$refs.basketSidebar;
              basketSidebar.fetchBasket();
            }else{
              el.path[1].className = '';
            }
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: response.data.message.title,
              message: response.data.message.text,
              stateClass: response.data.message.class,
              timeout: response.data.message.timeout ? response.data.message.timeout : 3000
            });
          }).catch(err => {
            el.path[1].className = '';
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          });
        }
      }
    },
    mounted(){
      let self = this;
      if(this.$refs.basketItems){
        self.listType = this.$refs.basketItems.attributes['data-type'].value;
        if(self.listType === 'favorite'){
          self.favoritesCount.value = this.$refs.basketItems.childElementCount;
        }else{
          self.basketCount.value = this.$refs.basketItems.childElementCount;
        }
      }
    }
  })
}else if(document.getElementById("profilepage")){
  new Vue({
    el: '#profilepage',
    components:{
      'efect-input': efectInput
    },
    data: {
      currentTab: 'view_data',
      editData: {
        _token: window.LaravelToken,
        name: '',
        surname: '',
        phone: '',
        email: '',
        locality: '',
        postal_code: '',
        street: '',
        house_number: ''
      },
      updatedData: '',
      errors: [],
      sending: false
    },
    methods:{
      editProfile(){
        const self = this;
        if(!self.sending){
          self.sending = true;
          axios.post('/profile/edit', self.editData).then(response => {
            console.log(response);
            self.sending = false;
            if(response.data.message){
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: response.data.message.title,
                message: response.data.message.text,
                stateClass: response.data.message.class,
                timeout: response.data.message.timeout ? response.data.message.timeout : 5000,
              });
            }
            if(!response.data.status){
              if(response.data.errors) self.errors = response.data.errors;
            }else{
              self.updatedData = self.editData;
              self.currentTab = 'view_data';
            }
          }).catch(err => {
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          })
        }
      }
    }
  })
}else if(document.getElementById("lastseenproducts")){
  new Vue({
    el: ".profile-pages",
    components: {
      'add-to-basket-modal': addModal
    },
    data:{
      self: this,
      favoritesCount: {
        show: false,
        value: 0
      },
      basketCount: {
        show: false,
        value: 0
      }
    },
    methods:{
      incrementFavorite(){
        this.favoritesCount.value++;
        if(!this.favoritesCount.show) this.favoritesCount.show = true;
      },
      decrementFavorite(){
        this.favoritesCount.value--;
        if(!this.favoritesCount.show) this.favoritesCount.show = true;
      },
      incrementBasket(){
        this.basketCount.value++;
        if(!this.basketCount.show) this.basketCount.show = true;
      }
    },
    mounted(){
      this.favoritesCount.value = this.$refs.favoritesCount.attributes['data-count'].value;
      this.basketCount.value = this.$refs.basketCount.attributes['data-count'].value;
    }
  })
}else if(document.getElementById("userorders")){
  new Vue({
    el: '#userorders',
    components:{
      'profile-order-item':profileOrderItem
    },
    data: {
      currentTab: 'openedorders'
    }
  })
}