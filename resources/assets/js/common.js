require('./bootstrap');

import dropdown from './components/dropdown.vue';

new Vue({
  el: '#headerapp',
  components: {
    'dropdown': dropdown
  },
  data: {
    navMenu: false,
    search: false,
    basketData: window.basketData,
    searchText: '',
    searchProducts: [],
    timeout: null,
    loading: false,
    messagesData: window.messages,
    messagesList: [],
    cookiesConfirmed: true
  },
  watch:{
    searchText(){
      let self = this;
      clearTimeout(self.timeout);
      self.loading = true;
      this.timeout = setTimeout(function(){
        self.searching(self.searchText);
      }, 500)
    },
    messagesData(){
      let theMessage = this.messagesData[this.messagesData.length - 1];
      this.messagesList.unshift(theMessage);
      this.showMessage(theMessage);
    }
  },
  methods:{
    searching(text){
      let self = this;
      if(text.length > 1){
        let searchData = {
          _token: window.LaravelToken,
          search: text
        };
        axios.post("/search", searchData).then(response => {
          self.searchProducts = response.data;
          self.loading = false;
        }).catch(err => {
          self.loading = false;
        })
      }else if(text.length === 0){
        self.searchProducts = [];
      }
    },
    searchOn(){
      if(this.search){
        this.search = false;
      }else{
        this.search = true;
        setTimeout(()=>{
          this.$refs.searchInput.focus();
        },500);
      }
    },
    showMessage(message){
      let self = this;
      if(message.timeout){
        setTimeout(()=>{
          let index = this.messagesList.indexOf(message);
          this.messagesList.splice(index,1);
        },message.timeout);
      }
    },
    cookiesAccept(el){
      const self = this;
      try{
        localStorage.efectModaComCookies = "confirmed";
      }catch(err) {
          console.log(err.response);
          let messageId = '_' + Math.random().toString(36).substr(2, 9);
          window.messages.push({
            id: messageId,
            title: 'Niepowodzenie',
            message: `Błąd z kodem ${err.response.status}`,
            stateClass: 'fixed-messages__item_error',
            timeout: 9000
          });
      } finally {
        self.cookiesConfirmed = true;
      }

      // let self = this,
      //   data = {
      //   _token: window.LaravelToken
      // };
      // el.path[4].removeChild(el.path[3]);
      // axios.post('/cookies/accept', data).then(response => {
      //   if(!response.data.status){
      //     self.cookiesSending = false;
      //     let messageId = '_' + Math.random().toString(36).substr(2, 9);
      //     window.messages.push({
      //       id: messageId,
      //       title: response.data.message.title,
      //       message: response.data.message.text,
      //       stateClass: response.data.message.class,
      //       timeout: response.data.message.timeout ? response.data.message.timeout : 5000
      //     });
      //   }
      // }).catch(err => {
      //   self.cookiesSending = false;
      //   console.log(err.response);
      //   let messageId = '_' + Math.random().toString(36).substr(2, 9);
      //   window.messages.push({
      //     id: messageId,
      //     title: 'Niepowodzenie',
      //     message: `Błąd z kodem ${err.response.status}`,
      //     stateClass: 'fixed-messages__item_error',
      //     timeout: 9000
      //   });
      // })
    }
  },
  mounted: function(){
    const self = this;
    if (typeof(Storage) !== "undefined"){
      if(localStorage.efectModaComCookies !== "confirmed"){
        self.cookiesConfirmed = false;
      }
    }
    window.addEventListener('click', function(e){
      if(!e.target.classList.contains('burger') && !e.target.parentNode.classList.contains('burger')){
        let navgigationClass = "main-header__navigation",
          node = e.target,
          isThereTarget = false;
        while (node !== null){
          if(node.classList){
            if(node.classList.contains(navgigationClass)){
              isThereTarget = true;
              break;
            }
          }
          node = node.parentNode;
        }
        if(!isThereTarget){
          self.navMenu = false;
        }
      }
    });
    window.addEventListener('click', function(e){
      if(!e.target.classList.contains('search') && !e.target.parentNode.classList.contains('search')){
        let navgigationClass = "search-content",
          node = e.target,
          isThereTarget = false;
        while (node !== null){
          if(node.classList){
            if(node.classList.contains(navgigationClass)){
              isThereTarget = true;
              break;
            }
          }
          node = node.parentNode;
        }
        if(!isThereTarget){
          self.search = false;
        }
      }
    });
  }
});
