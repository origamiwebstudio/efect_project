require('./common');

import efectInput from './components/input-text.vue';

if(document.getElementById("contactspage")){
  new Vue({
    el: '#contactsform',
    components:{
      'efect-input': efectInput
    },
    data:{
      formData:{
        _token: window.LaravelToken,
        name: '',
        surname: '',
        email: '',
        phone: '',
        message: '',
        'g-recaptcha-response': ''
      },
      sending: false,
      errors: {}
    },
    methods:{
      sendForm(){
        const self = this;
        if (!self.sending){
          self.sending = true;
          self.formData['g-recaptcha-response'] = self.$refs.captcha.childNodes[0].childNodes[0].childNodes[1].value;
          axios.post('/contacts/send', self.formData).then(response => {
            self.sending = false;
            if(response.data.message){
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: response.data.message.title,
                message: response.data.message.text,
                stateClass: response.data.message.class,
                timeout: response.data.message.timeout ? response.data.message.timeout : 5000,
              });
            }
            if (!response.data.status){
              if(response.data.errors){
                self.errors = response.data.errors;
              }
            }else{
              self.formData.name = '';
              self.formData.surname = '';
              self.formData.email = '';
              self.formData.phone = '';
              self.formData.message = '';
              self.formData['g-recaptcha-response'] = '';
              grecaptcha.reset(0);
            }
          }).catch(err => {
            self.errors = {};
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          })
        }
      }
    }
  })
}