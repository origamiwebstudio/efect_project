require('./common');

let VueAwesomeSwiper = require('vue-awesome-swiper');
Vue.use(VueAwesomeSwiper);

let VueLazyload = require('vue-lazyload');
Vue.use(VueLazyload,{
  error: `${window.location}/images/image-error.jog`
});

new Vue({
  el: '.s-hero',
  data:{
    sliderOptions:{
      pagination: '.swiper-pagination',
      paginationClickable: true,
      autoplay: 5000,
      lazyLoading: true,
      loop: true
    }
  }
});

import addModal from './components/add-to-basket.modal.vue';

new Vue({
  el: '#products',
  data:{
    currentTab: 'bestseller'
  },
  components:{
    'add-to-basket-modal': addModal
  }
});