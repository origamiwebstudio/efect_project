require('./common');

import efectInput from './components/input-text.vue';

Vue.component('efect-input', efectInput);

if (document.getElementById("registerpage")) {
  new Vue({
    el: '#registerpage',
    data: {
      formData: {
        _token: window.LaravelToken,
        name: '',
        surname: '',
        email: '',
        password: '',
        password_confirmation: '',
        phone: '',
        add_address: '',
        country: '',
        locality: '',
        postal_code: '',
        street: '',
        house_number: '',
        privacy_policy: '',
        regimen: '',
        'g-recaptcha-response': ''
      },
      errors: {},
      sending: false
    },
    computed: {
      uncheckedRegimen() {
        return !(this.formData.privacy_policy === true && this.formData.regimen === true);
      }
    },
    methods: {
      registerUser() {
        const self = this;
        if (!self.sending && !self.uncheckedRegimen) {
          self.sending = true;
          self.formData['g-recaptcha-response'] = self.$refs.captcha.childNodes[0].childNodes[0].childNodes[1].value;
          axios.post('/register', self.formData).then(response => {
            if(response.data.message){
              let messageId = '_' + Math.random().toString(36).substr(2, 9);
              window.messages.push({
                id: messageId,
                title: response.data.message.title,
                message: response.data.message.text,
                stateClass: response.data.message.class,
                timeout: response.data.message.timeout ? response.data.message.timeout : 5000,
              });
            }
            if (!response.data.status) {
              self.sending = false;
              if(response.data.errors){
                self.errors = response.data.errors;
              }
            } else {
              window.location = window.location.origin;
            }
          }).catch(err => {
            self.errors = {};
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          })
        }
      }
    }
  });
} else if (document.getElementById("orderpage")) {
  new Vue({
    el: '#orderpage',
    data: {
      orderData: {
        _token: window.LaravelToken,
        name: '',
        surname: '',
        email: '',
        phone: '',
        country: '',
        locality: '',
        postal_code: '',
        street: '',
        house_number: '',
        delivery: '',
        payment: '',
        privacy_policy: '',
        regimen: ''
      },
      orderErrors: {},
      sending: false,
      productPrice: ''
    },
    computed:{
      uncheckedCheckboxes(){
        return !(this.orderData.privacy_policy && this.orderData.regimen && this.orderData.payment);
      },
      totalPrice(){
        if(this.orderData.delivery){
          return parseFloat(this.deliveryData.price) + parseFloat(this.productPrice);
        }else{
          return this.productPrice;
        }
      },
      deliveryData(){
        return this.orderData.delivery ? JSON.parse(this.orderData.delivery) : 0;
      }
    },
    methods:{
      makeOrder(type){
        let self = this,
          address = type === 'create' ? '/order' : '/order/update';
        if (!self.sending && !self.uncheckedCheckboxes) {
          self.sending = true;
          axios.post(address, self.orderData).then(response => {
            if (!response.data.status) {
              if(response.data.errors){
                self.orderErrors = response.data.errors;
              }
              if(response.data.message){
                let messageId = '_' + Math.random().toString(36).substr(2, 9);
                window.messages.push({
                  id: messageId,
                  title: response.data.message.title,
                  message: response.data.message.text,
                  stateClass: response.data.message.class,
                  timeout: response.data.message.timeout ? response.data.message.timeout : 5000,
                });
              }
              self.sending = false;
            } else {
              window.location = response.data.redirect;
            }
          }).catch(err => {
            self.orderErrors ={};
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          });
        }
      }
    },
    mounted(){
      if(this.$refs.orderprice !== undefined) this.productPrice = this.$refs.orderprice.attributes['data-price'].value;
      if(this.$refs.chosenPayment !== undefined) this.orderData.payment = this.$refs.chosenPayment.value;
      if(this.$refs.chosenDelivery !== undefined) this.orderData.delivery = this.$refs.chosenDelivery.value;
      if(this.$refs.privacyPolicy !== undefined) this.orderData.privacy_policy = true;
      if(this.$refs.regimen !== undefined) this.orderData.regimen= true;
      if(this.$refs.order_number !== undefined) this.orderData.order_number = this.$refs.order_number.value;
    }
  })
} else {
  new Vue({
    el: '.efect-form',
    data: {
      loginData: {
        _token: window.LaravelToken,
        email: '',
        password: ''
      },
      resetEmailData: {
        _token: window.LaravelToken,
        email: ''
      },
      resetPassData: {
        _token: window.LaravelToken,
        token: '',
        email: '',
        password: '',
        password_confirmation: ''
      },
      loginErrors: {},
      resetEmailErrors: {},
      resetPassErrors: {},
      sending: false,
      errors: {}
    },
    methods: {
      login() {
        let self = this;
        if(!self.sending){
          self.sending = true;
          axios.post('/login', self.loginData).then(response => {
            if (response.data.errors) {
              self.loginErrors = response.data.errors;
              self.sending = false;
            } else {
              window.location = response.data;
            }
          }).catch(err => {
            self.loginErrors ={};
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          });
        }
      },
      resetEmail() {
        let self = this;
        if(!self.sending){
          self.sending = true;
          axios.post('/password/email', self.resetEmailData).then(response => {
            if(!response.data.status){
              self.sending = false;
              if (response.data.errors) {
                self.resetEmailErrors = response.data.errors;
              }
              if(response.data.message){
                let messageId = '_' + Math.random().toString(36).substr(2, 9);
                window.messages.push({
                  id: messageId,
                  title: response.data.message.title,
                  message: response.data.message.text,
                  stateClass: response.data.message.class,
                  timeout: response.data.message.timeout ? response.data.message.timeout : 5000,
                });
              }
            }else{
              window.location = response.data.redirect;
            }
          }).catch(err => {
            self.resetEmailErrors ={};
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          })
        }
      },
      resetPassword(){
        let self = this;
        if(!self.sending){
          self.sending = true;
          if(!self.resetPassData.token) self.resetPassData.token = self.$refs.resetPassForm.attributes['data-token'].value;
          axios.post('/password/reset', self.resetPassData).then(response => {
            if(!response.data.status){
              self.sending = false;
              if (response.data.errors) {
                self.resetPassErrors = response.data.errors;
              }
              if(response.data.message){
                let messageId = '_' + Math.random().toString(36).substr(2, 9);
                window.messages.push({
                  id: messageId,
                  title: response.data.message.title,
                  message: response.data.message.text,
                  stateClass: response.data.message.class,
                  timeout: response.data.message.timeout ? response.data.message.timeout : 5000,
                });
              }
            }else{
              if(response.data.redirect){
                window.location = response.data.redirect;
              }else{
                window.location = window.location.origin;
              }
            }
          }).catch(err => {
            self.resetPassErrors ={};
            self.sending = false;
            console.log(err.response);
            let messageId = '_' + Math.random().toString(36).substr(2, 9);
            window.messages.push({
              id: messageId,
              title: 'Niepowodzenie',
              message: `Błąd z kodem ${err.response.status}`,
              stateClass: 'fixed-messages__item_error',
              timeout: 9000
            })
          })
        }
      }
    }
  })
}
